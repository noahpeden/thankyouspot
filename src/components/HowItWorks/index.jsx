import React, { Component } from 'react'

import {
	Wrapper,
	HowItWorksTitle,
	StepsBound,
	Steps,
	StepsGraphic,
	StepNumber
} from './styles'

export default class HowItWorks extends Component {
	render() {
		return (
			<Wrapper>
				<HowItWorksTitle>How Thank You Spot Works</HowItWorksTitle>
				<StepsBound>
					<Steps>
						<StepsGraphic
							src={require('../../assets/images/loop1.gif')}
							alt="Hand holding phone collage"
						/>
						<StepNumber>Choose a thank you type.</StepNumber>
					</Steps>
					<Steps>
						<StepsGraphic
							src={require('../../assets/images/loop2.gif')}
							alt="Letter in envelope collage"
						/>
						<StepNumber>Compose your thank you.</StepNumber>
					</Steps>
					<Steps>
						<StepsGraphic
							src={require('../../assets/images/loop3.gif')}
							alt="World and mail collage"
						/>
						<StepNumber>Send and share your thank you.</StepNumber>
					</Steps>
				</StepsBound>
			</Wrapper>
		)
	}
}

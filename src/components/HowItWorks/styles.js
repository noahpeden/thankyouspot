import styled from 'styled-components'
import media from 'styled-media-query'

export const Wrapper = styled.div`
	padding: 5% 7%;
	height: auto;
	text-align: center;
	${media.between('medium', 'large')`
		padding: 4% 5%;
	`};
	${media.lessThan('medium')`
	`};
`
export const HowItWorksTitle = styled.h2`
	font-size: 2.75rem;
	color: #184e68;
	text-shadow: -6px 6px #dbeef7;
	margin: 0;
	padding-bottom: 30px;
	font-family: 'Paytone One', sans-serif;
	-webkit-font-smoothing: antialiased;
	letter-spacing: 1px;
	${media.lessThan('medium')`
		margin-top: 20px;
	`};
`
export const StepsBound = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	height: auto;
	text-align: center;
	max-width: 90%;
	margin: 0 auto;
	${media.between('medium', 'large')`
		max-width: none;
	`};
	${media.lessThan('medium')`
		max-width: none;
		flex-direction: column;
		align-items: center;
	`};
`
export const Steps = styled.div`
	max-width: 336px;
	padding: 25px 25px 0;
	${media.lessThan('medium')`
		padding: 15px;
		max-width: none;
	`};
`
export const StepsGraphic = styled.img`
	max-height: 250px;
	${media.between('medium', 'large')`
		max-height: 180px;
	`};
	${media.lessThan('medium')`
		max-height: 150px;
		margin-bottom: 20px;
	`};
`
export const StepNumber = styled.h2`
	font-size: 1.5rem;
	color: #ff7676;
	margin: 30px 0 0;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 600;
	-webkit-font-smoothing: antialiased;
	${media.lessThan('medium')`
		margin: 0 0 20px;
	`};
`
export const StepCaption = styled.p`
	font-size: 18px;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 300;
	-webkit-font-smoothing: antialiased;
	font-color: #3a3d3f;
	line-height: 1.5;
	${media.lessThan('medium')`
	`};
`

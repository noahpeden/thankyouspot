import styled from 'styled-components'
import {
	CardElement,
	CardNumberElement,
	CardExpiryElement,
	CardCVCElement
} from 'react-stripe-elements'

export const CheckoutWrapper = styled.div`
	padding: 30px 30px 0;
`

export const StripeCardInput = styled(CardElement)`
	background-color: #fff;
	padding: 15px;
`

export const StripeCardNumberInput = styled(CardNumberElement)`
	background-color: #fff;
	margin: 10px 0 20px;
	padding: 15px;
	cursor: text;
	border-bottom: 2px solid transparent;
	transition: all 0.2s ease;
	&:hover,
	&:focus {
		background: #f8fbfe;
		border-bottom: 2px solid #ff7675;
	}
`

export const ExpiryCVCWrapper = styled.div`
	display: flex;
	flex-direction: row;
	align-items: flex-end;
	margin: 10px 0;
`

export const ExpirationWrapper = styled.div`
	flex: 1;
`

export const StripeCardExpiryInput = styled(CardExpiryElement)`
	background-color: #fff;
	margin: 10px 0 20px;
	padding: 15px;
	cursor: text;
	border-bottom: 2px solid transparent;
	transition: all 0.2s ease;
	&:hover,
	&:focus {
		background: #f8fbfe;
		border-bottom: 2px solid #ff7675;
	}
`

export const CVCWrapper = styled.div`
	flex: 1;
	margin-left: 10px;
`

export const StripeCardCVCInput = styled(CardCVCElement)`
	background-color: #fff;
	margin: 10px 0 20px;
	padding: 15px;
	cursor: text;
	border-bottom: 2px solid transparent;
	transition: all 0.2s ease;
	&:hover,
	&:focus-within {
		background: #f8fbfe;
		border-bottom: 2px solid #ff7675;
	}
`

export const StripeCardElementStyle = {
	base: {
		fontFamily: '"Josefin Sans", sans-serif',
		fontSize: '16px',
		padding: '15px'
	}
}

export const StripeCardExpiryCVCStyle = {
	base: {
		fontFamily: '"Josefin Sans", sans-serif',
		fontSize: '14px',
		padding: '15px'
	}
}

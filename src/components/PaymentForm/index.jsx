import React, { Component } from 'react'
import { StripeProvider, Elements, injectStripe } from 'react-stripe-elements'
import {
	StripeCardElementStyle,
	StripeCardExpiryCVCStyle,
	StripeCardNumberInput,
	StripeCardExpiryInput,
	StripeCardCVCInput,
	ExpiryCVCWrapper,
	ExpirationWrapper,
	CVCWrapper,
	CheckoutWrapper
} from './styles'
import { InfoLabel } from '../Thanking/styles'
import { SidePanelHeading } from '../Thanking/Preview/styles'

const { REACT_APP_STRIPE_API_KEY } = process.env

class CheckoutForm extends Component {
	render() {
		return (
			<CheckoutWrapper>
				<SidePanelHeading>Payment Details</SidePanelHeading>
				<InfoLabel>Card Number</InfoLabel>
				<StripeCardNumberInput style={StripeCardElementStyle} />
				<ExpiryCVCWrapper>
					<ExpirationWrapper>
						<InfoLabel>Expiration Date</InfoLabel>
						<StripeCardExpiryInput style={StripeCardExpiryCVCStyle} />
					</ExpirationWrapper>
					<CVCWrapper>
						<InfoLabel>CVC</InfoLabel>
						<StripeCardCVCInput style={StripeCardExpiryCVCStyle} />
					</CVCWrapper>
				</ExpiryCVCWrapper>
				{this.props.thankingState.paymentError && (
					<p>{this.props.thankingState.paymentError.message}</p>
				)}
			</CheckoutWrapper>
		)
	}

	createToken = () => {
		return this.props.stripe.createToken()
	}
}

const InjectedCheckoutForm = injectStripe(CheckoutForm, { withRef: true })

class CheckoutFormWithProvider extends React.Component {
	render() {
		return (
			<StripeProvider apiKey={REACT_APP_STRIPE_API_KEY}>
				<Elements
					fonts={[
						{
							cssSrc:
								'https://fonts.googleapis.com/css?family=Josefin+Sans:100i,300,400,600,700'
						}
					]}
				>
					<InjectedCheckoutForm
						ref={ref => (this.injectedForm = ref)}
						{...this.props}
					/>
				</Elements>
			</StripeProvider>
		)
	}

	createToken = () => {
		return this.injectedForm.getWrappedInstance().createToken()
	}
}

export default CheckoutFormWithProvider

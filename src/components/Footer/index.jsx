import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

import {
	Wrapper,
	PageLinksWrapper,
	PageLink,
	FooterContainer,
	FooterColumn,
	SocialColumn,
	SocialIcon,
	SocialLink,
	FooterHeading
} from './styles'

import ContactUs from '../ContactUs'
import MailChimpSubscribeForm from './MailChimpSubscribeForm'

class Footer extends Component {
	render() {
		const { authInfo: { token = null } = {} } = this.props
		const loggedIn = Boolean(token)
		return (
			<Wrapper>
				<PageLinksWrapper>
					<PageLink to="/features">Features</PageLink>
					<PageLink to="/thank-someone">Create a Thank You</PageLink>
					<PageLink to="/global-thank-yous">Global Thank You's</PageLink>
					<PageLink to="/terms">Terms</PageLink>
					{!loggedIn && (
						<React.Fragment>
							<PageLink
								to={{ pathname: '/auth/login', state: { modal: true } }}
							>
								Log In
							</PageLink>
							<PageLink
								to={{ pathname: '/auth/signup', state: { modal: true } }}
							>
								Sign Up
							</PageLink>
						</React.Fragment>
					)}
				</PageLinksWrapper>
				<FooterContainer>
					<SocialColumn>
						<FooterHeading>Connect With Us</FooterHeading>
						<SocialLink
							to="https://www.facebook.com/thankyouspot/"
							target="_blank"
						>
							<SocialIcon
								src={require('../../assets/images/facebook.svg')}
								alt="fb icon"
							/>
						</SocialLink>
						<SocialLink to="https://twitter.com/ThankYouSpot" target="_blank">
							<SocialIcon
								src={require('../../assets/images/twitter.svg')}
								alt="twitter icon"
							/>
						</SocialLink>
						<SocialLink
							to="https://www.instagram.com/thankyouspot/"
							target="_blank"
						>
							<SocialIcon
								src={require('../../assets/images/instagram.svg')}
								alt="ig icon"
							/>
						</SocialLink>
					</SocialColumn>
					<FooterColumn>
						<FooterHeading>Contact Us</FooterHeading>
						<ContactUs />
					</FooterColumn>
					<MailChimpSubscribeForm />
				</FooterContainer>
			</Wrapper>
		)
	}
}

const AUTH_INFO_QUERY = gql`
	query AuthInfo {
		authInfo @client {
			token
		}
	}
`

export default graphql(AUTH_INFO_QUERY, {
	props: ({ data: { authInfo } }) => ({
		authInfo
	})
})(Footer)

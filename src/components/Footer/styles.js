import styled from 'styled-components'
import media from 'styled-media-query'
import { Link } from 'react-router-dom'

export const Wrapper = styled.div`
	height: auto;
	background-color: #184e68;
	justify-content: space-between;
	padding: 70px;
	color: white;

	${media.between('medium', 'large')`
		padding: 30px;
	`};

	${media.lessThan('medium')`
		padding: 20px;
	`};
`

export const PageLinksWrapper = styled.div`
	margin-left: 20%;
	width: 60%;
	display: flex;
	justify-content: space-around;
	flex-wrap: wrap;

	${media.lessThan('large')`
		margin-left: 5%;
		width: 90%;
	`};
`

export const PageLink = styled(Link)`
	text-decoration: none;
	color: #ffffff;
	font-size: 20px;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 500;
	-webkit-font-smoothing: antialiased;
	&:hover {
		color: #ff7676;
	}
	${media.lessThan('large')`
		font-size: 16px;
		padding: 15px 10px;
	`};
`

export const FooterContainer = styled.div`
	display: grid;
	grid-template-columns: 33% 33% 33%;
	padding-top: 20px;
	justify-items: center;
	width: 90%;
	margin: 0 auto;

	${media.between('medium', 'large')`
		grid-template-columns: 45% 45%;
	`};

	${media.lessThan('medium')`
		grid-template-columns: auto;
		width: 100%;
		padding-top: 0;
	`};
`

export const FooterColumn = styled.div`
	padding: 0 15px;
	width: 100%;
`

export const SocialColumn = FooterColumn.extend`
	${media.between('medium', 'large')`
		grid-column: 1/3;
		grid-row: 2/3;
	`};

	${media.lessThan('medium')`
		grid-column: 1/2;
		text-align: center;
	`};
`

export const InnerColumn = styled.div`
	max-width: 300px;
	margin: 0 auto;

	${media.lessThan('medium')`
		max-width: 100%;
	`};
`

export const FooterHeading = styled.h2`
	margin: 20px 0;
	color: #6eaecc;
	font-family: 'Caveat', cursive;
	font-size: 32px;
	font-weight: 400;
	-webkit-font-smoothing: antialiased;

	${media.lessThan('large')`
		font-size: 28px;
	`};

	${media.lessThan('medium')`
		text-align: center;
	`};
`
export const SocialLink = styled(Link)`
	padding-right: 15px;
	&:last-child {
		padding-right: 0;
	}
`

export const SocialIcon = styled.img``

export const FooterForm = styled.form`
	display: flex;
	flex-direction: column;
	align-items: flex-end;
`

export const FooterFormInput = styled.input`
	box-sizing: border-box;
	margin-bottom: 20px;
	width: 100%;
	padding: 18px 12px 16px;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 300;
	outline: none;
	border: 0;
	border-bottom: 2px solid transparent;
	transition: all 0.2s ease;
	&:hover,
	&:focus {
		background: #f8fbfe;
		border-bottom: 2px solid #ff7675;
	}

	${media.lessThan('medium')`
		margin-bottom: 10px;
	`};
`

export const FooterSubmitButton = styled.input.attrs({
	type: 'submit'
})`
	background: #dbeef7;
	text-decoration: none;
	border: 1px solid #dbeef7;
	color: #184e68;
	border-radius: 50px;
	padding: 14px 20px 12px;
	width: auto;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	font-size: 16px;
	transition: all 0.2s ease;
	&:hover {
		background: transparent;
		color: #dbeef7;
		cursor: pointer;
	}
`

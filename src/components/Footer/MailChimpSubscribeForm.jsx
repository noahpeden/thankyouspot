import React from 'react'

import {
	FooterColumn,
	FooterHeading,
	FooterForm,
	FooterFormInput,
	FooterSubmitButton
} from './styles'

export default () => (
	<FooterColumn>
		<FooterHeading>Subscribe</FooterHeading>
		<FooterForm
			action={process.env.REACT_APP_MAILCHIMP_SUBSCRIBE_URL}
			method="post"
			name="mc-embedded-subscribe-form"
			target="_blank"
			novalidate
		>
			<FooterFormInput
				type="email"
				name="EMAIL"
				autoComplete="off"
				placeholder="Email"
			/>
			<FooterSubmitButton value="Submit" />
		</FooterForm>
	</FooterColumn>
)

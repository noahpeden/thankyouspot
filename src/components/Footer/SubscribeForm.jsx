import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

import {
	FooterColumn,
	FooterHeading,
	FooterForm,
	FooterFormInput,
	FooterSubmitButton
} from './styles'
import swal from 'sweetalert2'

class SubscribeForm extends Component {
	constructor(props) {
		super(props)

		this.state = {
			isSubmitting: false
		}

		this.setEmailInput = ref => (this.emailInput = ref)
	}

	handleSubscription = async evt => {
		evt.preventDefault()
		this.setState({ isSubmitting: true })

		try {
			const {
				data: {
					addContactToSubscribeList: { success = false }
				}
			} = await this.props.addContact({
				variables: { email: this.emailInput.value }
			})

			if (success) {
				swal('Success!', 'Successfully subscribed!', 'success')
				this.emailInput.value = ''
			}
		} catch (err) {
			swal('Error', err.message, 'error')
		}
		this.setState({ isSubmitting: false })
	}
	render() {
		const { isSubmitting } = this.state
		return (
			<FooterColumn>
				<FooterHeading>Subscribe</FooterHeading>
				<FooterForm onSubmit={this.handleSubscription}>
					<FooterFormInput
						autoComplete="off"
						placeholder="Email"
						innerRef={this.setEmailInput}
					/>
					<FooterSubmitButton
						disabled={isSubmitting}
						value={`Submit${isSubmitting ? 'ting' : ''}`}
					/>
				</FooterForm>
			</FooterColumn>
		)
	}
}

const ADD_CONTACT_MUTATION = gql`
	mutation AddContactMuation($email: String!) {
		addContactToSubscribeList(email: $email) {
			success
		}
	}
`

export default graphql(ADD_CONTACT_MUTATION, {
	props: ({ mutate }) => ({
		addContact: options => {
			return mutate(options)
		}
	})
})(SubscribeForm)

import React from 'react'

export default props => <video autoPlay muted src={props.src} />

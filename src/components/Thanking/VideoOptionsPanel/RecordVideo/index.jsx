import React, { Component } from 'react'
import swal from 'sweetalert2'
import RecordRTC from 'recordrtc'

import WebCam from './WebCam'
import { captureUserMedia } from './RecordVideoUtils'
import { Apply, VideoRecordingWrapper } from '../styles'

const hasGetUserMedia = !!(
	navigator.getUserMedia ||
	navigator.webkitGetUserMedia ||
	navigator.mozGetUserMedia ||
	navigator.msGetUserMedia
)

class RecordVideo extends Component {
	constructor(props) {
		super(props)

		this.state = {
			recordVideo: null,
			src: null,
			stream: null,
			isRecording: false,
			hasPermissions: false
		}
	}

	componentDidMount() {
		if (!hasGetUserMedia) {
			this.props.onOffRecording()
			swal(
				'Error!',
				'Your browser cannot stream from your webcam. Please switch to Chrome or Firefox.',
				'error'
			)
		} else {
			this.requestUserMedia()
		}
	}

	componentWillUnmount() {
		// Stop stream
		if (Boolean(this.state.stream)) {
			const tracks = this.state.stream.getTracks()
			tracks.forEach(track => track.stop())
		}
	}

	requestUserMedia = () => {
		captureUserMedia(
			this.onSuccessCaptureUserMedia,
			this.onErrorCaptureUserMedia
		)
	}

	onSuccessCaptureUserMedia = stream => {
		this.setState({
			src: window.URL.createObjectURL(stream),
			stream,
			hasPermissions: true
		})
	}

	onErrorCaptureUserMedia = () => {
		this.setState({ hasPermissions: false })
	}

	startRecord = _ => {
		this.setState(
			{
				recordVideo: RecordRTC(this.state.stream, {
					type: 'video',
					checkForInactiveTracks: true
				}),
				isRecording: true
			},
			() => {
				this.state.recordVideo.startRecording()
			}
		)
	}

	stopRecord = () => {
		const { onAddVideo, onOffRecording } = this.props
		this.state.recordVideo.stopRecording(() => {
			try {
				const file = new File([this.state.recordVideo.getBlob()], 'video.mp4', {
					type: 'video/mp4'
				})

				onAddVideo(file)
				onOffRecording()
			} catch (error) {
				swal('Error!', error.message, 'error')
			}
		})
	}

	render() {
		const { src, isRecording, hasPermissions } = this.state

		return (
			<VideoRecordingWrapper>
				{hasPermissions ? (
					<WebCam src={src} />
				) : (
					<p>We need your permission to access the camera and microphone.</p>
				)}
				{hasPermissions &&
					(!isRecording ? (
						<Apply record onClick={this.startRecord}>
							Start Recording
						</Apply>
					) : (
						<Apply record onClick={this.stopRecord}>
							Stop Recording
						</Apply>
					))}
			</VideoRecordingWrapper>
		)
	}
}

export default RecordVideo

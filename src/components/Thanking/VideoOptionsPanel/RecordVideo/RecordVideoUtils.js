// handle user media capture
export function captureUserMedia(callback, errorCallBack) {
	var params = { audio: true, video: true }

	navigator.getUserMedia(params, callback, errorCallBack)
}

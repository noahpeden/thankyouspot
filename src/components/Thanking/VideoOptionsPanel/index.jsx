import React, { Component } from 'react'
import { Header, Title, Wrapper } from './styles'

import RecordVideo from './RecordVideo'

export default class VideoOptionsPanel extends Component {
	render() {
		return (
			<Wrapper>
				<Header>
					<Title>Record Video</Title>
				</Header>
				<RecordVideo
					onAddVideo={this.props.onAddVideo}
					onOffRecording={this.onOffRecording}
				/>
			</Wrapper>
		)
	}

	onOffRecording = () => {
		this.props.changeState('videoOptionsModalOpen', false)
	}
}

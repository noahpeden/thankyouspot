import TextareaAutosize from 'react-autosize-textarea'
import styled from 'styled-components'
import media from 'styled-media-query'

export const Card = styled.div`
	display: flex;
	flex-direction: column;
	width: 445px;
	height: 650px;
	padding: 50px 50px 40px;
	${media.lessThan('460px')`
		width: 100%;
	`};
`

export const Intro = styled(TextareaAutosize)`
	text-align: center;
	font-size: 45px;
	color: #184e68;
	margin: 0;
	font-family: 'Concert One', cursive;
	-webkit-font-smoothing: antialiased;
	letter-spacing: 1px;
	border: none;
	resize: none;
	height: auto;
	&:focus {
		outline: none;
	}
`

export const TextContainer = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	flex: 1;
	overflow: hidden;
	width: 100%;
`

export const TextScrollWrapper = styled.div`
	overflow: auto;
	margin: 16px -30px 16px 0;
	padding-right: 30px;
	width: 100%;
	word-break: break-word;
`

export const Text = styled(TextareaAutosize)`
	text-align: center;
	font-size: 27px;
	color: #3a3d3f;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 300;
	border: none;
	resize: none;
	width: 100%;
	&:focus {
		outline: none;
	}
`

export const Closing = styled(TextareaAutosize)`
	text-align: center;
	height: 60px;
	font-size: 45px;
	font-weight: 200;
	color: #6eaecc;
	font-family: 'Caveat', cursive;
	font-weight: 400;
	border: none;
	resize: none;
	&:focus {
		outline: none;
	}
`

export const SubmitButton = styled.button`
	border: none;
	height: 46px;
	border-radius: 23px;
	padding: 0 20px;
	font-size: 18px;
	color: #fff;
	background-color: #184e68;
	cursor: pointer;
	font-family: 'Josefin Sans', sans-serif;
	align-self: center;
	margin: 40px 0;
`
export const Wrapper = styled.div`
	display: flex;
	${media.between('medium', 'large')`
		flex-direction: column;
	`};
	${media.lessThan('medium')`
		flex-direction: column;
	`};
`
export const SidePanel = styled.div`
	width: 250px;
	background-color: #dbeef7;
	display: flex;
	flex-direction: column;
	${media.between('medium', 'large')`
		justify-content: space-between;
		width: auto;
		padding: 20px;
		flex-direction: column;
	`};
	${media.lessThan('medium')`
		width: auto;
		padding: 0;
		flex-direction: column;
		padding: 0 0 20px;
	`};
`
export const SidePanelHeading = styled.h2`
	color: #184e68;
	font-size: 26px;
	margin: 0;
	padding: 30px;
	border-top: 1px solid #ffffff;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	text-align: center;
	${media.between('medium', 'large')`
		display: none;
	`};
	${media.lessThan('medium')`
		display: none;
	`};
`
export const FeaturesBound = styled.div`
	display: flex;
	flex-direction: column;
	${media.between('medium', 'large')`
		display: flex;
		flex-direction: row;
		width: 100%;
		padding-top: 0;
		margin-bottom: 10px;
	`};
	${media.lessThan('medium')`
		display: flex;
		flex-direction: row;
		margin-bottom: 10px;
	`};
`

export const RemoveMedia = styled.button`
	position: absolute;
	top: 20px;
	left: 20px;
	height: 40px;
	width: 40px;
	border-radius: 50px;
	border: none;
	background: rgba(24, 78, 104, 0.7);
	color: #fff;
	cursor: pointer;
	transition: all 0.2s ease;
	outline: none;
	z-index: 1;
	&:hover {
		background: rgba(24, 78, 104, 1);
	}
`

import React, { Component } from 'react'
import Modal from 'react-modal'
import { Redirect } from 'react-router-dom'
import { Photo, Video, VideoContainer } from '../../ThankYouCard/styles'
import {
	BottomNavigationButton,
	FeatureButton,
	FeatureImage,
	FeatureLabel,
	Rightside,
	Price,
	Features1stRow,
	MainPanel,
	NavigationBottom
} from '../styles'
import {
	Card,
	Closing,
	Intro,
	Text,
	Wrapper,
	SidePanel,
	FeaturesBound,
	TextContainer,
	TextScrollWrapper,
	RemoveMedia
} from './styles'

import PhotoOptionsPanel from '../PhotoOptionsPanel'
import { PhotoOptionsModal } from '../PhotoOptionsPanel/styles'
import VideoOptionsPanel from '../VideoOptionsPanel'
import { VideoOptionsModal } from '../VideoOptionsPanel/styles'
import CustomDesignsPanel from '../CustomDesignsPanel'
import { CustomDesignModal } from '../CustomDesignsPanel/styles'
import {
	ThankyouCardWrapper,
	PreviewSectionWrapper,
	SidePanelHeading,
	FeatureRow,
	FeatureItem,
	FeaturePrice
} from '../Preview/styles'
import swal from 'sweetalert2'

Modal.setAppElement('#root')

const PRICES = {
	video: 1.49,
	customDesign: 0.99,
	postcard: 1.99
}

class ThankYouDetails extends Component {
	render() {
		const { thankingState, changeState } = this.props
		const { image, video, customDesign, isPostcard } = thankingState

		const hasPaidAddons = video || customDesign || isPostcard
		const totalPrice = hasPaidAddons
			? isPostcard
				? PRICES.postcard
				: (video && PRICES.video) || (customDesign && PRICES.customDesign)
			: 0

		if (this.props.incompleteDetails()) {
			return <Redirect to="/thank-someone" />
		}

		return (
			<Wrapper>
				<SidePanel>
					<FeaturesBound>
						<Features1stRow>
							<FeatureButton
								hasMedia={Boolean(image)}
								onClick={this.onOpenPhotoPanel}
							>
								<FeatureImage
									src={require('../../../assets/images/newfeature1.svg')}
									alt="Add"
								/>
								<Rightside>
									<FeatureLabel>Add a Photo</FeatureLabel>
									<Price>Free</Price>
								</Rightside>
							</FeatureButton>
							<FeatureButton
								hasMedia={Boolean(customDesign)}
								onClick={this.onOpenCustomDesign}
							>
								<FeatureImage
									src={require('../../../assets/images/newfeature2.svg')}
									alt="Custom Design"
								/>
								<Rightside>
									<FeatureLabel>Custom Design</FeatureLabel>
									<Price>$0.99</Price>
								</Rightside>
							</FeatureButton>
							<FeatureButton
								hasMedia={Boolean(video)}
								onClick={this.onOpenVideoPanel}
							>
								<FeatureImage
									src={require('../../../assets/images/newfeature3.svg')}
									alt="Video Thanks"
								/>
								<Rightside>
									<FeatureLabel>Video Thanks</FeatureLabel>
									<Price>$1.49</Price>
								</Rightside>
							</FeatureButton>
							<FeatureButton
								hasMedia={Boolean(isPostcard)}
								onClick={this.onClickSendPostcard}
							>
								<FeatureImage
									src={require('../../../assets/images/newfeature4.svg')}
									alt="Export as PDF"
								/>
								<Rightside>
									<FeatureLabel>Send as Postcard</FeatureLabel>
									<Price>$1.99</Price>
								</Rightside>
							</FeatureButton>
						</Features1stRow>
					</FeaturesBound>
					{Boolean(totalPrice) && (
						<PreviewSectionWrapper breakdown>
							<SidePanelHeading>Breakdown</SidePanelHeading>
							{image && (
								<FeatureRow>
									<FeatureItem>Photo</FeatureItem>
									<FeaturePrice>Free</FeaturePrice>
								</FeatureRow>
							)}
							{video && (
								<FeatureRow>
									<FeatureItem>Video</FeatureItem>
									<FeaturePrice>${PRICES.video}</FeaturePrice>
								</FeatureRow>
							)}
							{customDesign && (
								<FeatureRow>
									<FeatureItem>Custom Design</FeatureItem>
									<FeaturePrice>
										{!isPostcard ? `$${PRICES.customDesign}` : 'Free'}
									</FeaturePrice>
								</FeatureRow>
							)}
							{isPostcard && (
								<FeatureRow>
									<FeatureItem>Send as Postcard</FeatureItem>
									<FeaturePrice>${PRICES.postcard}</FeaturePrice>
								</FeatureRow>
							)}
							<FeatureRow total>
								<FeatureItem total>Total</FeatureItem>
								<FeaturePrice total>${totalPrice}</FeaturePrice>
							</FeatureRow>
						</PreviewSectionWrapper>
					)}
					<NavigationBottom>
						<BottomNavigationButton back onClick={this.onBack}>
							Back
						</BottomNavigationButton>
						<BottomNavigationButton onClick={this.onSubmit}>
							Next
						</BottomNavigationButton>
					</NavigationBottom>
				</SidePanel>
				<MainPanel>
					<ThankyouCardWrapper>
						{(image || customDesign || video) && (
							<RemoveMedia onClick={this.onRemoveMedia}>
								<svg
									width="16"
									height="19"
									viewBox="0 0 16 19"
									xmlns="http://www.w3.org/2000/svg"
								>
									<path
										d="M15.351 2.822h-3.639V1.354c0-.717-.58-1.298-1.297-1.298h-4.83c-.717 0-1.297.581-1.297 1.298v1.468H.648A.649.649 0 0 0 0 3.47v2.854c0 .359.29.649.649.649h.893v9.403c0 1.194.968 2.162 2.162 2.162h8.594a2.162 2.162 0 0 0 2.163-2.162V6.973h.89c.359 0 .649-.29.649-.649V3.47a.649.649 0 0 0-.649-.648zM5.585 1.354h4.83v1.468h-4.83V1.354zm7.567 15.022a.865.865 0 0 1-.864.865H3.704a.865.865 0 0 1-.865-.865V6.973h10.324l-.01 9.403zm1.55-10.7H1.298V4.119h13.406v1.557zM8 15.453a.649.649 0 0 1-.649-.649V9.211a.649.649 0 1 1 1.298 0v5.593c0 .359-.29.649-.649.649zm2.378 0a.649.649 0 0 1-.648-.649V9.211a.649.649 0 1 1 1.297 0v5.593c0 .359-.29.649-.649.649zm-4.756 0a.649.649 0 0 1-.649-.649V9.211a.649.649 0 1 1 1.297 0v5.593c0 .359-.29.649-.648.649z"
										fill="#ffffff"
										fillRule="nonzero"
									/>
								</svg>
							</RemoveMedia>
						)}
						{image && <Photo image={image.url} />}
						{customDesign && (
							<Photo
								image={`https://s3.amazonaws.com/www.thankyouspot.com/custom/${
									thankingState.customDesignImage
								}`}
							/>
						)}
						{video && (
							<VideoContainer>
								<Video src={thankingState.video.url} controls />
							</VideoContainer>
						)}
						<Card>
							<Intro
								value={thankingState.intro}
								onChange={this.onChangeIntro}
							/>
							<TextContainer>
								<TextScrollWrapper>
									<Text
										value={thankingState.text}
										onChange={this.onChangeText}
									/>
								</TextScrollWrapper>
							</TextContainer>
							<Closing
								value={thankingState.closing}
								onChange={this.onChangeClosing}
							/>
						</Card>
					</ThankyouCardWrapper>
				</MainPanel>
				<CustomDesignModal
					isOpen={thankingState.customDesignModalOpen}
					onRequestClose={this.onCloseCustomDesign}
				>
					<CustomDesignsPanel
						currentDesign={thankingState.customDesign}
						changeState={changeState}
					/>
				</CustomDesignModal>
				<VideoOptionsModal
					isOpen={thankingState.videoOptionsModalOpen}
					onRequestClose={this.onCloseVideoPanel}
					displayUploadError={this.displayUploadError}
				>
					<VideoOptionsPanel
						changeState={changeState}
						onAddVideo={this.onAddVideo}
					/>
				</VideoOptionsModal>
				<PhotoOptionsModal
					isOpen={thankingState.photoOptionsModalOpen}
					onRequestClose={this.onClosePhotoPanel}
					displayUploadError={this.displayUploadError}
				>
					<PhotoOptionsPanel
						changeState={changeState}
						onAddVideo={this.onAddVideo}
					/>
				</PhotoOptionsModal>
			</Wrapper>
		)
	}

	onRemoveMedia = _ => {
		swal({
			title: 'Delete Media',
			text: 'Are you sure you want to delete this?',
			icon: 'warning',
			type: 'warning',
			showCancelButton: true,
			showConfirmButton: true
		}).then(result => {
			if (result.value) {
				this.props.changeState('customDesign', null)
				this.props.changeState('customDesignImage', null)
				this.props.changeState('video', null)
				this.props.changeState('image', null)
			}
		})
	}

	onAddImage = imageFile => {
		const fileReader = new FileReader()

		fileReader.addEventListener('load', () => {
			const base64OfImage = fileReader.result

			this.props.changeState('customDesign', null)
			this.props.changeState('customDesignImage', null)
			this.props.changeState('video', null)
			this.props.changeState('paymentSource', null)
			this.props.changeState('paymentError', null)
			this.props.changeState('image', {
				file: imageFile,
				url: base64OfImage,
				type: imageFile.type
			})
		})

		fileReader.readAsDataURL(imageFile)
	}

	onAddVideo = videoFile => {
		const fileReader = new FileReader()

		fileReader.addEventListener('load', () => {
			const base64OfVideo = fileReader.result

			this.props.changeState('customDesign', null)
			this.props.changeState('customDesignImage', null)
			this.props.changeState('image', null)
			this.props.changeState('paymentSource', null)
			this.props.changeState('paymentError', null)
			this.props.changeState('video', {
				file: videoFile,
				url: base64OfVideo,
				type: videoFile.type
			})
		})

		fileReader.readAsDataURL(videoFile)
	}

	displayUploadError = errorMessage => {
		alert(errorMessage)
	}

	onChangeIntro = e => {
		const newText = this.textWithoutNewLine(e)
		const noDuplicatedComma = newText.replace(/,+/g, ',')
		this.props.changeState('intro', noDuplicatedComma)
	}

	onChangeText = e => {
		this.props.changeState('text', e.target.value)
	}

	onBack = _ => {
		this.props.history.push('/thank-someone')
	}

	onChangeClosing = e => {
		const newText = this.textWithoutNewLine(e)
		this.props.changeState('closing', newText)
	}

	onOpenPhotoPanel = e => {
		this.props.changeState('photoOptionsModalOpen', true)
	}

	onClosePhotoPanel = e => {
		this.props.changeState('photoOptionsModalOpen', false)
	}

	onOpenVideoPanel = e => {
		this.props.changeState('videoOptionsModalOpen', true)
	}

	onCloseVideoPanel = e => {
		this.props.changeState('videoOptionsModalOpen', false)
	}

	onOpenCustomDesign = e => {
		this.props.changeState('customDesignModalOpen', true)
	}

	onCloseCustomDesign = e => {
		this.props.changeState('customDesignModalOpen', false)
	}

	onClickSendPostcard = _ => {
		this.props.changeState('isPostcard', !this.props.thankingState.isPostcard)
	}

	onSubmit = e => {
		e.preventDefault()

		const canSubmit = !this.props.incompleteDetailsOrCard()

		if (canSubmit) {
			this.props.history.push('/thank-someone/preview')
		}
	}

	textWithoutNewLine = e => {
		const untilLastChar = e.target.value.length - 1
		const lastChar = e.target.value.charAt(untilLastChar)
		const newText = lastChar.match(/(\r|\n)/g)
			? e.target.value.slice(0, untilLastChar)
			: e.target.value

		return newText
	}
}

export default ThankYouDetails

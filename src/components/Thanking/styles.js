import { NavLink } from 'react-router-dom'
import styled, { css, injectGlobal } from 'styled-components'
import media from 'styled-media-query'

injectGlobal`
	.fade-enter {
		opacity: 0;
		z-index: 1;
	}

	.fade-enter.fade-enter-active {
		opacity: 1;
		transition: opacity 250ms ease-in;
	}
`

export const TabWrapper = styled.div`
	display: grid;
	grid-template-columns: auto 1020px auto;
	background-color: #ff7676;
	${media.between('medium', 'large')`
		margin-top: 90px;
		grid-template-columns: auto;
	`};
	${media.lessThan('medium')`
		margin-top: 0;
		grid-template-columns: auto auto auto;
		padding: 0;
	`};
`

export const Tabs = styled.div`
	grid-column: 2 / 3;
	padding: 20px 0;
	display: flex;
	justify-content: space-between;

	${media.between('medium', 'large')`
		grid-column: auto;
		padding: 20px 10%;
		margin-top: 40px;
	`};
`

export const Tab = styled(NavLink)`
	text-align: center;
	color: #fff;
	text-decoration: none;
	font-size: 23px;
	font-weight: 200;
	opacity: 0.5;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 400;
	-webkit-font-smoothing: antialiased;
	letter-spacing: 1px;
	&:hover {
		opacity: 0.85;
	}
	&.active {
		opacity: 1;

		&:hover {
			opacity: 1;
		}
	}
	${media.between('medium', 'large')`
		font-size: 18px;
		font-family: 'Josefin Sans', sans-serif;
		font-weight: 400;
	`};
	${media.lessThan('medium')`
		font-size: 12px;
		width: 100px;
		font-family: 'Josefin Sans', sans-serif;
		font-weight: 400;
	`};
`

export const DisabledTab = styled.span`
	text-align: center;
	color: #fff;
	text-decoration: none;
	font-size: 25px;
	font-weight: 200;
	opacity: 0.5;
`

export const TabNumber = styled.strong`
	font-size: 50px;
	font-weight: 900;
	text-shadow: 0 8px 0 #dd6161;
	margin-right: 15px;
	font-family: 'Paytone One', sans-serif;
	-webkit-font-smoothing: antialiased;
	letter-spacing: 1px;
	${media.between('medium', 'large')`
		font-size: 40px;
		display: block;
		margin-right: 0;
		margin-bottom: 10px
	`};
	${media.lessThan('medium')`
		display: block;
		font-size: 30px;
		margin-right: 0;
		margin-bottom: 10px
	`};
`

export const InfoInput = styled.input.attrs({
	disabled: true
})`
	padding: 15px;
	margin-bottom: 20px;
	font-family: 'Josefin Sans', sans-serif;
	font-size: 18px;
	font-weight: 400;
	border: none;
`

export const InfoLabel = styled.label`
	color: #184e68;
	font-size: 16px;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 400;
	text-align: left;
	margin-bottom: 5px;
`

export const MainPanel = styled.div`
	flex: 1;
	display: flex;
	flex-direction: row;
	align-items: center;
	justify-content: center;
	padding: 100px 30px;
	${media.between('medium', 'large')`
		padding: 70px 30px;
	`};
	${media.lessThan('medium')`
		padding: 30px 20px;
	`};
`
export const Features1stRow = styled.div`
	display: flex;
	text-align: center;
	flex-direction: column;
	padding: 20px;
	${media.between('medium', 'large')`
		display: grid;
		margin-bottom: 0;
		grid-template-columns: auto auto auto auto;
		padding: 0;
		width: 100%;
	`};
	${media.lessThan('medium')`
		padding: 20px 20px 0;
		width: 100%;
		margin-bottom: 0;
		display: grid;
		grid-template-columns: auto auto;
	`};
`
export const FeatureImage = styled.img`
	max-height: 55px;
	margin-right: 10px;
	${media.between('medium', 'large')`
		max-height: 50px;
	`};
	${media.lessThan('medium')`
		max-height: 50px;
		margin-right: 0;
	`};
`

export const FeatureLabel = styled.h3`
	color: #184e68;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 700;
	-webkit-font-smoothing: antialiased;
	font-size: 15px;
	margin: 0;
	padding-bottom: 20px;
	${media.between('medium', 'large')`
		padding-top: 5px;
		font-size: 12px;
	`};
	${media.lessThan('medium')`
		font-size: 10px;
		padding-top: 10px;
	`};
`
export const Rightside = styled.div`
	text-align: right;
`

export const Price = styled.h4`
	margin: 0;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 100i;
	font-style: italic;
	color: #184e68;
`

export const FeatureButton = styled.button`
	background: white;
	border-radius: 10px;
	border: 2px solid #dbeef7;
	width: 100%;
	padding: 10px 15px;
	margin: 8px 0;
	display: flex;
	align-items: center;
	flex: 1;
	justify-content: space-between;
	cursor: pointer;
	${({ hasMedia }) =>
		Boolean(hasMedia) &&
		css`
			border: 2px solid #ff7676;
		`};

	&:hover {
		border: 2px solid #ff7676;
	}
	${media.between('medium', 'large')`
		margin: 0;
	`};
	${media.lessThan('medium')`
		padding: 5px 10%;
		display: flex;
		flex-direction: row;
		margin: 0;
	`};
`
export const NavigationBottom = styled.div`
	width: 100%;
	display: flex;
	padding: 0 20px;
	justify-content: space-between;
	${props =>
		props.recipient &&
		css`
			padding: 0 30px;
		`} ${media.between('medium', 'large')`
			padding: 0 30px 20px;
		margin-top: 15px;
		width: auto;
	`};
	${media.lessThan('medium')`
		justify-content: center;
	`};
`
export const BottomNavigationButton = styled.button`
    border: none;
	border-radius: 60px;
	padding: 12px 20px;
	font-size: 18px;
	color: #fff;
	background-color: #184e68;
	cursor: pointer;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	width: auto;
	border: 1px solid #184e68;
	transition: all 0.2s ease;
	
	&:hover {
		background-color: transparent;
		color: #184e68;
		border: 1px solid #184e68;
	}
    &:nth-child(1) {
		
    }
	${props =>
		props.back &&
		css`
			color: #184e68;
			background: transparent;
			&:hover {
				background-color: #184e68;
				color: #fff;
			}
		`} 
    ${media.between('medium', 'large')`
        margin: auto 10px;
		max-width: 200px;
    `};
	${media.lessThan('medium')`
		margin: 20px 20px 0;
		max-width: 200px;
	`};
`

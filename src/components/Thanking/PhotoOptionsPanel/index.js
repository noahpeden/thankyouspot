import React, { Component } from 'react'
import { Header, Title, Wrapper } from './styles'
import CapturePhoto from './CapturePhoto'

export default class PhotoOptionsPanel extends Component {
	render() {
		const { changeState } = this.props
		return (
			<Wrapper>
				<Header>
					<Title>Add Photo</Title>
				</Header>
				<CapturePhoto
					changeState={Boolean(changeState) && changeState}
				/>
			</Wrapper>
		)
	}
}

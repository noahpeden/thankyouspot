import React from 'react'
import swal from 'sweetalert2'
import { FilePicker } from 'react-file-picker'
import { Apply, CTAWrapper, VideoWrapper } from '../styles'

const hasGetUserMedia = !!(
	navigator.getUserMedia ||
	navigator.webkitGetUserMedia ||
	navigator.mozGetUserMedia ||
	navigator.msGetUserMedia
)

function captureUserMedia(callback, errorCallback) {
	var params = {
		audio: false,
		video: true
	}

	navigator.getUserMedia(params, callback, errorCallback)
}

export default class CapturePhoto extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			capturedImageDataURL: null,
			src: null,
			stream: null,
			isCapturing: true,
			hasPermissions: false
		}
	}

	componentDidMount() {
		if (hasGetUserMedia) {
			this.requestUserMedia()
		}
	}

	componentDidUpdate() {
		const { stream, isCapturing } = this.state
		if (Boolean(stream) && isCapturing) {
			if (Boolean(this.refs.video)) {
				this.refs.video.play()
			}
		}
	}

	componentWillUnmount() {
		const { stream } = this.state
		this.stopStream(stream)
	}

	stopStream = stream => {
		if (Boolean(stream)) {
			const tracks = stream.getTracks()
			tracks.forEach(track => track.stop())
		}
	}

	requestUserMedia = () => {
		captureUserMedia(
			this.onSuccessCaptureUserMedia,
			this.onErrorCaptureUserMedia
		)
	}

	onSuccessCaptureUserMedia = stream => {
		this.setState({
			src: window.URL.createObjectURL(stream),
			stream,
			hasPermissions: true
		})
	}

	onErrorCaptureUserMedia = () => {
		this.setState({ hasPermissions: false })
	}

	takePicture = () => {
		const video = this.refs.video
		const videoWidth = video.clientWidth
		const videoHeight = video.clientHeight

		//create a canvas to draw image from media
		const canvas = document.createElement('canvas')
		canvas.width = videoWidth
		canvas.height = videoHeight
		const context = canvas.getContext('2d')

		//draw image to canvas and convet to dataURL
		context.drawImage(video, 0, 0, videoWidth, videoHeight)
		let dataURL = canvas.toDataURL('image/png')
		let imageFile = this.dataURLtoBlob(dataURL)

		this.setState({
			capturedImageDataURL: dataURL,
			isCapturing: false,
			imageFile
		})
	}

	retakePicture = () => {
		this.setState({ capturedImageDataURL: null, isCapturing: true })
	}

	uploadPhoto = imageFile => {
		const fr = new FileReader()
		fr.addEventListener('load', () => {
			const base64OfImage = fr.result
			this.setState({
				capturedImageDataURL: base64OfImage,
				isCapturing: false,
				imageFile
			})
		})
		fr.readAsDataURL(imageFile)
	}

	displayUploadError = errorMessage => {
		alert(errorMessage)
	}

	// Converts the dataurl file into a blob file⌐
	dataURLtoBlob = dataurl => {
		let arr = dataurl.split(','),
			mime = arr[0].match(/:(.*?);/)[1],
			bstr = atob(arr[1]),
			n = bstr.length,
			u8arr = new Uint8Array(n)
		while (n--) {
			u8arr[n] = bstr.charCodeAt(n)
		}
		return new File([u8arr], new Date().toISOString() + mime.split('/')[1], {
			type: mime
		})
	}

	selectPhoto = () => {
		const { imageFile, capturedImageDataURL: url, stream } = this.state
		this.props.changeState('customDesign', null)
		this.props.changeState('customDesignImage', null)
		this.props.changeState('video', null)
		this.props.changeState('paymentSource', null)
		this.props.changeState('paymentError', null)
		this.props.changeState('photoOptionsModalOpen', false)
		this.stopStream(stream)
		this.props.changeState('image', {
			file: imageFile,
			type: imageFile.type,
			url
		})
	}

	cancel = () => {
		this.stopStream(this.state.stream)
		this.props.changeState('photoOptionsModalOpen', false)
	}

	render() {
		const {
			src,
			capturedImageDataURL,
			isCapturing,
			hasPermissions
		} = this.state
		return (
			<div>
				{isCapturing ? (
					<React.Fragment>
						{!hasPermissions && (
							<p
								style={{
									textAlign: 'center',
									maxWidth: 400,
									margin: '20px auto'
								}}
							>
								You have not provided permission to access the camera. You can
								upload a photo instead.
							</p>
						)}
						<CTAWrapper>
							{hasPermissions && (
								<Apply className="takePicture" onClick={this.takePicture}>
									Take A Photo
								</Apply>
							)}
							<FilePicker
								extensions={['jpg', 'jpeg', 'png']}
								maxSize={3}
								onChange={this.uploadPhoto}
								onError={this.displayUploadError}
							>
								<Apply className="uploadPhoto">Upload Photo</Apply>
							</FilePicker>
						</CTAWrapper>
					</React.Fragment>
				) : (
					<React.Fragment>
						<CTAWrapper>
							<Apply className="retakePicture" onClick={this.retakePicture}>
								Change
							</Apply>
							<Apply onClick={this.selectPhoto}>Done</Apply>
						</CTAWrapper>
					</React.Fragment>
				)}
				{Boolean(capturedImageDataURL) ? (
					<VideoWrapper>
						<img src={capturedImageDataURL} alt="" />
					</VideoWrapper>
				) : hasPermissions ? (
					<VideoWrapper>
						<video ref="video" src={src}>
							Video stream not available.
						</video>
					</VideoWrapper>
				) : null}
				<Apply cancel className="cancel" onClick={this.cancel}>
					Cancel
				</Apply>
			</div>
		)
	}
}

import React from 'react'
import styled from 'styled-components'
import media from 'styled-media-query'
import Modal from 'react-modal'

function ReactModalAdapter({ className, modalClassName, ...props }) {
	return (
		<Modal className={modalClassName} portalClassName={className} {...props} />
	)
}

export const CustomDesignModal = styled(ReactModalAdapter).attrs({
	overlayClassName: 'modal-overlay',
	modalClassName: 'modal-content'
})`
	.modal-overlay {
		position: fixed;
		top: 0px;
		left: 0px;
		right: 0px;
		bottom: 0px;
		background-color: rgba(0, 0, 0, 0.75);
		padding: 20px;
		overflow: auto;

		${media.lessThan('medium')`
			padding: 0;
		`};
	}

	.modal-content {
		position: static;
		border: none;
		background: none;
		display: flex;
		flex-direction: column;
		font-family: gotham;
		overflow: initial;
		padding: 0;
		max-width: 700px;
		margin: 0 auto;

		&:focus {
			outline: none;
		}

		${media.lessThan('medium')`
			max-width: 100%;
		`};
	}
`

export const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
	background-color: #fff;
	box-shadow: 0 2px 42px -6px rgba(24, 78, 104, 0.45);
	padding: 20px;
`

export const Header = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	padding: 15px;
	border-bottom: 1px solid #efefef;
	margin-bottom: 15px;
`

export const Title = styled.h3`
	font-size: 24px;
	margin: 0;
	font-family: 'Concert One', cursive;
	-webkit-font-smoothing: antialiased;
	letter-spacing: 1px;

	${media.lessThan('medium')`
		font-size: 18px;
	`};
`

export const Apply = styled.button`
	cursor: pointer;
	background-color: #ff7676;
	color: #fff;
	border: 1px solid #ff7676;
	padding: 8px 16px;
	border-radius: 30px;
	font-family: 'Josefin Sans', sans-serif;
	transition: all 0.2s ease;
	&:hover {
		background: transparent;
		color: #ff7676;
	}
`

export const CustomDesignWrapper = styled.div`
	display: grid;
	grid-template-columns: 120px 120px 120px 120px;
	grid-gap: 25px;
	justify-content: center;
	flex: 1;

	${media.lessThan('large')`
		grid-template-columns: 120px 120px 120px;
		grid-gap: 10px;
	`};

	${media.lessThan('medium')`
		grid-template-columns: 120px 120px 120px;
		grid-gap: 5px;
	`};

	${media.lessThan('small')`
		grid-template-columns: 120px 120px;
		grid-gap: 5px;
	`};
`

export const DesignButton = styled.button`
	display: flex;
	flex-direction: column;
	border: none;
	cursor: pointer;
	align-items: center;
	background-color: ${props => (props.current ? '#dbeef7' : 'transparent')};
	padding: 15px;
	transition: transform 0.3s;
	outline: none;

	&:hover {
		transform: scale(1.05, 1.05);
	}
`

export const DesignImage = styled.img`
	max-height: 350px;
`

export const DesignName = styled.span`
	text-align: center;
	margin-top: 15px;
	font-size: 14px;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
`

import React, { Component } from 'react'
import { compose, graphql } from 'react-apollo'
import gql from 'graphql-tag'
import {
	CustomDesignWrapper,
	DesignButton,
	DesignImage,
	DesignName,
	Header,
	Apply,
	Title,
	Wrapper
} from './styles'

class CustomDesignsPanel extends Component {
	render() {
		const { customDesigns = [], currentDesign } = this.props

		return (
			<Wrapper>
				<Header>
					<Title>Custom Designs</Title>
					<Apply onClick={this.onApply}>Apply</Apply>
				</Header>
				<CustomDesignWrapper>
					<DesignButton
						onClick={this.onClearDesign}
						current={!currentDesign ? true : null}
					>
						<DesignImage
							src={require('../../../assets/images/no-custom-design.png')}
						/>
						<DesignName>None</DesignName>
					</DesignButton>
					{customDesigns.map(({ id, name, image }) => (
						<DesignButton
							key={id}
							onClick={this.onChooseDesign({ id, image })}
							current={currentDesign === id ? true : null}
						>
							<DesignImage
								src={`https://s3.amazonaws.com/www.thankyouspot.com/custom/thumb/${image}`}
								alt={name}
							/>
							<DesignName>{name}</DesignName>
						</DesignButton>
					))}
				</CustomDesignWrapper>
			</Wrapper>
		)
	}

	onClearDesign = () => {
		this.props.changeState('customDesign', null)
		this.props.changeState('customDesignImage', null)
	}

	onChooseDesign = ({ id, image }) => e => {
		e.preventDefault()

		this.props.changeState('image', null)
		this.props.changeState('video', null)
		this.props.changeState('paymentSource', null)
		this.props.changeState('paymentError', null)
		this.props.changeState('customDesign', id)
		this.props.changeState('customDesignImage', image)
	}

	onApply = () => {
		this.props.changeState('customDesignModalOpen', false)
	}
}

const CUSTOM_DESIGNS_QUERY = gql`
	query CustomDesignsQuery {
		allCustomDesigns(filter: { isPublished: true }) {
			id
			name
			image
		}
	}
`

export default compose(
	graphql(CUSTOM_DESIGNS_QUERY, {
		options: { fetchPolicy: 'cache-and-network' },
		props: ({ data: { allCustomDesigns } }) => ({
			customDesigns: allCustomDesigns
		})
	})
)(CustomDesignsPanel)

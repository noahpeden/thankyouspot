import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import NotFound from '../Redirect/NotFound'
import Preview from './Preview'
import RecipientDetails from './RecipientDetails'
import ThankYouDetails from './ThankYouDetails'
import { Tab, TabNumber, TabWrapper, Tabs } from './styles'
import swal from 'sweetalert2'

class Thanking extends Component {
	state = {
		intro: 'Dear ',
		text: 'Thank you ',
		closing: 'Love ',
		email: '',
		name: '',
		placeID: null,
		description: '',
		image: null,
		video: null,
		addressInput: '',
		saving: false,
		recipientType: 'FRIEND',
		customDesignModalOpen: false,
		videoOptionsModalOpen: false,
		photoOptionsModalOpen: false,
		customDesign: null,
		customDesignImage: null,
		paymentSource: null,
		paymentError: null,
		postcardAddress: 'USA',
		postcardAddressObj: {},
		isPostCard: false
	}

	render() {
		const { match, location } = this.props

		return (
			<React.Fragment>
				<TabWrapper>
					<Tabs>
						<Tab to={`${match.url}`} exact activeClassName="active">
							<TabNumber>1.</TabNumber>
							Choose a thank you type
						</Tab>
						<Tab
							onClick={this.onCreateTabClick}
							to={`${match.url}/write`}
							activeClassName="active"
						>
							<TabNumber>2.</TabNumber>
							Create your card
						</Tab>
						<Tab
							onClick={this.onPreviewTabClick}
							to={`${match.url}/preview`}
							activeClassName="active"
						>
							<TabNumber>3.</TabNumber>
							Preview and send
						</Tab>
					</Tabs>
				</TabWrapper>

				<Switch location={location}>
					<Route
						exact
						path={`${match.url}`}
						render={this.renderRecipientDetails}
					/>
					<Route
						exact
						path={`${match.url}/write`}
						render={this.renderThankYouDetails}
					/>
					<Route
						exact
						path={`${match.url}/preview`}
						render={this.renderPreview}
					/>
					<Route component={NotFound} />
				</Switch>
			</React.Fragment>
		)
	}

	onCreateTabClick = evt => {
		const cannotProceed = this.incompleteDetails()

		if (cannotProceed) evt.preventDefault()
	}

	onPreviewTabClick = evt => {
		const cannotProceed = this.incompleteDetailsOrCard()

		if (cannotProceed) evt.preventDefault()
	}

	incompleteDetailsOrCard = evt => {
		const incompleteDetails = this.incompleteDetails()
		const incompleteCard = this.incompleteCard()
		const incompleteDetailsOrCard = incompleteDetails || incompleteCard

		if (this.state.isPostcard && !this.hasImage()) {
			swal(
				'Error',
				'Add photo or select custom design before sending as postcard',
				'error'
			)
			return true
		}

		return incompleteDetailsOrCard
	}

	incompleteDetails = () => {
		const name = this.state.name.trim()
		const email = this.state.email.trim()
		const nameIsBlank = name === ''
		const emailIsBlank = email === ''
		const cannotProceedIfPerson = nameIsBlank || emailIsBlank

		const placeId = this.state.placeID
		const description = this.state.description.trim()
		const noAddress = placeId === null
		const descriptionIsBlank = description === ''
		const cannotProceedIfPlace = noAddress || descriptionIsBlank

		const recipientType = this.state.recipientType
		const recipientIsPerson = recipientType === 'FRIEND'

		const incompleteDetails = recipientIsPerson
			? cannotProceedIfPerson
			: cannotProceedIfPlace

		return incompleteDetails
	}

	incompleteCard = () => {
		const intro = this.state.intro.trim()
		const text = this.state.text.trim()
		const closing = this.state.closing.trim()
		const introIsBlank = intro === ''
		const textIsBlank = text === ''
		const closingIsBlank = closing === ''

		return introIsBlank || textIsBlank || closingIsBlank
	}

	hasImage = _ => {
		const { image, customDesignImage } = this.state
		return image || customDesignImage
	}

	renderRecipientDetails = ({ history }) => {
		return (
			<RecipientDetails
				thankingState={this.state}
				changeState={this.changeState}
				history={history}
				incompleteDetails={this.incompleteDetails}
			/>
		)
	}

	renderThankYouDetails = ({ history }) => {
		return (
			<ThankYouDetails
				thankingState={this.state}
				changeState={this.changeState}
				history={history}
				incompleteDetails={this.incompleteDetails}
				incompleteDetailsOrCard={this.incompleteDetailsOrCard}
			/>
		)
	}

	renderPreview = ({ history }) => {
		return (
			<Preview
				thankingState={this.state}
				changeState={this.changeState}
				history={history}
				incompleteDetailsOrCard={this.incompleteDetailsOrCard}
			/>
		)
	}

	changeState = (stateName, stateValue) => {
		const newState = {
			[stateName]: stateValue
		}

		this.setState(newState)
	}
}

export default Thanking

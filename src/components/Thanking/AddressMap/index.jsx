import React, { Component } from 'react'
import { geocodeByPlaceId, getLatLng } from 'react-places-autocomplete'
import { StaticMap } from './styles'
const { REACT_APP_GOOGLE_MAPS_API_KEY } = process.env

class AddressMap extends Component {
	state = {
		lat: null,
		lng: null,
		loading: true
	}

	componentDidMount() {
		geocodeByPlaceId(this.props.placeID)
			.then(results => getLatLng(results[0]))
			.then(({ lat, lng }) => this.setState({ lat, lng }))
	}

	render() {
		const { lat, lng } = this.state

		if (!lat || !lng) return null

		return (
			<StaticMap
				src={`https://maps.googleapis.com/maps/api/staticmap?format=jpg&zoom=10&scale=2&size=250x250&markers=color:red|${lat},${lng}&key=${REACT_APP_GOOGLE_MAPS_API_KEY}`}
			/>
		)
	}
}

export default AddressMap

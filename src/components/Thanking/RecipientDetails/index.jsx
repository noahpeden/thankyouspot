import React, { Component } from 'react'
import { MainPanel, InfoLabel } from '../styles'
import {
	Wrapper,
	SidePanel,
	SidePanelHeading,
	Form,
	FormTitle,
	TextInput,
	SubmitButton,
	RecipientType,
	RecipientTypeLabel,
	RecipientSideWrapper,
	RecipientImage
} from './styles'
import AddressSearch from './AddressSearch'

class RecipientDetails extends Component {
	render() {
		const { thankingState } = this.props

		return (
			<Wrapper>
				<SidePanel>
					<RecipientSideWrapper>
						<SidePanelHeading>Choose a thank you type</SidePanelHeading>
						<RecipientType
							active={thankingState.recipientType === 'FRIEND'}
							onClick={this.onClickFriendRecipient}
						>
							<RecipientImage
								src={require('../../../assets/images/thankyou-mail.svg')}
							/>
							<RecipientTypeLabel>Thank somebody you know</RecipientTypeLabel>
						</RecipientType>
						<RecipientType
							active={thankingState.recipientType === 'ADDRESS'}
							onClick={this.onClickAdressRecipient}
						>
							<RecipientImage
								src={require('../../../assets/images/thankyou-stranger.svg')}
							/>
							<RecipientTypeLabel>Thank a stranger</RecipientTypeLabel>
						</RecipientType>
					</RecipientSideWrapper>
				</SidePanel>
				<MainPanel>
					<Form onSubmit={this.onSubmit}>
						<FormTitle>Recipient Details</FormTitle>
						{thankingState.recipientType === 'FRIEND' ? (
							<React.Fragment>
								<InfoLabel>Name</InfoLabel>
								<TextInput
									type="text"
									name="name"
									placeholder="Name"
									value={thankingState.name}
									onChange={this.onChangeName}
								/>
								<InfoLabel>Email</InfoLabel>
								<TextInput
									type="email"
									name="email"
									placeholder="Email"
									value={thankingState.email}
									onChange={this.onChangeEmail}
								/>
							</React.Fragment>
						) : (
							<React.Fragment>
								<InfoLabel>Address</InfoLabel>
								<AddressSearch
									value={thankingState.addressInput}
									onChange={this.onChangeAddressInput}
									onSelectAddress={this.onSelectAddress}
								/>
								<InfoLabel>Description</InfoLabel>
								<TextInput
									type="text"
									name="description"
									placeholder="Person at the coffee shop"
									value={thankingState.description}
									onChange={this.onChangeDescription}
								/>
							</React.Fragment>
						)}
						<SubmitButton type="submit" value="Create your card" />
					</Form>
				</MainPanel>
			</Wrapper>
		)
	}

	onClickFriendRecipient = e => {
		e.preventDefault()

		this.props.changeState('recipientType', 'FRIEND')
	}

	onClickAdressRecipient = e => {
		e.preventDefault()

		this.props.changeState('recipientType', 'ADDRESS')
	}

	onChangeName = e => {
		this.props.changeState('name', e.target.value)
	}

	onChangeEmail = e => {
		this.props.changeState('email', e.target.value)
	}

	onChangeAddressInput = input => {
		this.props.changeState('addressInput', input)
	}

	onSelectAddress = (address, placeID) => {
		this.props.changeState('placeID', placeID)
	}

	onChangeDescription = e => {
		this.props.changeState('description', e.target.value)
	}

	onSubmit = e => {
		e.preventDefault()

		const canSubmit = !this.props.incompleteDetails()

		if (canSubmit) {
			this.props.history.push('/thank-someone/write')
		}
	}
}

export default RecipientDetails

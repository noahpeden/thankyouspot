import styled from 'styled-components'
import media from 'styled-media-query'

export const RecipientSideWrapper = styled.div`
	flex: 1;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	${media.between('medium', 'large')`
		flex-direction: row;
	`};
	${media.lessThan('medium')`
		flex-direction: row;
	`};
`

export const RecipientType = styled.a.attrs({
	href: ''
})`
	display: flex;
	flex-direction: column;
	padding: 20px;
	align-items: center;
	text-decoration: none;
	width: 200px;
	margin-top: 20px;
	border: 1px solid #dbeef7;
	&:hover {
		border: 1px solid #ff7676;
	}
	color: ${props => (props.active ? '#ff7676' : '#3a3d3f')};
	${media.between('medium', 'large')`
		margin: 0 10px;
		width: auto;
		padding: 5px;
		flex-direction: row;
	`};
	${media.lessThan('medium')`
		margin: 0 10px;
		width: auto;
		padding: 5px;
		flex-direction: row;
	`};
`

export const RecipientImage = styled.img`
	margin-bottom: 15px;
	height: 100px;
	${media.between('medium', 'large')`
		height: 50px;
		margin-right: 15px;
		margin-bottom: 0;
	`};
	${media.lessThan('medium')`
		height: 50px;
		margin-bottom: 0;
		margin-right: 15px;
	`};
`

export const RecipientTypeLabel = styled.span`
	font-size: 18px;
	text-align: center;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	${media.between('medium', 'large')`
		font-family: 'Josefin Sans', sans-serif;
		-webkit-font-smoothing: antialiased;
		font-weight: 400;
	`};
	${media.lessThan('medium')`
		font-family: 'Josefin Sans', sans-serif;
		-webkit-font-smoothing: antialiased;
		font-weight: 400;
		font-size: 14px;
	`};
`

export const Form = styled.form`
	width: 440px;
	padding: 40px;
	background-color: #dbeef7;
	display: flex;
	flex-direction: column;
	-webkit-font-smoothing: antialiased;
	${media.lessThan('medium')`
		padding: 30px 15px;
	`};
`

export const FormTitle = styled.h5`
	color: #184e68;
	font-family: 'Josefin Sans', sans-serif;
	font-size: 27px;
	font-weight: 700;
	margin: 0 0 20px;
	${media.lessThan('medium')`
		text-align: center;
	`};
`

export const TextInput = styled.input`
	padding: 20px;
	margin-bottom: 20px;
	border: none;
	outline: none;
	border-bottom: 2px solid transparent;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 400;
	transition: all 0.2s ease;
	&:hover,
	&:focus {
		background: #f8fbfe;
		border-bottom: 2px solid #ff7675;
	}
`

export const SubmitButton = styled.input`
	align-self: flex-start;
	border: none;
	border-radius: 30px;
	padding: 17px 30px 15px;
	font-size: 18px;
	color: #fff;
	background-color: #184e68;
	cursor: pointer;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	border: 1px solid #184e68;
	transition: all 0.2s ease;
	}
	&:hover {
		background-color: transparent;
		color: #184e68;
		border: 1px solid #184e68;
	}
	${media.lessThan('medium')`
		margin: 0 auto;
	`};
`
export const Wrapper = styled.div`
	display: flex;
	${media.between('medium', 'large')`
		flex-direction: column;
	`};
	${media.lessThan('medium')`
		flex-direction: column;
	`};
`
export const SidePanel = styled.div`
	width: 250px;
	background-color: #dbeef7;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	padding: 30px;
	${media.between('medium', 'large')`
		width: auto;
		padding: 10px 10%;
		height: 95px;
	`};
	${media.lessThan('medium')`
		width: auto;
		padding: 10px 5px;
		height: 95px;
	`};
`
export const SidePanelHeading = styled.h2`
	color: #184e68;
	font-size: 26px;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	text-align: center;
	margin: 0;
	${media.between('medium', 'large')`
		display: none;
	`};
	${media.lessThan('medium')`
		display: none;
	`};
`

import React, { Component } from 'react'
import PlacesAutocomplete from 'react-places-autocomplete'

import { TextInput } from '../styles'
import {
	Wrapper,
	DropdownWrapper,
	Suggestion,
	SuggestionText,
	SuggestionIcon
} from './styles'

class AddressSearch extends Component {
	render() {
		const { value, onChange, onSelectAddress, searchOptions = {} } = this.props

		return (
			<PlacesAutocomplete
				value={value}
				onChange={onChange}
				onSelect={onSelectAddress}
				searchOptions={searchOptions}
			>
				{RenderPlaces}
			</PlacesAutocomplete>
		)
	}
}

const RenderPlaces = ({
	getInputProps,
	getSuggestionItemProps,
	suggestions
}) => {
	return (
		<Wrapper>
			<TextInput placeholder="Where" {...getInputProps()} />
			<DropdownWrapper>
				{suggestions.map(suggestion => (
					<Suggestion
						active={suggestion.active}
						{...getSuggestionItemProps(suggestion)}
					>
						<SuggestionIcon />
						<SuggestionText>{suggestion.description}</SuggestionText>
					</Suggestion>
				))}
			</DropdownWrapper>
		</Wrapper>
	)
}

export default AddressSearch

import styled from 'styled-components'

const pinDropdown = require('../../../../assets/images/pin-dropdown.png')

export const Wrapper = styled.div`
	position: relative;
	display: flex;
	flex-direction: column;
`

export const DropdownWrapper = styled.div`
	position: absolute;
	left: 0;
	right: 0;
	top: 100%;
	margin-top: -20px;
	background-color: #fff;
	border: 1px solid #f2f2f2;
	border-width: 0 1px 1px;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 300;
`

export const Suggestion = styled.div`
	display: flex;
	border-top: 1px solid #f2f2f2;
	padding: 5px 20px;
	cursor: pointer;
	background-color: ${props => (props.active ? '#f2f2f2' : 'transparent')};

	&:hover {
		background-color: #f2f2f2;
	}
`

export const SuggestionIcon = styled.span`
	width: 11px;
	height: 17px;
	background-image: url(${pinDropdown});
	background-size: contain;
	margin-top: 2px;
	margin-right: 10px;
`

export const SuggestionText = styled.span`
	flex: 1;
	font-size: 14px;
	line-height: 1.5em;
	color: #3a3d3f;
`

import Chance from 'chance'
import gql from 'graphql-tag'
import React, { Component } from 'react'
import { compose, graphql, withApollo } from 'react-apollo'
import Swal from 'sweetalert2'
import { Redirect } from 'react-router-dom'
import { Photo, VideoContainer, Video } from '../../ThankYouCard/styles'
import AddressMap from '../AddressMap'
import { InfoInput, MainPanel, InfoLabel } from '../styles'
import {
	Card,
	Closing,
	Intro,
	Text,
	SidePanel,
	SidePanelHeading,
	Wrapper,
	FeatureItem,
	FeatureRow,
	FeaturePrice,
	PreviewSectionWrapper,
	ThankyouCardWrapper,
	Address,
	Address1,
	Address2,
	Address3,
	StreetAddressInput,
	AptInput,
	CityInput,
	RegionInput,
	PostalInput,
	CountryInput,
	AddressNote
} from './styles'
import CheckoutForm from '../../PaymentForm'
import { TextContainer, TextScrollWrapper } from '../ThankYouDetails/styles'
import { BottomNavigationButton, NavigationBottom } from '../styles'

const PRICES = {
	video: 1.49,
	customDesign: 0.99,
	postcard: 1.99
}

const PREVIEW_STATE = {
	recipientDetails: 0,
	paymentDetails: 1
}

const chance = new Chance()
const fileTypes = {
	quicktime: 'mov'
}
const extensionFromType = type => {
	const extracted = type.split('/')[1]

	return fileTypes[extracted] || extracted
}
const generateFilename = type =>
	`${chance.guid()}-${Date.now()}.${extensionFromType(type)}`

function stripHTML(htmlString) {
	const tmp = document.createElement('div')
	tmp.innerHTML = htmlString
	return tmp.textContent
}

const AllPostsDesc = gql`
	query AllPostsDesc {
		allPosts(
			first: 9
			skip: 0
			orderBy: createdAt_DESC
			filter: { OR: [{ name_contains: "" }, { email_contains: "" }] }
		) {
			id
			intro
			text
			closing
			image
			name
			description
			createdAt
			address {
				name
			}
			author {
				id
				name
			}
			recipient {
				id
				name
			}
			customDesign {
				id
				image
			}
		}
	}
`

class Preview extends Component {
	constructor(props) {
		super(props)

		this.setStreet = ref => (this.streetRef = ref)
		this.setApt = ref => (this.aptRef = ref)
		this.setCity = ref => (this.cityRef = ref)
		this.setRegion = ref => (this.regionRef = ref)
		this.setPostal = ref => (this.postalRef = ref)
		this.setCountry = ref => (this.countryRef = ref)

		this.state = {
			previewState: PREVIEW_STATE.recipientDetails
		}
	}

	render() {
		const { thankingState, changeState } = this.props
		const {
			image,
			video,
			customDesign,
			isPostcard,
			postcardAddressObj = {}
		} = thankingState
		const hasPaidAddons = video || customDesign || isPostcard
		const totalPrice = hasPaidAddons
			? isPostcard
				? PRICES.postcard
				: (video && PRICES.video) || (customDesign && PRICES.customDesign)
			: 0

		if (this.props.incompleteDetailsOrCard()) {
			return <Redirect to="/thank-someone" />
		}

		return (
			<Wrapper>
				<SidePanel>
					{this.state.previewState === PREVIEW_STATE.recipientDetails && (
						<React.Fragment>
							<PreviewSectionWrapper>
								<SidePanelHeading>Recipient Details</SidePanelHeading>
								{thankingState.recipientType === 'FRIEND' ? (
									<React.Fragment>
										<InfoLabel>Name</InfoLabel>
										<InfoInput value={thankingState.name} />
										<InfoLabel>Email</InfoLabel>
										<InfoInput value={thankingState.email} />
									</React.Fragment>
								) : (
									<React.Fragment>
										<InfoInput value={thankingState.description} />
										<AddressMap placeID={thankingState.placeID} />
									</React.Fragment>
								)}
							</PreviewSectionWrapper>
							{isPostcard && (
								<Address>
									<SidePanelHeading>Recipient Address</SidePanelHeading>
									<Address1>
										<StreetAddressInput
											placeholder="Address"
											innerRef={this.setStreet}
											defaultValue={postcardAddressObj.street}
											onChange={this.onChangeAddressInput('street')}
										/>
										<AptInput
											placeholder="Apt"
											innerRef={this.setApt}
											defaultValue={postcardAddressObj.apt}
											onChange={this.onChangeAddressInput('apt')}
										/>
									</Address1>
									<Address2>
										<CityInput
											placeholder="City"
											innerRef={this.setCity}
											defaultValue={postcardAddressObj.city}
											onChange={this.onChangeAddressInput('city')}
										/>
										<RegionInput
											placeholder="State"
											innerRef={this.setRegion}
											defaultValue={postcardAddressObj.region}
											onChange={this.onChangeAddressInput('region')}
										/>
										<PostalInput
											placeholder="Postal"
											innerRef={this.setPostal}
											defaultValue={postcardAddressObj.postal}
											onChange={this.onChangeAddressInput('postal')}
										/>
									</Address2>
									<Address3>
										<CountryInput
											placeholder="Country"
											disabled={true}
											value="USA"
											innerRef={this.setCountry}
										/>
									</Address3>
									<AddressNote>
										Please note that you can only send postcards in the USA
										currently.
									</AddressNote>
								</Address>
							)}
						</React.Fragment>
					)}
					{this.state.previewState === PREVIEW_STATE.paymentDetails &&
						hasPaidAddons && (
							<React.Fragment>
								<PreviewSectionWrapper breakdown>
									<SidePanelHeading>Breakdown</SidePanelHeading>
									{image && (
										<FeatureRow>
											<FeatureItem>Photo</FeatureItem>
											<FeaturePrice>Free</FeaturePrice>
										</FeatureRow>
									)}
									{video && (
										<FeatureRow>
											<FeatureItem>Video</FeatureItem>
											<FeaturePrice>${PRICES.video}</FeaturePrice>
										</FeatureRow>
									)}
									{customDesign && (
										<FeatureRow>
											<FeatureItem>Custom Design</FeatureItem>
											<FeaturePrice>
												{!isPostcard ? `$${PRICES.customDesign}` : 'Free'}
											</FeaturePrice>
										</FeatureRow>
									)}
									{isPostcard && (
										<FeatureRow>
											<FeatureItem>Send as Postcard</FeatureItem>
											<FeaturePrice>${PRICES.postcard}</FeaturePrice>
										</FeatureRow>
									)}
									<FeatureRow total>
										<FeatureItem total>Total</FeatureItem>
										<FeaturePrice total>${totalPrice}</FeaturePrice>
									</FeatureRow>
								</PreviewSectionWrapper>
								<CheckoutForm
									ref={checkoutForm => (this.checkoutForm = checkoutForm)}
									thankingState={thankingState}
									changeState={changeState}
								/>
								{/*isPostcard && (
									<AddressSearch
										value={thankingState.postcardAddressInput}
										onChange={this.onChangePostcardAddressInput}
										onSelectAddress={this.onSelectPostcardAddress}
										searchOptions={{
											componentRestrictions: { country: 'us' }
										}}
									/>
								)*/}
							</React.Fragment>
						)}
					<NavigationBottom recipient>
						<BottomNavigationButton back onClick={this.onBack}>
							Back
						</BottomNavigationButton>
						{this.state.previewState !== PREVIEW_STATE.recipientDetails ||
						!hasPaidAddons ? (
							<BottomNavigationButton
								onClick={this.onSend}
								disabled={thankingState.saving}
							>
								{thankingState.saving ? 'Sending...' : 'Send Thank You'}
							</BottomNavigationButton>
						) : (
							<BottomNavigationButton onClick={this.onNext}>
								Next
							</BottomNavigationButton>
						)}
					</NavigationBottom>
				</SidePanel>
				<MainPanel>
					<ThankyouCardWrapper>
						{thankingState.image && <Photo image={thankingState.image.url} />}
						{thankingState.customDesign && (
							<Photo
								image={`https://s3.amazonaws.com/www.thankyouspot.com/custom/${
									thankingState.customDesignImage
								}`}
							/>
						)}
						{thankingState.video && (
							<VideoContainer>
								<Video src={thankingState.video.url} controls />
							</VideoContainer>
						)}
						<Card>
							<Intro>{stripHTML(thankingState.intro)}</Intro>
							<TextContainer>
								<TextScrollWrapper>
									<Text>{stripHTML(thankingState.text)}</Text>
								</TextScrollWrapper>
							</TextContainer>
							<Closing>{stripHTML(thankingState.closing)}</Closing>
						</Card>
					</ThankyouCardWrapper>
				</MainPanel>
			</Wrapper>
		)
	}

	getPostcardAddress = _ => {
		return `${this.streetRef.value || ''} ${this.aptRef.value || ''} ${this
			.cityRef.value || ''} ${this.regionRef.value || ''} ${this.postalRef
			.value || ''} ${this.countryRef.value || ''}`.trim()
	}

	onSend = async () => {
		const {
			intro,
			text,
			closing,
			email,
			name,
			placeID,
			description,
			recipientType,
			image,
			video,
			customDesign,
			isPostcard,
			postcardAddress
		} = this.props.thankingState
		const hasPaidAddons = video || customDesign || isPostcard
		const { authInfo, client: apolloClient } = this.props
		let paymentSource = null

		if (!authInfo.token && hasPaidAddons) {
			const response = await Swal({
				type: 'warning',
				title: 'Login required',
				text: 'You need to sign up or log in before completing your purchase.',
				showCloseButton: true,
				confirmButtonText: 'Sign up',
				cancelButtonText: 'Log in',
				showCancelButton: true
			})

			if (response.value) {
				this.props.history.push('/auth/signup', { modal: true })
			} else if (response.dismiss && response.dismiss === 'cancel') {
				this.props.history.push('/auth/login', { modal: true })
			}

			return
		} else if (!authInfo.token && !hasPaidAddons) {
			const response = await Swal({
				type: 'question',
				title: 'Send as Anonymous?',
				text: `Sign up or log in so your thank you won't be sent as Anonymous.`,
				showCloseButton: true,
				confirmButtonText: 'Sign up or<br /> Log in',
				cancelButtonText: 'Send as<br /> Anonymous',
				showCancelButton: true,
				reverseButtons: true
			})

			if (response.value) {
				this.props.history.push('/auth/signup', { modal: true })

				return
			} else if (!response.dismiss || response.dismiss !== 'cancel') {
				return
			}
		}

		if (hasPaidAddons) {
			const { token, error } = await this.checkoutForm.createToken()

			if (token) {
				paymentSource = token.id
			} else if (error) {
				console.log(error)
				this.props.changeState('paymentError', error)
				this.props.changeState('saving', false)

				return
			}
		}

		let imageUrl = null
		let videoUrl = null

		this.props.changeState('saving', true)

		try {
			if (image || video) {
				const isImage = Boolean(image)
				const mediaFile = image || video
				const filetype = mediaFile.type
				const mediaFolder = isImage ? 'images' : 'videos'
				const filename = `${mediaFolder}/${generateFilename(filetype)}`

				const s3LinkResponse = await apolloClient.query({
					query: GET_S3_UPLOAD_URL_QUERY,
					variables: { filename, filetype }
				})

				const s3Link = s3LinkResponse.data.getS3Url.url

				await fetch(s3Link, {
					body: mediaFile.file,
					method: 'PUT',
					headers: {
						'Content-Type': filetype
					}
				})

				const s3PublicLink = `https://s3.amazonaws.com/www.thankyouspot.com/${filename}`

				if (isImage) {
					imageUrl = s3PublicLink
				} else {
					videoUrl = s3PublicLink
				}
			}
		} catch (err) {
			console.log(err)
			this.props.changeState('saving', false)

			return
		}

		const variables =
			recipientType === 'FRIEND'
				? {
						intro,
						text,
						closing,
						email,
						name,
						image: imageUrl,
						video: videoUrl,
						customDesignID: customDesign,
						paymentSource,
						postcardAddress,
						isPostcard
				  }
				: {
						intro,
						text,
						closing,
						placeID,
						description,
						image: imageUrl,
						video: videoUrl,
						customDesignID: customDesign,
						paymentSource,
						postcardAddress,
						isPostcard
				  }

		try {
			const options = {
				variables,
				optimisticResponse: {
					__typename: 'Mutation',
					savePost: {
						__typename: 'Post',
						id: Math.round(Math.random() * -1000000) + '',
						...variables,
						createdAt: new Date()
					}
				},
				update: (store, { data: { savePost } }) => {
					try {
						const queryData = store.readQuery({ query: AllPostsDesc })
						store.writeQuery({
							query: AllPostsDesc,
							data: {
								allPosts: [...queryData.allPosts, savePost]
							}
						})
					} catch (err) {}
				}
			}

			await this.props.savePost(options)
			this.props.history.push('/global-thank-yous')
		} catch (e) {
			// TODO: Handle error
			console.error(e)
		} finally {
			this.props.changeState('saving', false)
		}
	}

	onChangePostcardAddressInput = input => {
		this.props.changeState('postcardAddressInput', input)
	}

	onSelectPostcardAddress = (_, placeID) => {
		this.props.changeState('postCardAddress', placeID)
	}

	onChangeAddressInput = field => async evt => {
		const { changeState, thankingState } = this.props
		const postcardAddressObj = Object.assign(
			{},
			thankingState.postcardAddressObj
		)

		postcardAddressObj[field] = evt.target.value
		changeState('postcardAddressObj', postcardAddressObj)
		changeState('postcardAddress', this.getPostcardAddress())
	}

	onBack = _ => {
		if (this.state.previewState === PREVIEW_STATE.recipientDetails) {
			this.props.history.push('/thank-someone/write')
		} else {
			this.setState({ previewState: PREVIEW_STATE.recipientDetails })
		}
	}

	onNext = async _ => {
		const {
			video,
			customDesign,
			isPostcard,
			postcardAddress
		} = this.props.thankingState
		const hasPaidAddons = video || customDesign || isPostcard
		const { authInfo, history } = this.props

		if (isPostcard && postcardAddress.trim() === 'USA') {
			Swal({
				type: 'error',
				title: 'Recipient Address required',
				text: 'We need to know where to send the postcard to.'
			})

			return
		}

		if (!authInfo.token && hasPaidAddons) {
			const response = await Swal({
				type: 'warning',
				title: 'Login required',
				text: 'You need to sign up or log in before completing your purchase.',
				showCloseButton: true,
				confirmButtonText: 'Sign up',
				cancelButtonText: 'Log in',
				showCancelButton: true
			})

			if (response.value) {
				history.push('/auth/signup', { modal: true })
			} else if (response.dismiss && response.dismiss === 'cancel') {
				history.push('/auth/login', { modal: true })
			}

			return
		}

		this.setState({ previewState: PREVIEW_STATE.paymentDetails })
	}
}

const GET_S3_UPLOAD_URL_QUERY = gql`
	query GetS3Url($filename: String!, $filetype: String!) {
		getS3Url(filename: $filename, filetype: $filetype) {
			url
		}
	}
`

const AUTH_INFO_QUERY = gql`
	query AuthInfo {
		authInfo @client {
			userID
			token
		}
	}
`

const SAVE_POST_MUTATION = gql`
	mutation SavePost(
		$intro: String!
		$text: String!
		$closing: String!
		$email: String
		$name: String
		$placeID: String
		$description: String
		$image: String
		$video: String
		$customDesignID: ID
		$paymentSource: String
		$postcardAddress: String
		$isPostcard: Boolean
	) {
		savePost(
			intro: $intro
			text: $text
			closing: $closing
			email: $email
			name: $name
			placeId: $placeID
			description: $description
			image: $image
			video: $video
			customDesignId: $customDesignID
			paymentSource: $paymentSource
			postcardAddress: $postcardAddress
			isPostcard: $isPostcard
		) {
			id
		}
	}
`

export default compose(
	graphql(SAVE_POST_MUTATION, {
		props: ({ mutate }) => ({
			savePost: options => {
				return mutate(options)
			}
		})
	}),
	graphql(AUTH_INFO_QUERY, {
		props: ({ data: { authInfo } }) => ({
			authInfo
		})
	}),
	withApollo
)(Preview)

import styled, { css } from 'styled-components'
import media from 'styled-media-query'

export const ThankyouCardWrapper = styled.div`
	box-shadow: 0 2px 20px -5px rgba(24, 79, 105, 0.5);
	display: flex;
	flex-direction: row;
	position: relative;
	${media.lessThan('960px')`
		flex-direction: column;
	`};
	${media.lessThan('460px')`
		width: 100%;
	`};
`

export const Card = styled.div`
	display: flex;
	flex-direction: column;
	width: 445px;
	height: 650px;
	padding: 50px 50px 40px;
	${media.lessThan('460px')`
		width: 100%;
	`};
`

export const Intro = styled.h3`
	color: #184e68;
	font-size: 45px;
	text-align: center;
	font-family: 'Concert One', cursive;
	-webkit-font-smoothing: antialiased;
	letter-spacing: 1px;
	margin: 0;
	word-break: break-word;
`

export const Text = styled.p`
	text-align: center;
	font-size: 27px;
	color: #3a3d3f;
	display: flex;
	flex: 1;
	align-items: center;
	justify-content: center;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 300;
	-webkit-font-smoothing: antialiased;
	margin: 0;
`

export const Closing = styled.p`
	text-align: center;
	font-size: 45px;
	color: #6eaecc;
	font-family: 'Caveat', cursive;
	font-weight: 400;
	margin: 0;
	word-break: break-word;
`

export const SubmitButton = styled.button`
	border: none;
	border-radius: 30px;
	padding: 17px 30px 15px;
	font-size: 18px;
	color: #fff;
	background-color: #184e68;
	cursor: pointer;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	margin: 20px auto 0;
	border: 1px solid #184e68;
	transition: all 0.2s ease;
	&:hover {
		background-color: transparent;
		color: #184e68;
		border: 1px solid #184e68;
	}
	${media.between('medium', 'large')`
		margin: 20px auto;
	`};
	${media.lessThan('medium')`
		margin: 10px auto 0;
	`};
`

export const Wrapper = styled.div`
	display: flex;
	${media.between('medium', 'large')`
		flex-direction: column;
	`};
	${media.lessThan('medium')`
		flex-direction: column;
	`};
`

export const SidePanel = styled.div`
	width: 350px;
	background-color: #dbeef7;
	display: flex;
	flex-direction: column;
	padding: 0 0 30px;
	${media.between('medium', 'large')`
		width: auto;
		padding: 10px 20%;
	`};
	${media.lessThan('medium')`
		width: auto;
	`};
`

export const SidePanelHeading = styled.h2`
	color: #184e68;
	font-size: 26px;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	text-align: left;
	margin: 0 0 10px;
	${media.between('medium', 'large')`
		padding: 20px 0;
	`};
	${media.lessThan('medium')`
		padding: 0 0 20px;
	`};
`

export const PreviewSectionWrapper = styled.div`
	display: flex;
	flex-direction: column;
	padding: 30px 30px 25px;
	margin-bottom: 25px;
	${media.between('medium', 'large')`
		margin-bottom: 0;
	`};
	${props =>
		props.breakdown &&
		css`
			background: #c7dfec;
			padding: 30px;
		`};
`

export const FeatureRow = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	padding: 10px 0;
	border-bottom: 1px solid #b3cbd8;
	&:last-child {
		border-bottom: 0;
	}
	&:nth-last-child(2) {
		border-bottom: 2px solid #89a7b7;
	}
	${props =>
		props.total &&
		css`
			align-items: center;
		`};
`

export const FeatureItem = styled.h3`
	margin: 0;
	color: #184e68;
	font-size: 18px;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 500;
	text-align: left;
	${props =>
		props.total &&
		css`
			font-size: 22px;
			font-weight: 700;
		`};
`

export const FeaturePrice = styled.p`
	margin: 0;
	color: #184e68;
	font-size: 18px;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 400;
	text-align: right;
	${props =>
		props.total &&
		css`
			background: #fff;
			padding: 10px;
			font-size: 22px;
			font-weight: 700;
			text-shadow: 0 4px 0 #dbeef7;
		`};
`
export const Address = styled.div`
	padding: 0 30px 0;
`
export const Address1 = styled.div`
	display: flex;
	flex-direction: row;
	align-items: flex-end;
`
export const Address2 = styled.div`
	display: flex;
	flex-direction: row;
	align-items: flex-end;
`

export const Address3 = styled.div`
	display: flex;
	flex-direction: row;
	align-items: flex-end;
`

export const AddressNote = styled.label`
	color: #6e90a0;
	font-size: 15px;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 400;
	text-align: left;
	margin-bottom: 5px;
`

export const StreetAddressInput = styled.input`
	font-family: 'Josefin Sans', sans-serif;
	flex: 4;
	overflow: hidden;
	background-color: #fff;
	margin: 10px 5px 10px 0;
	padding: 15px;
	cursor: text;
	border: 0;
	border-bottom: 2px solid transparent;
	transition: all 0.2s ease;
	&:hover,
	&:focus {
		background: #f8fbfe;
		border-bottom: 2px solid #ff7675;
	}
`

export const AptInput = styled.input`
	font-family: 'Josefin Sans', sans-serif;
	flex: 1;
	overflow: hidden;
	background-color: #fff;
	margin: 10px 0 10px 5px;
	padding: 15px;
	cursor: text;
	border: 0;
	border-bottom: 2px solid transparent;
	transition: all 0.2s ease;
	&:hover,
	&:focus {
		background: #f8fbfe;
		border-bottom: 2px solid #ff7675;
	}
`

export const CityInput = styled.input`
	font-family: 'Josefin Sans', sans-serif;
	overflow: hidden;
	background-color: #fff;
	margin: 10px 5px 10px 0;
	padding: 15px;
	cursor: text;
	border: 0;
	border-bottom: 2px solid transparent;
	transition: all 0.2s ease;
	&:hover,
	&:focus {
		background: #f8fbfe;
		border-bottom: 2px solid #ff7675;
	}
`

export const RegionInput = styled.input`
	font-family: 'Josefin Sans', sans-serif;
	overflow: hidden;
	background-color: #fff;
	margin: 10px 5px 10px 5px;
	padding: 15px;
	cursor: text;
	border: 0;
	border-bottom: 2px solid transparent;
	transition: all 0.2s ease;
	&:hover,
	&:focus {
		background: #f8fbfe;
		border-bottom: 2px solid #ff7675;
	}
`

export const PostalInput = styled.input`
	font-family: 'Josefin Sans', sans-serif;
	overflow: hidden;
	background-color: #fff;
	margin: 10px 0 10px 5px;
	padding: 15px;
	cursor: text;
	border: 0;
	border-bottom: 2px solid transparent;
	transition: all 0.2s ease;
	&:hover,
	&:focus {
		background: #f8fbfe;
		border-bottom: 2px solid #ff7675;
	}
`

export const CountryInput = styled.input`
	font-family: 'Josefin Sans', sans-serif;
	flex: 1;
	overflow: hidden;
	background-color: #fff;
	margin: 10px 0 10px;
	padding: 15px;
	cursor: text;
	border: 0;
	border-bottom: 2px solid transparent;
	transition: all 0.2s ease;
	&:hover,
	&:focus {
		background: #f8fbfe;
		border-bottom: 2px solid #ff7675;
	}
`

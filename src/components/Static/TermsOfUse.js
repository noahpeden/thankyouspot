import React from 'react'
import {
	TermsWrapper,
	Header,
	SubHeader,
	SectionContent,
	SecondarySubHeader,
	ListItem
} from './styles'

// GOOGLE DOC COPY https://docs.google.com/document/d/1Q7QkChQqfKoezrEEXoDmuv_bO4peHh4JabvkyAEHD9w/edit?ts=5b99c1b0#heading=h.hdylnkxx8160

const TermsOfUse = () => (
	<TermsWrapper>
		<Header>Thank You Spot Terms and Conditions & Privacy Policy</Header>
		<SectionContent>
			By using or accessing ThankYouSpot.com or any ThankYouSpot operated
			websites you acknowledge that you have read, understood, and agree to be
			bound by the terms and conditions of this privacy policy. If you do not
			agree to all of the terms of this privacy policy, your sole and exclusive
			remedy is to discontinue use of the website.
		</SectionContent>
		<SubHeader>How Our Website Works</SubHeader>
		<SectionContent>
			ThankYouSpot is a web-app that allows users to thank other people and
			users anywhere, anytime, at any place. We also allow our users to enhance
			their thank you’s with purchased add ons such as videos, custom designs,
			and sending the thank
		</SectionContent>
		<SubHeader>Data & Information Privacy Policies</SubHeader>
		<SecondarySubHeader>Our Commitment</SecondarySubHeader>
		<SectionContent>
			ThankYouSpot ("ThankYouSpot") collects certain data and information
			about our users ("You") in order to develop and improve various features
			and functions of any ThankYouSpot operated website ("Site"). Personal
			information includes, but is not limited to, your first and last name,
			birth date, and your email address ("Personal Information"). User data
			includes, but is not limited to, your browser type, device, IP address,
			and session statistics (“User Data”).
		</SectionContent>
		<SectionContent>
			ThankYouSpot is committed to respecting your privacy and recognizes your
			need for appropriate protection and management of any Personal Information
			You share with us. ThankYouSpot established this privacy policy ("Privacy
			Policy") so that You can understand the care with which ThankYouSpot will
			treat your Personal Information, including how we collect, share, store,
			and utilize data.
		</SectionContent>
		<SubHeader>How We Collect User Data</SubHeader>
		<SecondarySubHeader>Use of Cookies</SecondarySubHeader>
		<SectionContent>
			If you are browsing our Site, you are anonymous to us, but we will still
			collect User Data, like browser type, device, and IP address, during your
			session through the use of web analytics tracking services. This User Data
			is collected for all ThankYouSpot visitors, is non-personally
			identifiable, and is used to manage, monitor, and track usage to improve
			our website’s services. Once this data is collected, it is stored in our
			Google Analytics Account indefinitely.
		</SectionContent>
		<SectionContent>
			If you reside in the European Union, a Cookies Acceptance Banner will
			appear the first time you visit our Site. Once you accept this Cookies
			policy, all future sessions on our Site will utilize Cookies and collect
			User Data, unless you manually disable Cookies in your browser.
		</SectionContent>
		<SectionContent>
			If you voluntarily sign up for a ThankYouSpot account and provide
			ThankYouSpot with Personal Information, when you are logged in we store
			certain User Data from your session using cookies to make higher quality
			recommendations based on your interests. To delete your ThankYouSpot
			account and all associated data that is personally identifiable, please
			submit this request form.
		</SectionContent>
		<SectionContent>
			If you voluntarily provide Personal Information to us through any forms,
			tools, or services on our Site, you will no longer be anonymous to us.
			Your User Data and Personal Information will be collected and stored in
			our database as specified below. To delete the Personal Information you’ve
			voluntarily shared with us via any form, tool, or service on our Site or
			withdraw your consent, please submit this request form.
		</SectionContent>
		<SecondarySubHeader>Disclosure of User Data</SecondarySubHeader>
		<SectionContent>
			ThankYouSpot may share aggregated Site usage information gathered from
			Cookies or other services in a non-personally identifiable manner with
			partners and other third parties in order to create more efficient,
			targeted advertising, products, and services. We promise this User Data
			will be non-personally identifiable.
		</SectionContent>
		<SubHeader>
			How We Collect, Utilize, & Share Users’ Personal Information
		</SubHeader>
		<SectionContent>
			Except as otherwise stated in this privacy policy, ThankYouSpot will not
			disclose Personal Information collected through our Site to any third
			party unless we believe that disclosure is necessary: (1) to conform to
			legal requirements or to respond to a subpoena, search warrant or other
			legal process received by ThankYouSpot, whether or not a response is
			required by applicable law; (2) to enforce the ThankYouSpot Terms of
			Service or to protect our rights; or (3) to protect the safety of members
			of the public and users of ThankYouSpot.
		</SectionContent>
		<SecondarySubHeader>Thank You Spot Account</SecondarySubHeader>
		<SectionContent>
			If you voluntarily create a ThankYouSpot Account on our Site, your
			Personal Information will be collected and stored in our database
			indefinitely, unless we deem it necessary to delete your account. By
			checking the Consent Checkbox, you willingly give ThankYouSpot consent to
			collect and store your Personal Information, and share it with our
			internal team in order to provide you with more accurate recommendations
			or improve our services. We will not share the Personal Information
			associated with your ThankYouSpot Account directly to any third parties or
			outside entities, unless you provide us with written consent via email to
			do so or you use the Online Advisor Tool within your ThankYouSpot Account.
			If you wish to delete your ThankYouSpot Account from our database or
			withdraw your consent, please submit this request form.
		</SectionContent>
		<SecondarySubHeader>Contact Us Form</SecondarySubHeader>
		<SectionContent>
			If you voluntarily submit any Personal Information through the Contact Us
			Form on our Site, the content of your inquiry will be shared with relevant
			ThankYouSpot team members directly so they can follow up with you to
			provide detailed information and answer your questions. By checking the
			Consent Checkbox on a Provider Inquiry Form, you willingly give
			ThankYouSpot consent to collect your Personal Information and share it
			with our internal team. We will not share the Personal Information you
			submit through the Contact Us Form with any third parties or outside
			entities, unless you provide us with written consent via email to do so.
			The Personal Information you submit is not stored in our database.
		</SectionContent>
		<SectionContent>
			If you have any questions about how your Personal Information is collected
			or shared, please email info@ThankYouSpot.com.
		</SectionContent>
		<SubHeader>How We Safely Store User Data & Personal Information</SubHeader>
		<SectionContent>
			We use industry standard measures to protect all information that is
			stored on our servers and within our database. We limit access to this
			information to those employees who need access to perform their job
			function
		</SectionContent>
		<SectionContent>
			If you have any questions about the security of our website, please{' '}
			<a href="/contact-us">contact us</a>
		</SectionContent>
		<Header>Email Marketing Policies</Header>
		<SubHeader>Strict adherence to CAN-SPAM Act of 2003</SubHeader>
		<SectionContent>
			Just like you, we hate spam. We have never sent spam or junk emails and
			our mailing practice strictly conforms to the CAN-SPAM Act of 2003.
			However, if you willingly provide ThankYouSpot with your contact
			information / email address on our Site, we will send notices from time to
			time to your inbox based on the type of form you used to submit your
			Personal Information. This permission is expressed in our Consent Checkbox
			and specific terms and conditions below.
		</SectionContent>
		<SecondarySubHeader>Newsletter</SecondarySubHeader>
		<SectionContent>
			ThankYouSpot sends regular newsletters to all Users and Providers that
			subscribe through our website or other online email marketing signup
			forms. These newsletters were created to ensure we provide the best
			possible service to our community. Subscribing to our newsletter also
			automatically puts you on our regular mailing list, which shares
			intermittent updates (i.e. contests) on company-wide announcements to our
			community. All our newsletter subscribers Personal Information is
			collected and stored in the Mailchimp email marketing platform. We never
			share our subscriber lists with third parties or outside entities, so all
			subscriber Personal Information is kept confidential to our internal team.
		</SectionContent>

		{/* TO DO ADD UNSUBSCRIBE LINKS */}
		<SectionContent>
			If you would like to unsubscribe to our newsletter, click here. If you
			would like to subscribe to our newsletter, you can do so here.
		</SectionContent>
		<SubHeader>Thank You Spot Email Notifications</SubHeader>
		<SectionContent>
			ThankYouSpot sends independent email notifications to ThankYouSpot users
			once they have voluntarily created an account and consented to our
			ThankYouSpot Terms of Service. These notifications are sent at various
			intervals and help ThankYouSpot users navigate their account, learn about
			important updates and opportunities, and get the most out of the tool.
		</SectionContent>
		<SectionContent>
			Additionally, all email addresses associated with a ThankYouSpot account
			are automatically added to our mailing list, which shares intermittent
			updates (i.e. contests) on company-wide announcements to our community.
		</SectionContent>
		<SubHeader>Thank You Spot Contests</SubHeader>
		<SectionContent>
			ThankYouSpot sends individualized electronic notifications to entrants of
			any contest hosted on ThankYouSpot.com at various times throughout the
			year. Each contest on our Site has a unique set of terms and conditions
			that all entrants should review prior to giving consent and submitting an
			entry. The email addresses of all entrants are automatically added to our
			Newsletter subscriber list, which means their Personal Information is
			shared and stored in the Mailchimp email marketing platform. Entering a
			ThankYouSpot contest also automatically puts you on our regular mailing
			list, which shares intermittent updates (i.e. contests) on company-wide
			announcements to our community. We never share our subscriber lists with
			third parties or outside entities, unless specified clearly in the terms
			and conditions of the contest, so all subscriber Personal Information is
			kept confidential to our internal team.
		</SectionContent>
		<SectionContent>
			If you would like to unsubscribe from ThankYouSpot’s subscriber list, you
			can do so by clicking on the unsubscribe link in any email campaign, or
			click here.
		</SectionContent>
		<SubHeader>Contact Us Form</SubHeader>
		<SectionContent>
			Users can voluntarily contact the ThankYouSpot team for any reason through
			our Contact Us Form. When you submit a Contact Us inquiry on our Site, the
			content of your inquiry will be shared with relevant ThankYouSpot team
			members directly so they can follow up with you to provide detailed
			information and answer your questions. To stop receiving emails resulting
			from your form submission, please inform the team member who contacts you
			directly.
		</SectionContent>
		<Header>Video & Image Submission Policies</Header>
		<SubHeader>License</SubHeader>
		<SectionContent>
			When submitting videos and images to ThankYouSpot, you grant us, without
			any compensation of any kind to you or others, the right and license to
			use, copy, distribute, display, publish, perform, sell, sublicense,
			modify, edit, adapt, translate, transmit, created derivative works from,
			and otherwise exploit all Images and Videos in any form, medium or
			technology that we elect, whether now or in the future. Without limiting
			the foregoing, you agree that, without further approval from you, we may
			exercise the rights you grant us herein for any and all purposes we deem
			appropriate, including, without limitation, for the promotion, marketing,
			and publicizing of our services and products. The right and license you
			grant us is perpetual, irrevocable, royalty-free, unrestricted, and
			worldwide.
		</SectionContent>
		<SectionContent>
			By voluntarily submitting images and video to ThankYouSpot through any
			form, contest, or tool, you affirm, represent and warrant to us:
		</SectionContent>
		<ol>
			<ListItem>You are at least 18 years old.</ListItem>
			<ListItem>All User Information submitted is true and accurate.</ListItem>
			<ListItem>
				You are not impersonating any person or falsely stated or misrepresented
				your affiliation with a person or entity.{' '}
			</ListItem>
			<ListItem>
				You own or have all necessary licenses, rights, consents, and
				permissions to use and submit to us the Images and User Information and
				to authorize us to use the Images and User Information as expressed in
				these Terms and Conditions.{' '}
			</ListItem>
			<ListItem>
				Your agreement to these Terms and Conditions does not in any way
				conflict with any existing legal agreement or commitment on your part to
				any third party.{' '}
			</ListItem>
			<ListItem>
				Your images and User Information do not embody, reflected or reveal any
				confidential information and that we are not obligated to treat them as
				confidential.{' '}
			</ListItem>
		</ol>

		<Header>Miscellaneous Policies</Header>
		<SectionContent>
			These Terms and Conditions and any other legal notices published by
			ThankYouSpot on the Site, shall constitute the entire agreement between
			you and ThankYouSpot concerning the Site, your use of ThankYouSpot, and
			consumption of any content. If any provision of these Terms is deemed
			invalid by a court of competent jurisdiction, the invalidity of such
			provision shall not affect the validity of the remaining provisions of
			these Terms, which shall remain in full force and effect. No waiver of any
			one provision set forth in these Terms shall be deemed a further or
			continuing waiver of such provision or any other provision, and
			ThankYouSpot’s failure to assert or enforce any right or provision under
			these Terms shall not constitute a waiver of such right or provision.
		</SectionContent>
		<SecondarySubHeader>External Links</SecondarySubHeader>
		<SectionContent>
			Our provision of a link to any other website or location is for your
			convenience and does not signify our endorsement of other websites or
			location or its contents. We have no control over, do not review, and
			cannot be responsible for these external websites or their content. Please
			be aware that the terms of our Privacy Policy do not apply to these third
			party websites. ThankYouSpot is not liable or responsible for any
			communications that result from your visit to these external websites.
		</SectionContent>
		<SecondarySubHeader>Changes to this Privacy Policy</SecondarySubHeader>
		<SectionContent>
			ThankYouSpot reserves the right to change, modify, add, or remove portions
			of this Policy at any time. If we make any material changes, a prominent
			notice will be posted on our website. You should visit this page from time
			to time to review the current Privacy Policy and Terms & Conditions,
			because all of the content on this page is binding when you become a User
			of ThankYouSpot.
		</SectionContent>
		<SectionContent>
			If you object to any such changes or any of the policies outlined above,
			you may cease using our Site. Continued use of ThankYouSpot following
			notice of any such changes shall indicate your acknowledgment of such
			changes and agreement to be bound by the terms and conditions of such
			changes.
		</SectionContent>
		<SectionContent>
			If you have any questions about these Terms & Conditions, please
			{/* TO DO ADD IN CONTACT US */} <a href="/contact-us">contact us</a>.
		</SectionContent>
	</TermsWrapper>
)

export default TermsOfUse

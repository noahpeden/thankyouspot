import styled from 'styled-components'

import media from 'styled-media-query'

export const TermsWrapper = styled.div`
	width: 800px;
	margin: 60px auto;
	padding: 40px 0;
	${media.between('768px', '1170px')`
        margin: 140px auto 60px;
    `};
	${media.lessThan('840px')`
        width: 100%;
        padding: 30px;
    `};
`

export const Header = styled.h1`
	color: #184e68;
	font-size: 32px;
	text-shadow: 0 4px 0 #e5cad0;
	text-align: left;
	margin: 0;
	padding-bottom: 10px;
	font-family: 'Paytone One', sans-serif;
	-webkit-font-smoothing: antialiased;
	letter-spacing: 1px;
`

export const SubHeader = styled.h2`
	font-size: 20px;
	color: #184e68;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 700;
	-webkit-font-smoothing: antialiased;
	margin-top: 50px;
	text-transform: uppercase;
`

export const SecondarySubHeader = styled.h3`
	font-size: 18px;
	color: #184e68;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 700;
	-webkit-font-smoothing: antialiased;
`

export const SectionContent = styled.p`
	font-size: 18px;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 300;
	-webkit-font-smoothing: antialiased;
	color: #3a3d3f;
	line-height: 1.5;
`

export const ListItem = styled.li`
	font-size: 18px;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 300;
	-webkit-font-smoothing: antialiased;
	color: #3a3d3f;
	margin: 8px 0 0;
`

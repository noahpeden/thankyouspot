import React from 'react'
import {
	AuthorAvatar,
	AuthorLink,
	CardContainer,
	CardDetailsFrom,
	CardDetailsTo,
	DeleteButton,
	Person,
	Content,
	ContentWrapper,
	Intro,
	Outro,
	ProfileLink,
	ReportButton,
	ThankYou,
	ThankYouVideoContainer,
	ThankYouVideoOverlay,
	ThankYouVideo,
	ThankYouLink,
	ThankYouContainer,
	ContentScrollWrapper,
	DetailsWrapper
} from './styles'

// import TimeAgo from 'javascript-time-ago'
// import en from 'javascript-time-ago/locale/en'
import LazyLoadPhoto from '../LazyLoad/LazyLoadPhoto'

const playButton = require('../../assets/playButton.svg')

// Initialize TimeAgo
// TimeAgo.locale(en)
// const timeAgo = new TimeAgo('en-US')
const NOOP = () => {}

const ThankYouCard = ({
	post,
	showTimestamp = true,
	showNames = true,
	isLoggedInUserPost,
	archivePost = NOOP,
	reportThankYou = NOOP,
	postIndex = null
}) => {
	const thankYouUrl = post.recipient
		? `/${post.recipient.id}/thank-yous/${post.id}`
		: `/global-thank-yous/${post.id}`

	const hasImage =
		('image' in post && post.image !== null) ||
		('customDesign' in post && post.customDesign !== null)

	const hasVideo = 'video' in post && post.video !== null
	const hasMedia = hasImage || hasVideo

	return (
		<ThankYouContainer hasMargin={showTimestamp && showNames}>
			<ThankYouLink to={{ pathname: thankYouUrl, state: { modal: true } }}>
				<CardContainer hasMedia={hasMedia}>
					{hasImage && (
						<LazyLoadPhoto
							thankYouId={post.id}
							isCard
							src={
								post.image ||
								`https://s3.amazonaws.com/www.thankyouspot.com/custom/${
									post.customDesign.image
								}`
							}
						/>
					)}
					{hasVideo && (
						<ThankYouVideoContainer>
							<ThankYouVideo>
								<source src={post.video} />
							</ThankYouVideo>
							<ThankYouVideoOverlay>
								<img src={playButton} alt="Play Button" />
							</ThankYouVideoOverlay>
						</ThankYouVideoContainer>
					)}
					<ThankYou hasMedia={hasMedia}>
						{!isLoggedInUserPost && !post.reported ? (
							<ReportButton
								onClick={evt => {
									evt.stopPropagation()
									evt.preventDefault()
									reportThankYou(post.id)
								}}
							>
								<svg
									width="16"
									height="19"
									viewBox="0 0 16 19"
									xmlns="http://www.w3.org/2000/svg"
								>
									<path
										d="M.001 18.191c0 .335.269.607.6.607.331 0 .6-.272.6-.607V9.904h4v1.82c0 .317.286.606.6.606h9.6c.225 0 .444-.14.541-.344a.626.626 0 0 0-.072-.642l-2.9-3.663 2.9-3.664a.626.626 0 0 0 .072-.641.613.613 0 0 0-.541-.344h-5.8V1.617a.627.627 0 0 0-.6-.606h-7.8V.809a.603.603 0 0 0-.6-.607c-.331 0-.6.272-.6.607V18.19zm1.2-15.968h7.2v6.468h-7.2V2.223zm8.4 2.022h4.55l-2.419 3.057a.63.63 0 0 0 0 .758l2.419 3.057h-7.75V9.904h2.6c.314 0 .6-.289.6-.606V4.245z"
										fill="#184E68"
										fillRule="nonzero"
									/>
								</svg>
							</ReportButton>
						) : null}
						{isLoggedInUserPost && !!archivePost ? (
							<DeleteButton
								onClick={evt => {
									evt.stopPropagation()
									evt.preventDefault()
									archivePost(post.id, postIndex)
								}}
							>
								<svg
									width="16"
									height="19"
									viewBox="0 0 16 19"
									xmlns="http://www.w3.org/2000/svg"
								>
									<path
										d="M15.351 2.822h-3.639V1.354c0-.717-.58-1.298-1.297-1.298h-4.83c-.717 0-1.297.581-1.297 1.298v1.468H.648A.649.649 0 0 0 0 3.47v2.854c0 .359.29.649.649.649h.893v9.403c0 1.194.968 2.162 2.162 2.162h8.594a2.162 2.162 0 0 0 2.163-2.162V6.973h.89c.359 0 .649-.29.649-.649V3.47a.649.649 0 0 0-.649-.648zM5.585 1.354h4.83v1.468h-4.83V1.354zm7.567 15.022a.865.865 0 0 1-.864.865H3.704a.865.865 0 0 1-.865-.865V6.973h10.324l-.01 9.403zm1.55-10.7H1.298V4.119h13.406v1.557zM8 15.453a.649.649 0 0 1-.649-.649V9.211a.649.649 0 1 1 1.298 0v5.593c0 .359-.29.649-.649.649zm2.378 0a.649.649 0 0 1-.648-.649V9.211a.649.649 0 1 1 1.297 0v5.593c0 .359-.29.649-.649.649zm-4.756 0a.649.649 0 0 1-.649-.649V9.211a.649.649 0 1 1 1.297 0v5.593c0 .359-.29.649-.648.649z"
										fill="#184E68"
										fillRule="nonzero"
									/>
								</svg>
							</DeleteButton>
						) : null}
						<Intro>{post.intro}</Intro>
						<ContentWrapper>
							<ContentScrollWrapper>
								<Content>{post.text}</Content>
							</ContentScrollWrapper>
						</ContentWrapper>
						{post.author ? (
							<AuthorLink to={`/${post.author.id}`}>
								<AuthorAvatar
									src={
										post.author.picture ||
										require('../../assets/images/default-avatar.svg')
									}
								/>
							</AuthorLink>
						) : null}
						<Outro>{post.closing}</Outro>
					</ThankYou>
				</CardContainer>
			</ThankYouLink>
			{/* {showTimestamp && (
					<CardDetailsCreatedAt>{getCardAge(post)}</CardDetailsCreatedAt>
				)} */}
			{showNames && (
				<React.Fragment>
					<DetailsWrapper>
						<CardDetailsTo>
							{getTo(post)}: <Person>{getRecipient(post)}</Person>
						</CardDetailsTo>
						<CardDetailsFrom>
							{getFrom(post)}: <Person>{getAuthor(post)}</Person>
						</CardDetailsFrom>
					</DetailsWrapper>
				</React.Fragment>
			)}
		</ThankYouContainer>
	)
}

function getRecipient(post) {
	const { recipient, name, description } = post

	if (recipient) {
		return (
			<ProfileLink to={{ pathname: `/${recipient.id}` }}>
				{recipient.name}
			</ProfileLink>
		)
	} else if (description && description.trim() !== '') {
		return description
	} else {
		return name
	}
}

function getAuthor(post) {
	const { author, address } = post

	if (author) {
		return (
			<ProfileLink to={{ pathname: `/${author.id}` }}>
				{author.name}
			</ProfileLink>
		)
	} else if (address) {
		return address.name
	} else {
		return 'Anonymous'
	}
}

function getFrom(post) {
	let ret = 'From'
	const { address, author } = post
	if (author) {
		return ret
	} else if (address) {
		ret = `At`
	}
	return ret
}

function getTo(_) {
	return `To`
}

// function getCardAge(post) {
// 	const dateCreated = new Date(post.createdAt)
// 	return timeAgo.format(dateCreated)
// }

export default ThankYouCard

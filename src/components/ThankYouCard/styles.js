import { Link } from 'react-router-dom'
import styled, { css } from 'styled-components'
import media from 'styled-media-query'

export const ProfileLink = styled(Link)`
	color: #184e68;
	font-family: 'Josefin Sans', sans-serif;
	text-decoration: none;
`

export const CardDetailsCreatedAt = styled.p`
	color: #184e68;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 300;
	font-size: 12px;
	text-decoration: none;
	margin-bottom: 5px;
`

export const DetailsWrapper = styled.div`
	width: 80%;
	margin-top: 10px;
	margin-bottom: 8px;
`

export const CardDetailsFrom = styled.p`
	color: #184e68;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 300;
	text-decoration: none;
	margin: 0;
`
export const CardDetailsTo = styled.p`
	color: #184e68;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 300;
	text-decoration: none;
	margin: 0;
`
export const ReportButton = styled.button`
	position: absolute;
	bottom: -40px;
	right: 0;
	padding-top: 20px;
	border: none;
	outline: none;
	background: transparent;
	cursor: pointer;
	transition: all 0.2s ease;
	visibility: hidden;
	&:hover {
		path {
			fill: #ff7676;
		}
	}
`
export const DeleteButton = styled.button`
	position: absolute;
	bottom: -40px;
	right: 0;
	padding-top: 20px;
	border: none;
	outline: none;
	background: transparent;
	cursor: pointer;
	transition: all 0.2s ease;
	visibility: hidden;
	&:hover {
		path {
			fill: #ff7676;
		}
	}
`
export const Person = styled.span`
	color: #184e68;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 500;
	text-decoration: none;
	margin-bottom: 0;
	a {
		text-decoration: none;
		&:hover {
			color: #ff7676;
		}
	}
`

export const Photo = styled.div`
	width: 445px;
	height: 650px;
	background: url('${props => props.image}');
	background-size: cover;
	background-position: center;
	display: flex;
	${media.lessThan('460px')`
		width: 100%;
	`};
`

export const VideoContainer = styled.div`
	width: 445px;
	height: 650px;
	box-shadow: 0 2px 42px -6px #184e68;
	display: flex;
`

export const Video = styled.video`
	width: 100%;
`

export const ThankYouContainer = styled.div`
	height: 350px;
	margin-bottom: 30px;
	perspective: 500;
	${({ hasMargin }) =>
		hasMargin &&
		css`
			margin-bottom: 70px;
		`};
`

export const ThankYouLink = styled(Link)`
	text-decoration: none;
	height: 100%;
	width: 100%;
	position: relative;
	perspective: 500;
	display: block;
	outline: none;
`

export const CardContainer = styled.div`
	position: absolute;
	height: 100%;
	width: 100%;
	transition: transform 1s;
	transform-style: preserve-3d;
	display: flex;

	${ThankYouLink}:hover & {
		${props =>
			props.hasMedia
				? `transform: rotateY(180deg); transition: transform 0.5s;`
				: ''};
	}
`

export const ThankYou = styled.div`
	position: absolute;
	font-family: 'Josefin Sans', sans-serif;
	display: flex;
	align-items: center;
	flex-direction: column;
	padding: 20px;
	height: 100%;
	width: 100%;
	box-shadow: 0 2px 14px -5px rgba(24, 79, 105, 0.43);
	background-color: white;
	backface-visibility: hidden;
	${props => (props.hasMedia ? `transform: rotateY(180deg);` : '')};
	&:hover {
		button {
			visibility: visible;
		}
	}
`

export const ThankYouImage = styled.div`
	position: absolute;
	padding: 20px;
	height: 100%;
	width: 100%;
	box-shadow: 0 2px 14px -5px rgba(24, 79, 105, 0.43);
	backface-visibility: hidden;
	background-size: cover;
	background-position: center;
	background-color: #fff;
	${props => ('src' in props ? `background-image: url(${props.src});` : '')};
`

export const ThankYouVideoContainer = styled.div`
	position: absolute;
	height: 100%;
	width: 100%;
	box-shadow: 0 2px 14px -5px rgba(24, 79, 105, 0.43);
	backface-visibility: hidden;
	overflow: hidden;
`

export const ThankYouVideo = styled.video`
	position: absolute;
	top: 0;
	left: 0;
	height: 100%;
	width: 100%;
	object-fit: cover;
`

export const ThankYouVideoOverlay = styled.div`
	position: absolute;
	top: 0;
	left: 0;
	background-color: rgba(24, 78, 104, 0.6);
	height: 100%;
	width: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
`

export const Intro = styled.h5`
	color: #184e68;
	font-weight: 900;
	font-size: 25px;
	text-align: center;
	margin: 0;
	font-family: 'Concert One', cursive;
	-webkit-font-smoothing: antialiased;
	letter-spacing: 1px;
	word-break: break-word;
`

export const ContentWrapper = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	flex: 1;
	overflow: hidden;
	width: 100%;
`

export const ContentScrollWrapper = styled.div`
	overflow: auto;
	margin: 16px -70px 16px 0;
	padding-right: 70px;
	padding-left: 15px;
`

export const Content = styled.p`
	margin: 0;
	color: #3a3d3f;
	font-size: 15px;
	text-align: center;
	line-height: 1.6em;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 300;
	word-break: break-word;
	-webkit-font-smoothing: antialiased;
`

export const AuthorLink = styled(Link)``

export const AuthorAvatar = styled.img`
	display: block;
	width: 50px;
	height: 50px;
	min-height: 50px;
	border-radius: 50%;
	border: 4px solid #ff7676;
	background-color: #ff7676;
	object-fit: cover;
`

export const Outro = styled.h5`
	font-family: 'Caveat', cursive;
	font-size: 45px;
	font-weight: 400;
	font-size: 29px;
	color: #6eaecc;
	text-align: center;
	margin: 15px 0 0;
	word-break: break-word;
`

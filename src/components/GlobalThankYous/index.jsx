import gql from 'graphql-tag'
import React from 'react'
import { compose, graphql } from 'react-apollo'
import ThankYouCard from '../ThankYouCard'
import Swal from 'sweetalert2'
import {
	BodyWrapper,
	ButtonContainer,
	FilterContainer,
	FilterInput,
	SeeMoreButton,
	ThankYouList
} from './styles'

class GlobalThankYous extends React.Component {
	async componentDidUpdate(prevProps) {
		this.loggedInUser = await localStorage.getItem('tys-userid')
		if (prevProps.query !== this.props.query) {
			this.props.filter(this.props.query)
		}
	}

	reportThankYou = postId => {
		Swal({
			title: 'Is this Thank You inappropriate?',
			type: 'warning',
			text: 'Please tell us why you are reporting it.',
			input: 'text',
			showCancelButton: true,
			showConfirmButton: true,
			confirmButtonText: 'Report'
		}).then(async result => {
			if (!!result.value) {
				let response = await this.props.reportThankYou({
					variables: {
						id: postId,
						message: result.value
					}
				})
				if (!!response.data) {
					Swal({
						title: 'ThankYou Card Reported.',
						text:
							'Thank you for your feedback. We will check on this as soon as possible',
						type: 'success'
					})
				}
				this.props.refetch()
			} else {
				return
			}
		})
	}

	archivePost = (postId, postIndex) => {
		Swal({
			title: 'Delete Thank You?',
			text:
				'You are about to delete a thank you, any payment made is not refundable. If you wish to continue, please confirm otherwise cancel.',
			type: 'warning',
			showCancelButton: true,
			cancelButtonColor: '#184e68',
			confirmButtonColor: '#ff7676',
			animation: false,
			confirmButtonText: 'Yes, delete it!'
		}).then(async result => {
			if (result.value) {
				await this.props.removeThankYou(postId, postIndex)

				setTimeout(() => {
					Swal({
						title: 'Deleted Thank You.',
						text: 'Thank you post has been successfully deleted.',
						type: 'success'
					})
				}, 1000)
			}
		})
	}

	render() {
		const { allPosts = [], allPostsCount, loading, loadMore, url } = this.props

		const shouldSeeMoreDisplay = () =>
			allPosts && !loading && allPostsCount !== allPosts.length

		return (
			<BodyWrapper>
				<ThankYouList>
					<FilterContainer>
						<FilterInput
							type="text"
							placeholder="Search by name, email, or location"
							onChange={this.onFilter}
						/>
					</FilterContainer>
					{allPosts &&
						allPosts.map((post, index) => (
							<ThankYouCard
								key={post.id}
								post={post}
								prefixUrl={url}
								reportThankYou={this.reportThankYou}
								archivePost={this.archivePost}
								postIndex={index}
								isLoggedInUserPost={
									(!!post.author && this.loggedInUser === post.author.id) ||
									false
								}
							/>
						))}
				</ThankYouList>
				<ButtonContainer>
					{shouldSeeMoreDisplay() && (
						<SeeMoreButton onClick={loadMore}>See More</SeeMoreButton>
					)}
				</ButtonContainer>
			</BodyWrapper>
		)
	}

	onFilter = evt => {
		evt.persist()
		this.props.setFilter(evt)
	}
}

const getPostFilter = query => ({
	AND: [
		{ archived: false },
		{
			OR: [
				{ name_contains: query },
				{ email_contains: query },
				{ description_contains: query }
			]
		}
	]
})

const ALL_POSTS_QUERY = gql`
	query allPosts($first: Int!, $offset: Int!, $filter: PostFilter) {
		allPosts(
			first: $first
			skip: $offset
			orderBy: createdAt_DESC
			filter: $filter
		) {
			id
			intro
			text
			closing
			image
			video
			name
			reported
			description
			createdAt
			address {
				name
			}
			author {
				id
				name
				picture
			}
			recipient {
				id
				name
			}
			customDesign {
				id
				image
			}
		}
	}
`

const ALL_POSTS_META = gql`
	query allPostsMeta($filter: PostFilter) {
		_allPostsMeta(filter: $filter) {
			count
		}
	}
`

const REPORT_THANKYOU = gql`
	mutation ReportThankYou($id: ID!, $message: String) {
		reportThankYouCard(id: $id, message: $message) {
			id
		}
	}
`

const ARCHIVE_THANKYOU_MUTATION = gql`
	mutation ArchivePost($id: ID!) {
		archivePost(id: $id) {
			id
		}
	}
`

const MAX_NUM_OF_CARDS = 9

export default compose(
	graphql(ALL_POSTS_QUERY, {
		props: ({ data: { allPosts, fetchMore, refetch }, query }) => ({
			refetch,
			allPosts,
			filter: query => {
				refetch({
					first: MAX_NUM_OF_CARDS,
					offset: 0,
					filter: query ? getPostFilter(query) : { archived: false }
				})
			},
			loadMore: () => {
				fetchMore({
					variables: {
						first: MAX_NUM_OF_CARDS,
						offset: allPosts.length,
						filter: query ? getPostFilter(query) : { archived: false }
					},
					updateQuery: (prev, { fetchMoreResult }) =>
						!fetchMoreResult
							? prev
							: {
									...prev,
									allPosts: [...allPosts, ...fetchMoreResult.allPosts]
							  }
				})
			}
		}),
		options: {
			variables: {
				first: MAX_NUM_OF_CARDS,
				offset: 0,
				filter: { archived: false }
			}
		}
	}),
	graphql(ALL_POSTS_META, {
		props: ({ data: { _allPostsMeta } }) => {
			return {
				allPostsCount: _allPostsMeta && _allPostsMeta.count
			}
		},
		options: {
			variables: {
				filter: { archived: false }
			}
		}
	}),
	graphql(REPORT_THANKYOU, { name: 'reportThankYou' }),
	graphql(ARCHIVE_THANKYOU_MUTATION, {
		props: ({ mutate, query }) => ({
			removeThankYou: (id, thankYouIndex) => {
				return mutate({
					variables: { id },
					update: store => {
						const data = store.readQuery({
							query: ALL_POSTS_QUERY,
							variables: {
								first: MAX_NUM_OF_CARDS,
								offset: 0,
								filter: query ? getPostFilter(query) : { archived: false }
							}
						})

						data.allPosts.splice(thankYouIndex, 1)

						store.writeQuery({
							query: ALL_POSTS_QUERY,
							data,
							variables: {
								first: MAX_NUM_OF_CARDS,
								offset: 0,
								filter: query ? getPostFilter(query) : { archived: false }
							}
						})
					}
				})
			}
		})
	})
)(GlobalThankYous)

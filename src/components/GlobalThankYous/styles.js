import styled from 'styled-components'
import media from 'styled-media-query'

export const BodyWrapper = styled.div`
	grid-column: 2 / 3;
	grid-row: 2 / 3;
`

export const ThankYouList = styled.div`
	display: grid;
	grid-gap: 30px;
	grid-template-columns: 232px 232px 232px;
	padding: 60px 0 0;
	justify-content: center;
	${media.lessThan('medium')`
		grid-template-columns: 232px;
		padding: 40px 0 0;
	`};
`

export const FilterContainer = styled.div`
	grid-column: 1 / 4;
	display: grid;
	grid-template-columns: 3fr 1fr;
	grid-gap: 10%;
	${media.lessThan('medium')`
		grid-column: auto;
		grid-template-columns: 232px;
	`};
`

export const FilterInput = styled.input`
	border: none;
	padding: 15px 0;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 600;
	-webkit-font-smoothing: antialiased;
	font-color: #3a3d3f;
	line-height: 1.5;
	border-bottom: 2px solid #dddddd;
	transition: all 0.2s ease;
	&:hover,
	&:focus {
		padding: 15px;
		outline: none;
		background-color: #f8fbfe;
		border-bottom: 2px solid #ff7676;
	}
`

export const ButtonContainer = styled.div`
	display: flex;
	justify-content: center;
	margin-top: 100px;
	margin-bottom: 6%;
	${media.lessThan('medium')`
		margin-bottom: 30px;
	`};
`

export const SeeMoreButton = styled.button`
	background: #ff7676;
	text-decoration: none;
	border: 1px solid #ff7676;
	color: white;
	border-radius: 50px;
	padding: 17px 20px 15px;
	cursor: pointer;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	font-size: 18px;

	&:hover {
		background: transparent;
		color: #ff7676;
		cursor: pointer;
	}
`

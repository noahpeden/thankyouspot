import styled from 'styled-components'
import media from 'styled-media-query'
import { Link } from 'react-router-dom'

export const BannerWrapper = styled.div`
	background: #ffffff;
	padding: 20px 30px;
	position: fixed;
	z-index: 10;
	bottom: 20px;
	left: calc(50% - 350px);
	width: 700px;
	border-radius: 7px;
	box-shadow: 0 2px 10px rgba(24, 78, 104, 0.16);
	transition: all 0.2s ease;
	${media.lessThan('730px')`
        left: 0;
        bottom: 0;
        width: 100%;
        padding: 20px;
        border-radius: 0;
	`};
`

export const BannerContent = styled.p`
	color: #184e68;
	font-size: 16px;
	line-height: 20px;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 300;
	margin: 0;
	span {
		font-weight: 400;
	}
`

export const PrivacyPolicyLink = styled(Link)``

export const CloseButton = styled.button`
	background: #184e68;
	border: 1px solid #184e68;
	color: white;
	border-radius: 50px;
	padding: 7px 14px 6px;
	text-decoration: none;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 300;
	transition: all 0.2s ease;
	cursor: pointer;
	float: right;
	outline: none;
	&:hover {
		color: #184e68;
		background: transparent;
	}
	${media.lessThan('730px')`
        display: block;
        float: initial;
        margin: auto;
        margin-top: 10px;
        padding: 8px 30px 7px;
	`};
`

import React, { Component } from 'react'
import {
	BannerWrapper,
	BannerContent,
	PrivacyPolicyLink,
	CloseButton
} from './styles'

const { REACT_APP_GDPR_COOKIE_BANNER_VERSION } = process.env
const COOKIE_BANNER_KEY = 'tys-gdpr-cookie-version'

class CookieBanner extends Component {
	render() {
		const cookieVersion = localStorage.getItem(COOKIE_BANNER_KEY)

		if (
			cookieVersion &&
			cookieVersion === REACT_APP_GDPR_COOKIE_BANNER_VERSION
		) {
			return null
		}

		return (
			<BannerWrapper>
				<BannerContent>
					<span>Notice:</span> Our website and its third party tools use
					cookies, which are used to manage, monitor, and track usage to improve
					our website's services. If you want to know more or withdraw your
					consent to the use of cookies, please refer to our{' '}
					<PrivacyPolicyLink to="/terms">cookie policy</PrivacyPolicyLink>. By
					closing this banner, scrolling this page, clicking a link, or
					continuing to browse otherwise, you agree and consent to our use of
					cookies.
				</BannerContent>
				<CloseButton onClick={this.onClose}>Got it!</CloseButton>
			</BannerWrapper>
		)
	}

	onClose = () => {
		localStorage.setItem(
			COOKIE_BANNER_KEY,
			REACT_APP_GDPR_COOKIE_BANNER_VERSION
		)

		this.forceUpdate()
	}
}

export default CookieBanner

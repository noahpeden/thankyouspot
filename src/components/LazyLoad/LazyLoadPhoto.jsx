import React from 'react'
import styled, { css } from 'styled-components'

import { LazyLoadContext } from './index'

export const Photo = styled.div`
	width: 445px;
	height: 650px;
	background: #f6f7f8;
	${({ isCard }) =>
		Boolean(isCard) &&
		css`
			height: 100%;
			width: 100%;
			position: absolute;
			padding: 20px;
			box-shadow: 0 2px 14px -5px rgba(24, 79, 105, 0.43);
			backface-visibility: hidden;
			background-size: cover;
			background-position: center;
			background-color: #fff;
		`};

	${({ loaded, src }) =>
		loaded
			? css`
					@keyframes fadein {
						from { opacity: 0.2; }
						to   { opacity: 1; }
					}
					
					background-image: url('${src}');
					background-size: cover;
					background-position: center;
					box-shadow: 0 2px 14px -5px rgba(24,79,105,0.43);
					display: flex;
					animation: fadein 1.5s;
					transition: opacity 1.5s cubic-bezier(0.215, 0.61, 0.355, 1); }
			  `
			: css`
					@keyframes placeHolderShimmer {
						0% {
							background-position: -468px 0;
						}
						100% {
							background-position: 468px 0;
						}
					}

					animation-duration: 1s;
					animation-fill-mode: forwards;
					animation-iteration-count: infinite;
					animation-name: placeHolderShimmer;
					animation-timing-function: linear;
					background: linear-gradient(
						to right,
						#eeeeee 8%,
						#dddddd 18%,
						#eeeeee 33%
					);
					background-size: 800px 104px;
					position: relative;
			  `};

	${({ loaded, isCard, src }) =>
		loaded &&
		Boolean(isCard) &&
		css`
			background-image: url('${src}');
		`};
`
class LazyLoadPhoto extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			loaded: false,
			error: false
		}

		this.setRef = ref => (this.photo = ref)
	}

	componentDidMount() {
		const { addLazyImage = _ => {}, thankYouId, isCard = false } = this.props

		if (!isCard) {
			this.startLoadingImg()
		} else {
			addLazyImage(this.photo, thankYouId, this.startLoadingImg)
		}
	}

	componentWillUnmount() {
		const { thankYouId, removeLazyImage, isCard } = this.props
		if (thankYouId && isCard) {
			removeLazyImage(thankYouId)
		}
	}

	startLoadingImg = _ => {
		this.image = new Image()

		this.image.src = this.props.src
		this.image.onload = this.handleLoad
		this.image.onerror = this.handleError
	}

	handleError = _ => {
		console.error('Failed to load ', this.props.src)

		this.setState({
			error: true
		})
	}

	handleLoad = _ => {
		this.setState({
			loaded: true
		})
	}

	render() {
		const { isCard, thankYouId, src } = this.props
		return (
			<Photo
				data-id={thankYouId}
				isCard={isCard}
				innerRef={this.setRef}
				src={src}
				{...this.state}
			/>
		)
	}
}

export default props => (
	<LazyLoadContext.Consumer>
		{({ addLazyImage = _ => {}, removeLazyImage = _ => {} }) => (
			<LazyLoadPhoto
				addLazyImage={addLazyImage}
				removeLazyImage={removeLazyImage}
				{...props}
			/>
		)}
	</LazyLoadContext.Consumer>
)

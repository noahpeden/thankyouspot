import React from 'react'
import { withRouter } from 'react-router-dom'

const LazyLoadContext = React.createContext()

const LazyLoadProvider = withRouter(
	class extends React.Component {
		constructor(props) {
			super(props)

			this.state = {
				isFallbackListenerActive: false,
				removeFallbackListeners: () => {},
				lazyImages: [],
				loadImageFns: [],
				requestAnimationFrame: () => {}
			}
		}

		componentDidUpdate(prevProps) {
			if (this.props.location.pathname !== prevProps.location.pathname) {
				setTimeout(() => {
					if (this.state.lazyImages.length) {
						this.state.requestAnimationFrame()
					}
				}, 1000)
			}
		}

		startImageLoad = thankYouId => {
			const loadImage = this.state.loadImageFns.find(
				({ id, fn }) => id === thankYouId && typeof fn === 'function'
			)
			Boolean(loadImage) && loadImage.fn()
		}

		addLazyImage = (el, thankYouId = undefined, startLoadingImg = _ => {}) => {
			this.setState(prevState => {
				return {
					lazyImages: [...prevState.lazyImages, el],
					loadImageFns: [
						...prevState.loadImageFns,
						{ id: thankYouId, fn: startLoadingImg }
					]
				}
			})
		}

		removeLazyImage = thankYouId => {
			this.setState(prevState => {
				const lazyImages = [...prevState.lazyImages].filter(
					elem => elem.dataset.id !== thankYouId
				)
				const loadImageFns = [...prevState.loadImageFns].filter(({ id }) => {
					return id !== thankYouId
				})
				return {
					lazyImages,
					loadImageFns
				}
			})
		}

		domContentListener = _ => {
			let active = false

			const lazyLoad = () => {
				const { lazyImages } = this.state

				if (active === false) {
					active = true

					setTimeout(() => {
						lazyImages.forEach(lazyImage => {
							if (
								lazyImage.getBoundingClientRect().top <= window.innerHeight &&
								lazyImage.getBoundingClientRect().bottom >= 0 &&
								getComputedStyle(lazyImage).display !== 'none'
							) {
								this.startImageLoad(lazyImage.dataset.id)
							}
						})

						active = false
					}, 200)
				}
			}

			document.addEventListener('scroll', lazyLoad)
			window.addEventListener('resize', lazyLoad)
			window.addEventListener('orientationchange', lazyLoad)
			window.requestAnimationFrame(lazyLoad)
			this.setState({
				removeFallbackListeners: () => {
					document.removeEventListener('scroll', lazyLoad)
					window.removeEventListener('resize', lazyLoad)
					window.removeEventListener('orientationchange', lazyLoad)
				},
				isFallbackListenerActive: true,
				requestAnimationFrame: () => window.requestAnimationFrame(lazyLoad)
			})
		}

		componentDidMount() {
			document.addEventListener('DOMContentLoaded', this.domContentListener)
		}

		componentWillUnmount() {
			document.removeEventListener('DOMContentLoaded', this.domContentListener)

			if (this.state.isFallbackListenerActive) {
				this.state.removeFallbackListeners()
			}
		}

		render() {
			return (
				<LazyLoadContext.Provider
					value={{
						addLazyImage: this.addLazyImage,
						removeLazyImage: this.removeLazyImage
					}}
				>
					{this.props.children}
				</LazyLoadContext.Provider>
			)
		}
	}
)

export { LazyLoadProvider as default, LazyLoadContext }

import React from 'react'
import { NavLink } from 'react-router-dom'
import { ButtonLabel, EditButton } from './styles'

//execute all functions in array
const execFns = fns => !!fns && fns.map(fn => fn())

class EditButtons extends React.Component {
	static Edit = ({ onEdit, children, id, toggle, isLoggedInUser }) => {
		return !onEdit && isLoggedInUser ? (
			<NavLink to={`/${id}/edit`} exact>
				<EditButton onClick={toggle}>
					<ButtonLabel>{children}</ButtonLabel>
				</EditButton>
			</NavLink>
		) : null
	}

	static Cancel = ({
		onEdit,
		children,
		id,
		toggle,
		isLoggedInUser,
		cancelEdit
	}) => {
		return onEdit && isLoggedInUser ? (
			<NavLink to={`/${id}`} exact>
				<EditButton cancel onClick={() => execFns([toggle, cancelEdit])}>
					<ButtonLabel>{children}</ButtonLabel>
				</EditButton>
			</NavLink>
		) : null
	}

	static Save = ({
		onEdit,
		children,
		id,
		toggle,
		isLoggedInUser,
		saveProfile
	}) => {
		return onEdit && isLoggedInUser ? (
			<NavLink to={`/${id}`} exact>
				<EditButton save onClick={() => execFns([toggle, saveProfile])}>
					<ButtonLabel>{children}</ButtonLabel>
				</EditButton>
			</NavLink>
		) : null
	}

	state = {
		onEdit: this.props.onEdit,
		id: this.props.id,
		loggedInUser: localStorage.getItem('tys-userid')
	}

	toggleEditMode = () =>
		this.setState(({ onEdit }) => {
			return {
				onEdit: !onEdit
			}
		})

	render() {
		return React.Children.map(this.props.children, child =>
			React.cloneElement(child, {
				onEdit: this.props.onEdit,
				id: this.state.id,
				toggle: this.toggleEditMode,
				isLoggedInUser: this.props.isLoggedInUser,
				cancelEdit: this.props.cancelEdit,
				saveProfile: this.props.saveProfile
			})
		)
	}
}

export default EditButtons

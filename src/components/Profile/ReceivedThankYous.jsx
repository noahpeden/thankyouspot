import { compose, graphql } from 'react-apollo'
import gql from 'graphql-tag'

import ThankYouList from './ThankYouList'

const RECEIVED_THANK_YOUS_QUERY = gql`
	query ReceivedThankYous(
		$id: ID!
		$offset: Int
		$limit: Int
		$archived: Boolean
	) {
		_allPostsMeta(filter: { recipient: { id: $id }, archived: $archived }) {
			count
		}

		allPosts(
			filter: { recipient: { id: $id }, archived: $archived }
			skip: $offset
			first: $limit
			orderBy: createdAt_DESC
		) {
			id
			intro
			text
			closing
			image
			video
			reported
			author {
				id
				name
				picture
			}
			recipient {
				id
				name
			}
			customDesign {
				id
				image
			}
		}
	}
`

const REPORT_THANKYOU = gql`
	mutation ReportThankYou($id: ID!, $message: String) {
		reportThankYouCard(id: $id, message: $message) {
			id
		}
	}
`

const ARCHIVE_THANKYOU_MUTATION = gql`
	mutation ArchivePost($id: ID!) {
		archivePost(id: $id) {
			id
		}
	}
`

export default compose(
	graphql(RECEIVED_THANK_YOUS_QUERY, {
		options: ({ id }) => ({
			variables: { id, limit: 12, archived: false },
			fetchPolicy: 'cache-and-network',
			notifyOnNetworkStatusChange: true
		}),
		props: ({
			data: {
				_allPostsMeta,
				allPosts,
				networkStatus,
				fetchMore,
				refetch,
				loading
			}
		}) => ({
			loading,
			refetch,
			networkStatus,
			posts: allPosts,
			totalPosts: _allPostsMeta ? _allPostsMeta.count : 0,
			hasMorePosts: _allPostsMeta && allPosts.length < _allPostsMeta.count,
			seeMorePosts: () =>
				fetchMore({
					variables: {
						offset: allPosts ? allPosts.length : 0,
						limit: 12
					},
					updateQuery: (prevResult, { fetchMoreResult }) => {
						if (!fetchMoreResult) return prevResult

						return {
							...prevResult,
							allPosts: [...prevResult.allPosts, ...fetchMoreResult.allPosts]
						}
					}
				}),
			cardOptions: {
				showNames: false,
				showTimestamp: false
			}
		})
	}),
	graphql(ARCHIVE_THANKYOU_MUTATION, { name: 'removeThankYou' }),
	graphql(REPORT_THANKYOU, { name: 'reportThankYou' })
)(ThankYouList)

import styled, { css } from 'styled-components'
import { Link, NavLink } from 'react-router-dom'

import media from 'styled-media-query'

export const Wrapper = styled.div`
	display: grid;
	grid-template-columns: auto 1020px auto;
	grid-template-rows: 255px auto;
	${media.between('medium', 'large')`
		grid-template-columns: auto auto;
	`};
	${media.lessThan('medium')`
		grid-template-columns: auto auto;
		grid-template-rows: 280px auto;
	`};
`

export const Hero = styled.div`
	grid-column: 1 / 4;
	grid-row: 1 / 3;
	display: flex;
	flex-direction: column;
	padding: 40px 20%;
	align-items: center;
	justify-content: center;
	background-color: #dbeef7;
	${media.between('medium', 'large')`
        margin-top: 50px;
		padding: 30px;
	`};
	${media.lessThan('medium')`
		padding: 20px 20px 80px;
	`};
`

export const HeroTitle = styled.h1`
	color: #184e68;
	font-size: 50px;
	text-shadow: 0 8px 0 #e5cad0;
	margin: 0 auto;
	text-align: center;
	padding-bottom: 10px;
	font-family: 'Paytone One', sans-serif;
	-webkit-font-smoothing: antialiased;
	${media.lessThan('medium')`
		font-size: 35px;
		text-align: center;
	`};
`
export const Graphic = styled.img`
	margin: 0 auto;
`

export const BodyWrapper = styled.div`
	grid-column: 2 / 3;
	grid-row: 3 / 5;
	display: grid;
	grid-template-columns: auto 780px;
	${media.between('medium', 'large')`
		grid-template-columns: auto;
	`};
	${media.lessThan('medium')`
		display: flex;
		flex-direction: column;
		justify-content: center;
		align-items: center;
		margin: 0 auto;
	`};
`

export const ProfileInfo = styled.div`
	grid-column: 1 / 2;
	display: flex;
	flex-direction: column;
	align-items: center;
	padding: 0 20px;
	margin-top: -80px;
	width: 100%;
	max-width: 240px;
	word-break: break-word;
	${media.lessThan('medium')`
		max-width: 440px;
		margin-top: -70px;
	`};
`

export const AvatarImageWrapper = styled.div`
	display: block;
	border: 10px solid #ff7676;
	border-radius: 50%;
	width: 200px;
	height: 200px;
	margin-bottom: 20px;
	background-color: #ff7676;
	position: relative;

	${media.lessThan('medium')`
		width: 150px;
		height: 150px;
	`};
`

export const AvatarImage = styled.img`
	border-radius: 50%;
	width: 100%;
	height: 100%;
	object-fit: cover;
`

export const AvatarOverlay = styled.div`
	position: absolute;
	top: -10px;
	left: -10px;
	display: flex;
	align-items: center;
	justify-content: center;
	border-radius: 50%;
	width: 200px;
	height: 200px;
	border: 10px solid #ff7676;
	background-color: rgba(24, 78, 104, 0.2);
	cursor: pointer;
	transition: all 0.2s ease;
	&:hover {
		background-color: rgba(24, 78, 104, 0.7);
	}
	${media.lessThan('medium')`
		width: 150px;
		height: 150px;
	`};
`

export const ProfileName = styled.h2`
	color: #3a3d3f;
	font-size: 23px;
	text-align: center;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 700;
	-webkit-font-smoothing: antialiased;
`

export const ThankCTA = styled(Link)`
	display: flex;
	align-self: stretch;
	border: 2px solid #ff7676;
	align-items: center;
	justify-content: center;
	box-shadow: 0 12px 43px -14px #ff7676;
	background: #ff7676;
	text-decoration: none;
	color: white;
	border-radius: 50px;
	padding: 17px 15px 15px;
	cursor: pointer;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	font-size: 18px;
	transition: all 0.2s ease;
	&:hover {
		background: transparent;
		color: #ff7676;
		box-shadow: none;
	}
	${media.lessThan('medium')`
		margin-right: 10px;
		font-size:16px;
	`};
`

export const ThankYous = styled.div`
	grid-column: 2 / 3;
	display: flex;
	flex-direction: column;
	padding: 45px 20px;
	${media.lessThan('medium')`
		padding: 20px;
	`};
`

export const MainTabs = styled.div`
	${media.lessThan('medium')`
		display: flex;
		flex: 1;
		flex-direction: row;
		justify-content: space-between;
		margin: 0 0 40px;
	`};
`

export const TabButtons = styled.div`
	margin: 0 0 30px;
	display: flex;
	flex-direction: row;
	justify-content: space-between;
	align-items: flex-start;
	${media.lessThan('large')`
		display: flex;
		flex: 1;
		flex-direction: column-reverse;
		margin: 0 0 40px;
	`};
	${media.lessThan('medium')`
		margin: 20px 0 40px;
		align-items: center;
	`};
`

export const TabButton = styled(NavLink)`
	display: inline-block;
	text-decoration: none;
	padding: 0 0 15px;
	margin-right: 40px;

	&.active {
		border-bottom: 8px solid #ff7676;
	}
	${media.lessThan('large')`
		padding: 10px 0 15px;
	`};
	${media.lessThan('medium')`
		margin-right: 0;
		margin: 0 10px;
		text-align: center;
	`};
`
export const DeleteButton = styled.button`
	height: auto;
	width: auto;
	border-radius: 30px;
	border: 2px solid #ff7676;
	background: #ff7676;
	padding: 17px 30px 15px;
	margin-bottom: 20px;
	color: #fff;
	font-size: 18px;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 400;
	cursor: pointer;
	transition: all 0.2s ease;
	&:hover {
		background: transparent;
		color: #ff7676;
	}
`

export const ProfileCTAS = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: space-around;
	align-items: center;
	height: auto;
`
export const EditButton = styled.button`
	padding: 14px 20px 12px;
	border-radius: 30px;
	border: 2px solid #184e68;
	color: #184e68;
	font-size: 18px;
	cursor: pointer;
	transition: all 0.2s ease;
	&:hover {
		color: #fff;
		background: #184e68;
	}
	${props =>
		props.cancel &&
		css`
			margin-right: 16px;
		`};
	${props =>
		props.save &&
		css`
			color: #fff;
			background: #184e68;
			&:hover {
				color: #184e68;
				background: transparent;
			}
		`};
`

export const ButtonLabel = styled.span`
	font-family: 'Josefin Sans', sans-serif;
`

export const TabCount = styled.span`
	font-weight: 900;
	font-size: 30px;
	color: #184e68;
	text-shadow: 0 3px 0 #dbeef7;
	margin-right: 20px;
	font-family: 'Paytone One', sans-serif;
	-webkit-font-smoothing: antialiased;
	letter-spacing: 1px;
	${media.lessThan('medium')`
		margin-right: 0;
		font-size: 60px;
		margin-bottom: 5px;
		display: block;
	`};
`

export const TabLabel = styled.span`
	font-weight: 200;
	font-size: 16px;
	color: #3a3d3f;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 400;
`

export const FormContainer = styled.div`
	width: 100%;
	max-width: 200px;
	background: #f2f2f2;
	margin-bottom: 20px;
	${media.lessThan('medium')`
		max-width: 100%;
	`};
`

export const FormInput = styled.input`
	width: 100%;
	height: 60px;
	padding: 12px;
	background: #f2f2f2;
	border: none;
	font-family: 'Josefin Sans', sans-serif;
	font-size: 18px;
	border-bottom: 2px solid #ff7676;
`

export const HiddenInput = styled.input`
	display: none;
`

export const EmptyState = styled.div`
	align-items: center;
	flex: 1;
	display: flex;
	flex-direction: column;
`

export const EmptyStateTitle = styled.h2`
	color: #184e68;
	font-size: 30px;
	line-height: 40px;
	font-family: 'Paytone One', sans-serif;
	-webkit-font-smoothing: antialiased;
	letter-spacing: 1px;
	margin: 0;
	text-align: center;
	padding: 30px;
	${media.between('medium', 'large')`
		font-size: 30px;
	`};
	${media.lessThan('medium')`
		font-size: 25px;
		padding: 20px 0;
	`};
`

export const EmptyStateGraphic = styled.img`
	width: 200px;
	${media.between('medium', 'large')`
		width: 200px;
	`};
	${media.lessThan('medium')`
		width: 200px;
	`};
`

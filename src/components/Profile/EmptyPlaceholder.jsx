import React from 'react'
import { EmptyState, EmptyStateTitle, EmptyStateGraphic } from './styles'

export default () => (
	<EmptyState>
		<EmptyStateGraphic
			src={require('../../assets/images/empty-state1.gif')}
			alt="Video Camera"
		/>
		<EmptyStateTitle>There are no Thank You's here.</EmptyStateTitle>
	</EmptyState>
)

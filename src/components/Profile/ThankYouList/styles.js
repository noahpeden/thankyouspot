import styled from 'styled-components'
import { Link } from 'react-router-dom'
import media from 'styled-media-query'

export const Wrapper = styled.div`
	display: flex;
	flex-direction: column;
`

export const PostsWrapper = styled.div`
	display: grid;
	grid-gap: 30px;
	grid-template-columns: 232px 232px 232px;
	padding-bottom: 30px;
	${media.between('medium', 'large')`
		grid-template-columns: 232px 232px;
	`};
	${media.lessThan('medium')`
		grid-template-columns: 232px;
		justify-content: center;
	`};
`

export const SeeMoreButton = styled.button`
	align-self: center;
	background-color: #184e68;
	border: none;
	height: 40px;
	border-radius: 30px;
	padding: 0 20px;
	color: #fff;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	font-size: 18px;
	outline: none;
	cursor: pointer;
	border: 1px solid #184e68;

	&:hover {
		background-color: transparent;
		color: #184e68;
	}
`

export const ThankYouLink = styled(Link)`
	text-decoration: none;
`

export const ThankYou = styled.div`
	font-family: 'Josefin Sans', sans-serif;
	display: flex;
	align-items: center;
	flex-direction: column;
	height: 350px;
	padding: 20px;
	box-shadow: 0 2px 14px -5px rgba(24, 79, 105, 0.43);
	position: relative;
`

export const DeleteButton = styled.button`
	position: absolute;
	bottom: -40px;
	right: 0;
	padding-top: 20px;
	border: none;
	outline: none;
	background: transparent;
	cursor: pointer;
	transition: all 0.2s ease;
	display: none;
	${ThankYou}:hover & {
		display: block;
	}
	&:hover {
		display: block;
		path {
			fill: #ff7676;
		}
	}
`

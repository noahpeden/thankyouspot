import React, { Component } from 'react'
import ThankYouCard from '../../ThankYouCard'

import EmptyPlaceholder from '../EmptyPlaceholder'
import { PostsWrapper, SeeMoreButton, Wrapper } from './styles'
import Swal from 'sweetalert2'

class ThankYouList extends Component {
	async componentWillMount() {
		this.loggedInUser = await localStorage.getItem('tys-userid')
	}

	reportThankYou = postId => {
		Swal({
			title: 'Report This Inappropriate?',
			type: 'warning',
			text: 'Why are you reporting this?',
			input: 'text',
			showCancelButton: true,
			showConfirmButton: true,
			confirmButtonText: 'Report'
		}).then(async result => {
			if (!!result.value) {
				let response = await this.props.reportThankYou({
					variables: {
						id: postId,
						message: result.value
					}
				})
				if (!!response.data) {
					Swal({
						title: 'ThankYou Card Reported.',
						text:
							'Thank you for your feedback. We will check on this as soon as possible',
						type: 'success'
					})
				}
				this.props.refetch()
			} else {
				return
			}
		})
	}

	archivePost = postId => {
		Swal({
			title: 'Delete Thank You?',
			text:
				'You are about to delete a thank you, any payment made is not refundable. If you wish to continue, please confirm otherwise cancel.',
			type: 'warning',
			showCancelButton: true,
			cancelButtonColor: '#184e68',
			confirmButtonColor: '#ff7676',
			animation: false,
			confirmButtonText: 'Yes, delete it!'
		}).then(async result => {
			if (result.value) {
				await this.props.removeThankYou({ variables: { id: postId } })
				this.props.refetch()
				setTimeout(() => {
					Swal({
						title: 'Deleted Thank You.',
						text: 'Thank you post has been successfully deleted.',
						type: 'success'
					})
				}, 1000)
			}
		})
	}
	render() {
		const {
			posts = [],
			cardOptions = [],
			seeMorePosts,
			hasMorePosts,
			totalPosts,
			loading
		} = this.props

		//TODO: add loading component
		if (loading) return 'Loading...'

		return (
			<Wrapper>
				{!Boolean(totalPosts) ? (
					<EmptyPlaceholder />
				) : (
					<React.Fragment>
						<PostsWrapper>
							{posts.map((post, key) => (
								<Post
									key={key}
									post={post}
									cardOptions={cardOptions}
									archivePost={this.archivePost}
									reportThankYou={this.reportThankYou}
									isLoggedInUserPost={
										!!post.author ? this.loggedInUser === post.author.id : false
									}
								/>
							))}
						</PostsWrapper>
						{hasMorePosts && (
							<SeeMoreButton onClick={seeMorePosts}>See more</SeeMoreButton>
						)}
					</React.Fragment>
				)}
			</Wrapper>
		)
	}
}

const Post = ({
	post,
	cardOptions,
	archivePost,
	isLoggedInUserPost = false,
	reportThankYou = () => {}
}) => (
	<ThankYouCard
		post={post}
		{...cardOptions}
		archivePost={archivePost}
		isLoggedInUserPost={isLoggedInUserPost}
		reportThankYou={reportThankYou}
	/>
)

export default ThankYouList

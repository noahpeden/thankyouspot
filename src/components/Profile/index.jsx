import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import { withRouter } from 'react-router'
import { compose, graphql, withApollo } from 'react-apollo'
import gql from 'graphql-tag'
import Swal from 'sweetalert2'
import NotFound from '../Redirect/NotFound'
import {
	AvatarImage,
	BodyWrapper,
	DeleteButton,
	FormContainer,
	FormInput,
	Graphic,
	Hero,
	HeroTitle,
	MainTabs,
	ProfileCTAS,
	ProfileInfo,
	ProfileName,
	TabButton,
	TabButtons,
	TabCount,
	TabLabel,
	ThankCTA,
	ThankYous,
	Wrapper,
	HiddenInput,
	AvatarImageWrapper,
	AvatarOverlay
} from './styles'
import ReceivedThankYous from './ReceivedThankYous'
import SentThankYous from './SentThankYous'
import EditButtons from './EditButtons'

class Profile extends Component {
	state = {
		imageFile: null,
		imageDataurl: null,
		name: '',
		file: null,
		fileAsDataUrl: null
	}

	render() {
		const { id, user, loading, networkStatus, authInfo } = this.props

		if (networkStatus === 1) {
			// TODO: Show loading state
			return null
		}

		if (!loading && !user) {
			return <NotFound />
		}

		return (
			<Wrapper>
				<Hero>
					<Graphic
						src={require('../../assets/images/st-profile.gif')}
						alt="Feature"
					/>
					<HeroTitle>{user.name}'s</HeroTitle>
					<HeroTitle>Thank You Spot</HeroTitle>
				</Hero>

				<BodyWrapper>
					<ProfileInfo>
						<AvatarImageWrapper>
							<AvatarImage
								src={
									this.state.fileAsDataUrl ||
									(!!user.picture && user.picture) ||
									require('../../assets/images/default-avatar.svg')
								}
								alt={user.photo ? `${user.name} photo` : 'TYS default photo'}
							/>
							<Switch>
								<Route
									exact
									path={`/${id}/edit`}
									render={() => (
										<AvatarOverlay
											onClick={() => {
												const el = document.getElementById('fileinput')
												!!el && el.click()
											}}
										>
											<svg
												width="70"
												height="64"
												viewBox="0 0 24 22"
												xmlns="http://www.w3.org/2000/svg"
											>
												<path
													d="M21 22H3c-1.654 0-3-1.346-3-3V7c0-1.654 1.346-3 3-3 .853 0 1.619-.474 2-1.236l.211-.422A4.212 4.212 0 0 1 9 0h6c1.615 0 3.067.897 3.789 2.342l.211.422A2.224 2.224 0 0 0 21 4c1.654 0 3 1.346 3 3v12c0 1.654-1.346 3-3 3zM9 2c-.853 0-1.619.474-2 1.236l-.211.422A4.212 4.212 0 0 1 3 6c-.551 0-1 .449-1 1v12c0 .552.449 1 1 1h18a1 1 0 0 0 1-1V7c0-.551-.448-1-1-1a4.212 4.212 0 0 1-3.789-2.342L17 3.236A2.224 2.224 0 0 0 15 2H9zm3 16c-3.309 0-6-2.691-6-6s2.691-6 6-6 6 2.691 6 6-2.691 6-6 6zm0-10c-2.206 0-4 1.794-4 4s1.794 4 4 4 4-1.794 4-4-1.794-4-4-4z"
													fill="#FBF1A9"
													fillRule="nonzero"
												/>
											</svg>
										</AvatarOverlay>
									)}
								/>
							</Switch>
						</AvatarImageWrapper>
						<Switch>
							<Route
								exact
								path={`/${id}/edit`}
								render={() => {
									if (!authInfo || authInfo.userID !== id) {
										return <Redirect to={`/${id}`} />
									}

									return (
										<React.Fragment>
											<FormContainer>
												<FormInput
													type="text"
													value={this.state.name || ` ${user.name}`}
													accept="image/*"
													onChange={evt => {
														this.setState({
															name: ` ${evt.currentTarget.value.replace(
																/^\s+/,
																''
															)}`
														})
													}}
												/>
												<HiddenInput
													id="fileinput"
													type="file"
													ref="fileinput"
													onChange={this.onSelectFile}
												/>
											</FormContainer>
											<DeleteButton onClick={() => this.deleteAccount(id)}>
												Delete Account
											</DeleteButton>
										</React.Fragment>
									)
								}}
							/>
							<Route
								render={() => (
									<React.Fragment>
										<ProfileName>{user.name}</ProfileName>
										<ThankCTA to="/thank-someone">Say Thanks</ThankCTA>
									</React.Fragment>
								)}
							/>
						</Switch>
					</ProfileInfo>
					<ThankYous>
						<TabButtons>
							<MainTabs>
								<TabButton
									to={{ pathname: `/${id}`, state: { retainScroll: true } }}
									exact
									activeClassName="active"
								>
									<TabCount>{user._receivedPostsMeta.count}</TabCount>
									<TabLabel>Thank You's Received</TabLabel>
								</TabButton>
								<TabButton
									to={{
										pathname: `/${id}/sent`,
										state: { retainScroll: true }
									}}
									activeClassName="active"
								>
									<TabCount>{user._postsMeta.count}</TabCount>
									<TabLabel>Thank You's Sent</TabLabel>
								</TabButton>
							</MainTabs>
							<ProfileCTAS>
								<EditButtons
									onEdit={this.props.location.pathname.endsWith('/edit')}
									id={id}
									isLoggedInUser={authInfo && authInfo.userID === id}
									saveProfile={this.saveProfile}
									cancelEdit={this.cancelEdit}
								>
									<EditButtons.Cancel>Cancel</EditButtons.Cancel>
									<EditButtons.Save>Save Changes</EditButtons.Save>
									<EditButtons.Edit>Edit Profile</EditButtons.Edit>
								</EditButtons>
							</ProfileCTAS>
						</TabButtons>
						<Switch>
							<Route
								exact
								path={`/${id}`}
								render={() => <ReceivedThankYous id={id} />}
							/>
							<Route
								exact
								path={`/${id}/edit`}
								render={() => <ReceivedThankYous id={id} />}
							/>
							<Route
								exact
								path={`/${id}/thank-yous/:postID`}
								render={() => <ReceivedThankYous id={id} />}
							/>
							<Route
								exact
								path={`/${id}/sent`}
								render={() => <SentThankYous id={id} />}
							/>
							<Route component={NotFound} />
						</Switch>
					</ThankYous>
				</BodyWrapper>
			</Wrapper>
		)
	}

	onSelectFile = evt => {
		const file = evt.currentTarget.files[0]
		const fr = new FileReader()
		fr.onload = evt => {
			this.setState({
				file,
				fileAsDataUrl: evt.target.result
			})
		}
		fr.readAsDataURL(file)
	}

	cancelEdit = evt => this.resetState()

	saveProfile = async evt => {
		try {
			const { name, file } = this.state
			const { id: userId } = this.props
			const toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false
			})
			toast({
				title: 'Updating Profile',
				text: 'Your profile is being updated.',
				type: 'info',
				animation: false,
				onOpen: () => Swal.showLoading()
			})
			if (!!name) {
				await this.props.updateProfile({
					variables: { id: userId, name }
				})
			}
			if (!!file) {
				const filename = `${userId}/profile/avatar.${file.type.split('/')[1]}`
				const publicUrl = `https://s3.amazonaws.com/www.thankyouspot.com/${userId}/profile/avatar.${
					file.type.split('/')[1]
				}`
				const s3LinkResponse = await this.props.client.query({
					query: GET_S3_UPLOAD_URL_QUERY,
					variables: { filename: filename, filetype: file.type }
				})
				const s3Link = s3LinkResponse.data.getS3Url.url
				await fetch(s3Link, {
					body: file,
					method: 'PUT',
					headers: {
						'Content-Type': file.type
					}
				})
				await this.props.updatePicture({
					variables: { id: userId, picture: publicUrl }
				})
			}
			setTimeout(() => {
				toast({
					title: 'Profile Updated.',
					text: 'Profile information was succesfully updated.',
					type: 'success',
					timer: 1500,
					animation: false,
					onOpen: () => Swal.hideLoading()
				})
			}, 1000)
		} catch (exception) {
			Swal({
				title: 'Something went wrong.',
				text: 'An error occured while updating your profile information.',
				type: 'error'
			})
		}
	}

	resetState = () =>
		this.setState({ name: '', file: null, fileAsDataUrl: null })

	deleteAccount = id => {
		const toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false
		})
		Swal({
			title: 'Delete Your Account? Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			cancelButtonColor: '#184E68',
			confirmButtonColor: '#ff7676',
			animation: false,
			confirmButtonText: 'Yes, delete it!'
		}).then(async result => {
			if (result.value) {
				this.props.history.push('/')
				toast({
					title: 'Deleting Account',
					text:
						'We are sorry for your departure. Your account is being deleted.',
					type: 'info',
					animation: false,
					timer: 5000
				})
				try {
					await this.props.deleteAccount({
						variables: {
							userId: id
						}
					})
				} catch (err) {
					console.error(err)
				}
				await this.props.updateAuthInfo({ userID: null, token: null })
			}
		})
	}
}

const GET_S3_UPLOAD_URL_QUERY = gql`
	query GetS3Url($filename: String!, $filetype: String!) {
		getS3Url(filename: $filename, filetype: $filetype) {
			url
		}
	}
`

const UpdatePicture = gql`
	mutation UpdatePicture($id: ID!, $picture: String) {
		updateUser(id: $id, picture: $picture) {
			id
			picture
		}
	}
`

const UpdateProfile = gql`
	mutation UpdateProfile($id: ID!, $name: String) {
		updateUser(id: $id, name: $name) {
			id
			name
		}
	}
`

const PROFILE_QUERY = gql`
	query Profile($id: ID!) {
		User(id: $id) {
			id
			email
			name
			picture
			_postsMeta(filter: { archived: false }) {
				count
			}
			_receivedPostsMeta(filter: { archived: false }) {
				count
			}
		}
	}
`

const AUTH_INFO_QUERY = gql`
	query AuthInfoProfile {
		authInfo @client {
			userID
		}
	}
`

const DELETE_ACCOUNT = gql`
	mutation DeleteUserAccount($userId: ID!) {
		deleteUserAccount(userId: $userId) {
			id
		}
	}
`

const UPDATE_AUTH_INFO_MUTATION = gql`
	mutation UpdateAuthInfo($userID: String, $token: String) {
		updateAuthInfo(userID: $userID, token: $token) @client
	}
`

export default compose(
	graphql(PROFILE_QUERY, {
		options: ({ id }) => ({
			variables: { id },
			fetchPolicy: 'cache-and-network',
			notifyOnNetworkStatusChange: true,
			pollInterval: 1000
		}),
		props: ({ data: { User, loading, networkStatus } }) => ({
			user: User,
			loading,
			networkStatus
		})
	}),
	graphql(AUTH_INFO_QUERY, {
		props: ({ data: { authInfo } }) => ({
			authInfo
		})
	}),
	graphql(UpdatePicture, {
		props: ({ mutate }) => ({
			updatePicture: options => {
				return mutate(options)
			}
		})
	}),
	graphql(UpdateProfile, {
		props: ({ mutate }) => ({
			updateProfile: options => {
				return mutate(options)
			}
		})
	}),
	graphql(DELETE_ACCOUNT, {
		name: 'deleteAccount'
	}),
	graphql(UPDATE_AUTH_INFO_MUTATION, {
		props: ({ mutate }) => ({
			updateAuthInfo: variables => {
				return mutate({ variables })
			}
		})
	}),
	withApollo
)(withRouter(Profile))

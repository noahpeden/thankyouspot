import React, { Component } from 'react'
import { compose, graphql } from 'react-apollo'
import gql from 'graphql-tag'
import { slide as Menu } from 'react-burger-menu'

import {
	Wrapper,
	LogoLink,
	LogoImage,
	LinkContainer,
	HeaderLinkWrapper,
	HeaderLink,
	HeaderLinkSpan,
	SignUpLink
} from './styles'

import headerLogo from '../../assets/images/tys-logo.svg'

class Header extends Component {
	state = {
		isMenuOpen: false
	}

	render() {
		const { authInfo, id } = this.props
		const loggedIn = Boolean(authInfo.token)
		return (
			<React.Fragment>
				<Menu right isOpen={this.state.isMenuOpen}>
					<HeaderLinkWrapper>
						<HeaderLinks
							fullscreen="true"
							loggedIn={loggedIn}
							onLogout={this.onLogout}
							id={id}
						/>
					</HeaderLinkWrapper>
				</Menu>
				<Wrapper>
					<LogoLink to="/">
						<LogoImage src={headerLogo} alt="logo" />
					</LogoLink>
					<LinkContainer>
						<HeaderLinks
							id={authInfo && authInfo.userID}
							loggedIn={loggedIn}
							onLogout={this.onLogout}
						/>
					</LinkContainer>
				</Wrapper>
			</React.Fragment>
		)
	}

	onLogout = async e => {
		e.preventDefault()

		try {
			this.props.updateAuthInfo({ userID: null, token: null })
		} catch (err) {
			console.error(err)
		}
	}
}

const HeaderLinks = ({ fullscreen, onLogout, loggedIn, id }) => (
	<React.Fragment>
		<HeaderLink fullscreen={fullscreen} to="/global-thank-yous">
			<HeaderLinkSpan>Global Thank You's</HeaderLinkSpan>
		</HeaderLink>
		<HeaderLink fullscreen={fullscreen} to="/how-it-works">
			<HeaderLinkSpan>How It Works</HeaderLinkSpan>
		</HeaderLink>
		<HeaderLink fullscreen={fullscreen} to="/features">
			<HeaderLinkSpan>Features</HeaderLinkSpan>
		</HeaderLink>
		<HeaderLink fullscreen={fullscreen} to="/custom-designs">
			<HeaderLinkSpan>Custom Designs</HeaderLinkSpan>
		</HeaderLink>
		<HeaderLink fullscreen={fullscreen} to="/thank-someone">
			<HeaderLinkSpan>Thank Someone</HeaderLinkSpan>
		</HeaderLink>

		{loggedIn ? (
			<React.Fragment>
				<HeaderLink fullscreen={fullscreen} to={{ pathname: `/${id}` }}>
					<HeaderLinkSpan>Profile</HeaderLinkSpan>
				</HeaderLink>
				<SignUpLink to="/" fullscreen={fullscreen} onClick={onLogout}>
					Log out
				</SignUpLink>
			</React.Fragment>
		) : (
			<React.Fragment>
				<HeaderLink
					fullscreen={fullscreen}
					to={{ pathname: '/auth/login', state: { modal: true } }}
				>
					<HeaderLinkSpan>Log In</HeaderLinkSpan>
				</HeaderLink>
				<SignUpLink
					fullscreen={fullscreen}
					to={{ pathname: '/auth/signup', state: { modal: true } }}
				>
					Sign Up
				</SignUpLink>
			</React.Fragment>
		)}
	</React.Fragment>
)

const AUTH_INFO_QUERY = gql`
	query AuthInfo {
		authInfo @client {
			userID
			token
		}
	}
`

const UPDATE_AUTH_INFO_MUTATION = gql`
	mutation UpdateAuthInfo($userID: String, $token: String) {
		updateAuthInfo(userID: $userID, token: $token) @client
	}
`

const LOGGED_IN_USER = gql`
	query loggedInUser {
		loggedInUser {
			id
		}
	}
`

export default compose(
	graphql(LOGGED_IN_USER, {
		props: ({ data: { loggedInUser } }) => ({
			id: loggedInUser ? loggedInUser.id : null
		})
	}),
	graphql(AUTH_INFO_QUERY, {
		props: ({ data: { authInfo } }) => ({
			authInfo
		})
	}),
	graphql(UPDATE_AUTH_INFO_MUTATION, {
		props: ({ mutate }) => ({
			updateAuthInfo: variables => {
				return mutate({ variables })
			}
		})
	})
)(Header)

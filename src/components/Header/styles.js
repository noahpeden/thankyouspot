import styled, { injectGlobal } from 'styled-components'
import media from 'styled-media-query'
import { Link } from 'react-router-dom'

export const Wrapper = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
	padding: 5px 20px;
	position: fixed;
	top: 0;
	height: 60px;
	width: 100%;
	background: #fff;
	box-shadow: 0 0px 10px rgba(24, 78, 104, 0.2);
	z-index: 1;
	${media.lessThan('large')`
		padding: 16px;
		margin-top: 0;
	`};

	${media.between('medium', 'large')`
		display: flex;
		flex-direction: column;
		height: 140px;
	`};
`

export const LogoLink = styled(Link)``

export const LogoImage = styled.img`
	height: auto;
	padding: 10px;
	margin-bottom: -5px;

	${media.lessThan('large')`
		width: 200px;
	`};
`

export const LinkContainer = styled.div`
	white-space: nowrap;

	${media.lessThan('medium')`
		display: none;
	`};

	${media.between('medium', 'large')`
		align-self: stretch;
		display: flex;
		justify-content: space-around;
		align-items: center;
	`};
`

export const HeaderLinkWrapper = styled.div`
	display: flex !important;
	flex-direction: column;
	align-items: center;
`

export const HeaderLink = styled(Link)`
	color: #184e68;
	margin-right: 20px;
	&:focus,
	&:hover,
	&:visited,
	&:link,
	&:active {
		text-decoration: none;
	}

	&:hover {
		color: #fde5a7;
	}

	${media.lessThan('large')`
		font-size: ${props => (props.fullscreen ? '21px' : '16px')};
		text-align: ${props => (props.fullscreen ? 'center' : 'initial')};
		padding: ${props => (props.fullscreen ? '10px 0' : '0')};
		color: ${props => (props.fullscreen ? '#fff' : '#184e68')};
		margin-right: ${props => (props.fullscreen ? '0' : '10px')};
	`};
`

export const HeaderLinkSpan = styled.span`
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 600;
	-webkit-font-smoothing: antialiased;
`

export const SignUpLink = styled(Link)`
	background: #ff7676;
	border: 1px solid #ff7676;
	color: white;
	border-radius: 50px;
	padding: 13px 20px 12px;
	text-decoration: none;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	align-content: center;
	transition: all 0.2s ease;

	&:hover {
		background: transparent;
		color: #ff7676;
		cursor: pointer;
	}

	${media.lessThan('large')`
		font-size: ${props => (props.fullscreen ? '21px' : '16px')};
		padding: ${props => (props.fullscreen ? '16px 30px 15px' : '12px 20px 10px')};
		align-self: center;
		margin-top: 15px;
	`};

	${media.between('medium', 'large')`
		margin-top: 0;
	`};
`

injectGlobal`
	/* Position and sizing of burger button */
	.bm-burger-button {
		display: none;
		position: fixed;
		width: 22px;
		height: 18px;
		right: 15px;
		top: 22px;

		${media.lessThan('medium')`
			display: initial;
		`};
	}

	/* Color/shape of burger icon bars */
	.bm-burger-bars {
		background: #373a47;
	}

	/* Position and sizing of clickable cross button */
	.bm-cross-button {
		height: 24px;
		width: 24px;
	}

	/* Color/shape of close button cross */
	.bm-cross {
		background: #bdc3c7;
	}

	.bm-menu-wrap {
		display: none;
		width: 100% !important;

		${media.lessThan('medium')`
			display: initial;
		`};
	}

	/* General sidebar styles */
	.bm-menu {
		background: #184e68;
		padding: 2.5em 1.5em 0;
		font-size: 1.15em;
	}

	/* Morph shape necessary with bubble or elastic */
	.bm-morph-shape {
		fill: #184e68;
	}

	/* Wrapper for item list */
	.bm-item-list {
		color: #b8b7ad;
		padding: 0.8em;
	}

	/* Styling of overlay */
	.bm-overlay {
		background: rgba(0, 0, 0, 0.3);

		${media.greaterThan('medium')`
			display: none;
		`};
	}
`

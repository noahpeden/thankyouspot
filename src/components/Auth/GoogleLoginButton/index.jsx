import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { LoginButton } from './styles'

const { REACT_APP_GOOGLE_APP_ID } = process.env

class GoogleLoginButton extends Component {
	constructor(props) {
		super(props)
		this.gapi = window.gapi
		this.gapi.load('auth2', this.initSignin)
	}

	render() {
		return (
			<LoginButton onClick={this.onButtonClick}>
				<img
					src={require('../../../assets/images/icon-gmail.svg')}
					alt="gmail icon"
				/>
			</LoginButton>
		)
	}

	onButtonClick = async e => {
		e.preventDefault()

		const res = await this.auth2.signIn()
		const { id_token } = res.getAuthResponse()

		this.props.onLogin(id_token)
	}

	initSignin = async () => {
		this.auth2 = this.gapi.auth2.init({
			client_id: `${REACT_APP_GOOGLE_APP_ID}.apps.googleusercontent.com`,
			scope: 'profile email'
		})
	}
}

GoogleLoginButton.propTypes = {
	disabled: PropTypes.bool,
	onButtonClick: PropTypes.func
}

GoogleLoginButton.defaultProps = {
	disabled: false,
	onButtonClick: () => {}
}

export default GoogleLoginButton

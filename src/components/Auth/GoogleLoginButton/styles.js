import styled from 'styled-components'

export const LoginButton = styled.button`
	margin-left: 10px;
	padding: 0;
	border: none;
	background: transparent;
`

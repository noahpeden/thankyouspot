import React, { Component } from 'react'
import { compose, graphql } from 'react-apollo'
import gql from 'graphql-tag'
import { Helmet } from 'react-helmet'

import {
	Wrapper,
	FormWrapper,
	Title,
	HelpText,
	Form,
	TextInput,
	SubmitButton,
	ErrorText,
	CancelButton,
	HelpTextStrong
} from './styles'

class RequestPasswordReset extends Component {
	state = {
		email: '',
		isSubmitting: false,
		error: null,
		isSuccess: false
	}

	render() {
		const { email, error, isSubmitting, isSuccess } = this.state

		// TODO: Implement reCAPTCHA

		return (
			<React.Fragment>
				<Helmet>
					<title>Password Reset | Thank You Spot</title>
					<meta
						name="description"
						content="Reset your Thank You Spot password"
					/>
				</Helmet>

				<Wrapper>
					<FormWrapper>
						{isSuccess ? (
							<React.Fragment>
								<Title>Reset Link Sent</Title>
								<HelpText>
									The password reset link was sent to your email{' '}
									<HelpTextStrong>{email}</HelpTextStrong>
								</HelpText>
							</React.Fragment>
						) : (
							<React.Fragment>
								<Title>Reset Your Password</Title>
								<HelpText>
									Enter the email associated to your account to receive a
									password reset link
								</HelpText>
								<Form onSubmit={this.onSubmit}>
									<TextInput
										type="email"
										placeholder="Email"
										value={email}
										onChange={this.onChangeEmail}
									/>
									{error && <ErrorText>{error}</ErrorText>}
									<div>
										<SubmitButton
											value={isSubmitting ? 'Sending...' : 'Send Link'}
											disabled={isSubmitting}
										/>
										<CancelButton to="/">Cancel</CancelButton>
									</div>
								</Form>
							</React.Fragment>
						)}
					</FormWrapper>
				</Wrapper>
			</React.Fragment>
		)
	}

	onChangeEmail = e => {
		this.setState({
			email: e.target.value
		})
	}

	onSubmit = async e => {
		e.preventDefault()

		if (this.state.isSubmitting) return

		this.setState({ isSubmitting: true })

		try {
			await this.props.requestPasswordReset({
				variables: { email: this.state.email }
			})

			this.setState({ isSubmitting: false, isSuccess: true })
		} catch (err) {
			if (
				err.graphQLErrors &&
				err.graphQLErrors[0] &&
				err.graphQLErrors[0].functionError &&
				err.graphQLErrors[0].functionError.code === 42
			) {
				this.setState({
					error: err.graphQLErrors[0].functionError.userFacingMessage,
					isSubmitting: false
				})
			} else {
				this.setState({
					error: 'An unexpeced error happened. Try again later.',
					isSubmitting: false
				})
			}
		}
	}

	onRequestClose = () => {
		const { location, history, parentPath } = this.props

		if (
			location.state &&
			location.state.modal &&
			parentPath !== location.pathname
		) {
			history.goBack()
		} else {
			history.push('/')
		}
	}
}

const REQUEST_PASSWORD_RESET_MUTATION = gql`
	mutation RequestPasswordReset($email: String!) {
		requestPasswordReset(email: $email) {
			success
		}
	}
`

export default compose(
	graphql(REQUEST_PASSWORD_RESET_MUTATION, {
		props: ({ mutate }) => ({
			requestPasswordReset: options => {
				return mutate(options)
			}
		})
	})
)(RequestPasswordReset)

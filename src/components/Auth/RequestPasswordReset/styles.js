import styled from 'styled-components'
import media from 'styled-media-query'
import { Link } from 'react-router-dom'

export const Wrapper = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	min-height: 500px;
	font-family: 'Josefin Sans', sans-serif;

	${media.lessThan('large')`
		padding-top: 70px;
	`};

	${media.lessThan('medium')`
		min-height: unset;
		padding: 20px;
	`};
`

export const VerifyingWrapper = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
`

export const Loader = styled.img`
	width: 150px;
	margin-bottom: 30px;
`

export const FormWrapper = styled.div`
	max-width: 450px;
	display: flex;
	flex-direction: column;
	background-color: #dbeef7;
	padding: 40px 25px;
`

export const Title = styled.h4`
	font-family: 'Paytone One', sans-serif;
	font-size: 32px;
	color: #184e68;
	margin: 0 0 40px 0;
`

export const HelpText = styled.p`
	color: #777;
	font-weight: 400;
	-webkit-font-smoothing: antialiased;
	margin: 0 0 20px 0;
`

export const HelpTextStrong = styled.strong`
	color: #000;
`

export const Form = styled.form`
	display: flex;
	flex-direction: column;
`

export const TextInput = styled.input`
	padding: 20px;
	margin-bottom: 20px;
	border: none;
	outline: none;
	border-bottom: 2px solid transparent;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 400;
	transition: all 0.2s ease;

	&:hover,
	&:focus {
		background: #f8fbfe;
		border-bottom: 2px solid #ff7675;
	}
`

export const ErrorText = styled.p`
	color: #d34836;
	margin: 0 0 20px 0;
`

export const SubmitButton = styled.input.attrs({
	type: 'submit'
})`
	align-self: flex-start;
	border: none;
	border-radius: 30px;
	padding: 17px 30px 15px;
	font-size: 18px;
	color: #fff;
	background-color: #184e68;
	cursor: pointer;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	border: 1px solid #184e68;
	transition: all 0.2s ease;

	&:hover {
		background-color: transparent;
		color: #184e68;
		border: 1px solid #184e68;
	}

	${media.lessThan('medium')`
		margin: 0 auto;
	`};
`

export const CancelButton = styled(Link)`
	align-self: flex-start;
	border: none;
	border-radius: 30px;
	padding: 17px 30px 15px;
	font-size: 18px;
	color: #184e68;
	text-decoration: none;
	background-color: transparent;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	border: 1px solid #184e68;
	transition: all 0.2s ease;
	margin-left: 10px;
`

export const RequestResetLink = styled(Link)``

import gql from 'graphql-tag'
import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { compose, graphql } from 'react-apollo'
import { LoginButton } from './styles'

const { REACT_APP_FB_APP_ID } = process.env
const STATUS_CONNECTED = 'connected'

const fbAsyncInit = _this => () => {
	_this.FB = window.FB

	_this.FB.init({
		appId: REACT_APP_FB_APP_ID,
		xfbml: true,
		version: 'v2.12'
	})

	_this.setState({
		loading: false
	})
}

class FacebookLoginButton extends Component {
	constructor(props) {
		super(props)

		const { authInfo } = props
		const loggedIn = Boolean(authInfo.token)

		if (!loggedIn) {
			window.fbAsyncInit = fbAsyncInit(this)
			sdkLoader(document, 'script', 'facebook-jssdk')
		}
	}

	state = {
		loading: true
	}

	componentDidMount() {
		if (window.FB) {
			this.FB = window.FB
			this.setState({ loading: false })
		}
	}

	render() {
		return (
			<LoginButton
				disabled={this.props.disabled || this.state.loading}
				onClick={this.onButtonClick}
			>
				<img
					src={require('../../../assets/images/icon-fb.svg')}
					alt="facebook icon"
				/>
			</LoginButton>
		)
	}

	onButtonClick = e => {
		e.preventDefault()
		this.FB.login(this.loginResponse, { scope: 'public_profile,email' })
	}

	loginResponse = response => {
		const isLoggedIn = response.status === STATUS_CONNECTED

		if (isLoggedIn) {
			this.props.onLogin(response.authResponse.accessToken)
		} else {
			alert('Not logged in!')
		}
	}
}

function sdkLoader(d, s, id) {
	var js
	var fjs = d.getElementsByTagName(s)[0]
	if (d.getElementById(id)) return
	js = d.createElement(s)
	js.id = id
	js.src = 'https://connect.facebook.net/en_US/sdk.js'
	fjs.parentNode.insertBefore(js, fjs)
}

FacebookLoginButton.propTypes = {
	disabled: PropTypes.bool,
	onButtonClick: PropTypes.func
}

FacebookLoginButton.defaultProps = {
	disabled: false,
	onButtonClick: () => {}
}

const AUTH_INFO_QUERY = gql`
	query AuthInfo {
		authInfo @client {
			userID
			token
		}
	}
`

export default compose(
	graphql(AUTH_INFO_QUERY, {
		props: ({ data: { authInfo } }) => ({
			authInfo
		})
	})
)(FacebookLoginButton)

import gql from 'graphql-tag'
import React, { Component } from 'react'
import { compose, graphql } from 'react-apollo'
import { Helmet } from 'react-helmet'
import Modal from 'react-modal'

import {
	BottomArea,
	ErrorMessage,
	InputData,
	LeftSide,
	LoginInstead,
	RightSide,
	Row,
	SocialMedia,
	TermsConditions,
	TitleHeader,
	Wrapper,
	LoginHelperText,
	Logo,
	Title,
	SignupForm,
	SocialMediaLabel,
	IconContainer,
	InputIcon,
	Input,
	TermsConditionsLink,
	SubmitButton,
	LoginLink,
	TermsCheckboxContainer,
	TermsCheckbox
} from './styles'

import FacebookLoginButton from '../FacebookLoginButton'
import GoogleLoginButton from '../GoogleLoginButton'

const DEFAULT_REDIRECT_LOCATION = '/'

class Signup extends Component {
	state = {
		loading: false,
		error: null
	}

	render() {
		const { loading, error } = this.state

		return (
			<Modal
				isOpen={true}
				contentLabel="Thank You"
				shouldCloseOnOverlayClick={true}
				onRequestClose={this.onRequestClose}
				style={{
					// TODO: Refactor styling
					overlay: {
						display: 'flex',
						flexDirection: 'row',
						justifyContent: 'center',
						background: 'linear-gradient(to bottom right, #fde5a7, #ff7676)',
						alignItems: 'center',
						padding: 40,
						overflow: 'auto'
					},
					content: {
						position: 'static',
						border: 'none',
						background: 'none',
						display: 'flex',
						flexDirection: 'column',
						fontFamily: 'gotham',
						overflow: 'initial',
						padding: 0,
						marginBottom: 40
					}
				}}
			>
				<Helmet>
					<title>Sign Up | Thank You Spot</title>
					<meta
						name="description"
						content="Create an account on Thank You Spot to view and edit your profile and review your account activity."
					/>
				</Helmet>
				<Wrapper>
					<LeftSide />
					<RightSide>
						<TitleHeader>
							<Logo
								src={require('../../../assets/images/icon-tys.svg')}
								alt="logo"
							/>
							<Title>Sign up</Title>
						</TitleHeader>
						<SocialMedia>
							<SocialMediaLabel>Sign up using</SocialMediaLabel>
							<FacebookLoginButton
								disabled={loading}
								onLogin={this.onFacebookLogin}
							/>
							<GoogleLoginButton
								disabled={loading}
								onLogin={this.onGoogleLogin}
							/>
						</SocialMedia>
						<SignupForm onSubmit={this.onSubmit}>
							<InputData>
								<Row>
									<IconContainer>
										<InputIcon
											src={require('../../../assets/images/form-name.svg')}
											alt="user icon"
										/>
									</IconContainer>
									<Input name="name" type="text" placeholder="Name" />
								</Row>
								<Row>
									<IconContainer>
										<InputIcon
											src={require('../../../assets/images/form-email.svg')}
											alt="mail icon"
										/>
									</IconContainer>
									<Input name="email" type="email" placeholder="Email" />
								</Row>
								<Row>
									<IconContainer>
										<InputIcon
											src={require('../../../assets/images/form-password.svg')}
											alt="lock icon"
										/>
									</IconContainer>
									<Input
										name="password"
										type="password"
										placeholder="Password"
									/>
								</Row>
							</InputData>

							<BottomArea>
								{error ? <ErrorMessage>{error.message}</ErrorMessage> : null}
								<TermsCheckboxContainer>
									<TermsCheckbox />
									<TermsConditions>
										{
											"By checking this box, I consent to ThankYouSpot collecting and storing my data through the submission of this form. I understand that ThankYouSpot will use the information collected to provide me with a ThankYouSpot account, as well as updates and marketing via email and customized online advertising. Furthermore, I verify that I am 16 years of age or older and do not need parental consent. By submitting this form, I agree to ThankYouSpot's "
										}
										<TermsConditionsLink to="/terms">
											Terms & Conditions
										</TermsConditionsLink>
									</TermsConditions>
								</TermsCheckboxContainer>
								<SubmitButton
									type="submit"
									value={loading ? 'Logging in...' : 'Sign Up'}
									disabled={loading}
								/>
								<LoginInstead>
									<LoginHelperText>Already a member?</LoginHelperText>
									<LoginLink
										to={{
											pathname: '/auth/login',
											state: { modal: true }
										}}
										replace
									>
										LOG IN!
									</LoginLink>
								</LoginInstead>
							</BottomArea>
						</SignupForm>
					</RightSide>
				</Wrapper>
			</Modal>
		)
	}

	onRequestClose = () => {
		const { location, history, parentPath } = this.props

		if (
			location.state &&
			location.state.modal &&
			parentPath !== location.pathname
		) {
			history.goBack()
		} else {
			history.push('/')
		}
	}

	onSubmit = async e => {
		e.preventDefault()

		if (!e.target.agreeTerms.checked) {
			return
		}

		this.setState({ loading: true })

		try {
			const response = await this.props.signupUser({
				variables: {
					email: e.target.email.value,
					password: e.target.password.value,
					name: e.target.name.value
				}
			})

			await this.props.updateAuthInfo({
				variables: {
					userID: response.data.signupUser.id,
					token: response.data.signupUser.token
				}
			})
			this.redirectAccordingly(response.data.signupUser.id)
		} catch (err) {
			this.setState({
				loading: false,
				error: err
			})
		}
	}

	onGoogleLogin = async googleToken => {
		if (!this.state.loading) {
			this.setState(state => ({ loading: true }))

			try {
				const response = await this.props.authenticateGoogleUser({
					variables: { googleToken }
				})

				const { id, token } = response.data.authenticateGoogleUser
				await this.updateAuth(id, token)
				this.redirectAccordingly(id)
				this.setState(state => ({ loading: false }))
			} catch (err) {
				this.state(state => ({ error: err }))
			}
		}
	}

	onFacebookLogin = async facebookToken => {
		if (!this.state.loading) {
			this.setState(state => ({ loading: true }))

			try {
				const response = await this.props.authenticateFacebookUser({
					variables: { facebookToken }
				})

				const { id, token } = response.data.authenticateFacebookUser
				await this.updateAuth(id, token)
				this.redirectAccordingly(id)
				this.setState(state => ({ loading: false }))
			} catch (err) {
				this.setState(state => ({ error: err }))
			}
		}
	}

	redirectAccordingly = profileID => {
		const { location, history, parentPath } = this.props

		if (
			location.state &&
			location.state.modal &&
			parentPath !== location.pathname
		) {
			history.push(parentPath, { modal: false })
		} else {
			this.goProfile(profileID)
		}
	}

	goProfile = id => {
		const { history } = this.props
		history.push(`/${id}`, { modal: false })
	}

	goHome = () => {
		const { history, location, parentPath } = this.props
		history.push(
			location.pathname !== parentPath ? parentPath : DEFAULT_REDIRECT_LOCATION,
			{
				modal: false
			}
		)
	}

	updateAuth = async (userID, token) => {
		return await this.props.updateAuthInfo({ variables: { userID, token } })
	}
}

const SIGNUP_MUTATION = gql`
	mutation SignupUser($email: String!, $password: String!, $name: String!) {
		signupUser(email: $email, password: $password, name: $name) {
			id
			token
		}
	}
`

const UPDATE_AUTH_INFO_MUTATION = gql`
	mutation UpdateAuthInfo($userID: String, $token: String) {
		updateAuthInfo(userID: $userID, token: $token) @client
	}
`

const FB_LOGIN_MUTATION = gql`
	mutation FbLogin($facebookToken: String!) {
		authenticateFacebookUser(facebookToken: $facebookToken) {
			id
			token
		}
	}
`

const GOOGLE_LOGIN_MUTATION = gql`
	mutation GoogleLogin($googleToken: String!) {
		authenticateGoogleUser(googleToken: $googleToken) {
			id
			token
		}
	}
`

export default compose(
	graphql(SIGNUP_MUTATION, {
		props: ({ mutate }) => ({
			signupUser: options => {
				return mutate(options)
			}
		})
	}),
	graphql(UPDATE_AUTH_INFO_MUTATION, {
		props: ({ mutate }) => ({
			updateAuthInfo: options => {
				return mutate(options)
			}
		})
	}),
	graphql(FB_LOGIN_MUTATION, {
		props: ({ mutate }) => ({
			authenticateFacebookUser: options => {
				return mutate(options)
			}
		})
	}),
	graphql(GOOGLE_LOGIN_MUTATION, {
		props: ({ mutate }) => ({
			authenticateGoogleUser: options => {
				return mutate(options)
			}
		})
	})
)(Signup)

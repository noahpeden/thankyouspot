import React, { Component } from 'react'
import { compose, graphql } from 'react-apollo'
import { Helmet } from 'react-helmet'
import gql from 'graphql-tag'
import Modal from 'react-modal'

import {
	Wrapper,
	LeftSide,
	RightSide,
	TitleHeader,
	SocialMedia,
	InputData,
	Row,
	BottomArea,
	TopBottom,
	ForgotPassword,
	ErrorMessage,
	BottomBottom,
	SignupInstead,
	SignupHelperText,
	Logo,
	Title,
	SocialMediaLabel,
	IconContainer,
	InputIcon,
	Input,
	LoginForm,
	SignupLink,
	ForgotPasswordLink,
	SubmitButton
} from './styles'

import FacebookLoginButton from '../FacebookLoginButton'
import GoogleLoginButton from '../GoogleLoginButton'

const DEFAULT_REDIRECT_LOCATION = '/'

class Login extends Component {
	constructor(props) {
		super(props)

		const { authInfo } = props

		const loggedIn = Boolean(authInfo.token)
		if (loggedIn) this.goHome()
	}

	state = {
		loading: false,
		error: null
	}

	render() {
		const { loading, error } = this.state

		return (
			<Modal
				isOpen={true}
				contentLabel="Thank You"
				shouldCloseOnOverlayClick={true}
				onRequestClose={this.onRequestClose}
				style={{
					// TODO: Refactor styling
					overlay: {
						display: 'flex',
						flexDirection: 'row',
						justifyContent: 'center',
						background: 'linear-gradient(to bottom right, #fde5a7, #ff7676)',
						alignItems: 'center',
						padding: 40,
						overflow: 'auto'
					},
					content: {
						position: 'static',
						border: 'none',
						background: 'none',
						display: 'flex',
						flexDirection: 'column',
						fontFamily: 'gotham',
						overflow: 'initial',
						padding: 0,
						marginBottom: 40
					}
				}}
			>
				<Helmet>
					<title>Log In | Thank You Spot</title>
					<meta
						name="description"
						content="Log in to your personal Thank You Spot account to edit your profile and review your activity."
					/>
				</Helmet>
				<Wrapper>
					<LeftSide />
					<RightSide>
						<TitleHeader>
							<Logo
								src={require('../../../assets/images/icon-tys.svg')}
								alt="logo"
							/>
							<Title>Log In</Title>
						</TitleHeader>
						<SocialMedia>
							<SocialMediaLabel>Log in using</SocialMediaLabel>
							<FacebookLoginButton
								disabled={loading}
								onLogin={this.onFacebookLogin}
							/>
							<GoogleLoginButton
								disabled={loading}
								onLogin={this.onGoogleLogin}
							/>
						</SocialMedia>
						<LoginForm onSubmit={this.onSubmit}>
							<InputData>
								<Row>
									<IconContainer>
										<InputIcon
											src={require('../../../assets/images/form-email.svg')}
											alt="mail icon"
										/>
									</IconContainer>
									<Input name="email" type="email" placeholder="Email" />
								</Row>
								<Row>
									<IconContainer>
										<InputIcon
											src={require('../../../assets/images/form-password.svg')}
											alt="lock icon"
										/>
									</IconContainer>
									<Input
										name="password"
										type="password"
										placeholder="Password"
									/>
								</Row>
							</InputData>

							<BottomArea>
								<TopBottom>
									{error ? <ErrorMessage>{error.message}</ErrorMessage> : null}
									<ForgotPassword>
										<ForgotPasswordLink
											to={{
												pathname: '/auth/request-password-reset'
											}}
										>
											Forgot Password?
										</ForgotPasswordLink>
									</ForgotPassword>
								</TopBottom>
								<BottomBottom>
									<SubmitButton
										type="submit"
										value={loading ? 'Logging in...' : 'Log in'}
										disabled={loading}
									/>
									<SignupInstead>
										<SignupHelperText>New to this website?</SignupHelperText>
										<SignupLink
											to={{
												pathname: '/auth/signup',
												state: { modal: true }
											}}
											replace
										>
											SIGN UP!
										</SignupLink>
									</SignupInstead>
								</BottomBottom>
							</BottomArea>
						</LoginForm>
					</RightSide>
				</Wrapper>
			</Modal>
		)
	}

	onRequestClose = () => {
		const { location, history, parentPath } = this.props

		if (
			location.state &&
			location.state.modal &&
			parentPath !== location.pathname
		) {
			history.goBack()
		} else {
			history.push('/')
		}
	}

	onSubmit = async e => {
		e.preventDefault()

		if (!this.state.loading) {
			this.setState({ loading: true })

			try {
				const response = await this.props.authenticateUser({
					variables: {
						email: e.target.email.value,
						password: e.target.password.value
					}
				})

				const { id, token } = response.data.authenticateUser
				await this.updateAuth(id, token)
				this.redirectAccordingly(id)
			} catch ({ graphQLErrors = [] }) {
				const error = { message: 'Something went wrong' }
				if (graphQLErrors.length) {
					error.message = graphQLErrors.reduce(
						(message, error) =>
							`${message} ${error.functionError.userFacingMessage}`,
						''
					)
				}
				this.setState({
					loading: false,
					error
				})
			}
		}
	}

	onGoogleLogin = async googleToken => {
		if (!this.state.loading) {
			this.setState(state => ({ loading: true }))

			try {
				const response = await this.props.authenticateGoogleUser({
					variables: { googleToken }
				})

				const { id, token } = response.data.authenticateGoogleUser
				await this.updateAuth(id, token)
				this.setState(state => ({ loading: false }))
				this.redirectAccordingly(id)
			} catch (err) {
				this.state(state => ({ error: err }))
			}
		}
	}

	onFacebookLogin = async facebookToken => {
		if (!this.state.loading) {
			this.setState(state => ({ loading: true }))

			try {
				const response = await this.props.authenticateFacebookUser({
					variables: { facebookToken }
				})

				const { id, token } = response.data.authenticateFacebookUser
				await this.updateAuth(id, token)
				this.setState(state => ({ loading: false }))
				this.redirectAccordingly(id)
			} catch (err) {
				this.state(state => ({ error: err }))
			}
		}
	}

	redirectAccordingly = profileID => {
		const { location, history, parentPath } = this.props

		if (
			location.state &&
			location.state.modal &&
			parentPath !== location.pathname
		) {
			history.push(parentPath, { modal: false })
		} else {
			this.goProfile(profileID)
		}
	}

	goProfile = id => {
		const { history } = this.props
		history.push(`/${id}`, { modal: false })
	}

	goHome = () => {
		const { history, location, parentPath } = this.props
		history.push(
			location.pathname !== parentPath ? parentPath : DEFAULT_REDIRECT_LOCATION,
			{
				modal: false
			}
		)
	}

	updateAuth = async (userID, token) => {
		return await this.props.updateAuthInfo({ variables: { userID, token } })
	}
}

const AUTH_INFO_QUERY = gql`
	query AuthInfo {
		authInfo @client {
			userID
			token
		}
	}
`

const LOGIN_MUTATION = gql`
	mutation Login($email: String!, $password: String!) {
		authenticateUser(email: $email, password: $password) {
			id
			token
		}
	}
`

const UPDATE_AUTH_INFO_MUTATION = gql`
	mutation UpdateAuthInfo($userID: String, $token: String) {
		updateAuthInfo(userID: $userID, token: $token) @client
	}
`

const FB_LOGIN_MUTATION = gql`
	mutation FbLogin($facebookToken: String!) {
		authenticateFacebookUser(facebookToken: $facebookToken) {
			id
			token
		}
	}
`

const GOOGLE_LOGIN_MUTATION = gql`
	mutation GoogleLogin($googleToken: String!) {
		authenticateGoogleUser(googleToken: $googleToken) {
			id
			token
		}
	}
`

export default compose(
	graphql(AUTH_INFO_QUERY, {
		props: ({ data: { authInfo } }) => ({
			authInfo
		})
	}),
	graphql(LOGIN_MUTATION, {
		props: ({ mutate }) => ({
			authenticateUser: options => {
				return mutate(options)
			}
		})
	}),
	graphql(UPDATE_AUTH_INFO_MUTATION, {
		props: ({ mutate }) => ({
			updateAuthInfo: options => {
				return mutate(options)
			}
		})
	}),
	graphql(FB_LOGIN_MUTATION, {
		props: ({ mutate }) => ({
			authenticateFacebookUser: options => {
				return mutate(options)
			}
		})
	}),
	graphql(GOOGLE_LOGIN_MUTATION, {
		props: ({ mutate }) => ({
			authenticateGoogleUser: options => {
				return mutate(options)
			}
		})
	})
)(Login)

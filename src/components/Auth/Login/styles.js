import styled from 'styled-components'
import { Link } from 'react-router-dom'
import media from 'styled-media-query'

const bgImage = require('../../../assets/images/stock-login.jpg')

export const Wrapper = styled.div`
	display: flex;
	flex-direction: row;
	${media.lessThan('medium')`
		max-width: 395px;
	`};
`

export const LeftSide = styled.div`
	display: flex;
	width: 395px;
	height: 600px;
	box-shadow: 0 2px 42px -6px #184e68;
	background-image: url(${bgImage});
	background-size: cover;
	background-position: center;
	${media.lessThan('medium')`
		visibility: hidden;
		width: 0;
		height: 0;
	`};
`

export const RightSide = styled.div`
	display: flex;
	flex-direction: column;
	width: 395px;
	height: 600px;
	box-shadow: 0 2px 42px -6px #184e68;
	background: white;
	text-align: center;
	${media.lessThan('medium')`
		width: 100%;
		height: auto;
	`};
`

export const TitleHeader = styled.div`
	display: flex;
	flex-direction: row;
	padding: 30px;
	align-items: center;
	justify-content: center;
`

export const Logo = styled.img`
	display: block;
	padding-right: 20px;
`

export const Title = styled.h2`
	font-size: 38px;
	color: #184e68;
	margin: 0;
	color: #184e68;
	text-shadow: -4px 4px #dbeef7;
	font-family: 'Paytone One', sans-serif;
	text-transform: uppercase;
	-webkit-font-smoothing: antialiased;
	letter-spacing: 1px;
`

export const SocialMedia = styled.div`
	display: flex;
	flex-direction: row;
	justify-content: center;
	align-items: center;
	background: #dbeef7;
	padding-top: 20px;
`

export const SocialMediaLabel = styled.p`
	text-transform: uppercase;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 300;
	-webkit-font-smoothing: antialiased;
`

export const LoginForm = styled.form`
	display: flex;
	flex-direction: column;
	padding-bottom: 15px;
	flex: 1;
`

export const InputData = styled.div`
	width: 100%;
	background: #dbeef7;
	padding: 10px 40px;
`

export const Row = styled.div`
	display: flex;
	flex-direction: row;
	background: white;
	margin: 15px 0;
`

export const IconContainer = styled.div`
	display: flex;
	width: 60px;
	align-items: center;
	justify-content: center;
	border-right: 1px solid #dbeef7;
`

export const InputIcon = styled.img`
	width: 22px;
`

export const Input = styled.input`
	flex: 1;
	border: none;
	padding: 15px;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 400;
	-webkit-font-smoothing: antialiased;
`

export const BottomArea = styled.div`
	display: flex;
	justify-content: space-between;
	flex-direction: column;
	flex: 1;
`

export const TopBottom = styled.div``

export const ForgotPassword = styled.p`
	font-size: 15px;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 300;
	-webkit-font-smoothing: antialiased;
`

export const ForgotPasswordLink = styled(Link)`
	text-decoration: none;
	color: #3a3d3f;
	opacity: 0.7;
	&:hover {
		opacity: 0.4;
	}
`

export const ErrorMessage = styled.p`
	font-size: 15px;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 300;
	color: #d34836;
`

export const BottomBottom = styled.div``

export const SubmitButton = styled.input`
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	font-size: 18px;
	background: #ff7676;
	text-decoration: none;
	border: 1px solid #ff7676;
	color: white;
	border-radius: 50px;
	padding: 15px 30px;
	&:hover {
		background: transparent;
		color: #ff7676;
		cursor: pointer;
	}
`

export const SignupInstead = styled.p`
	margin-top: 20px;
	font-size: 14px;
	color: #3a3d3f;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 300;
	-webkit-font-smoothing: antialiased;
`

export const SignupHelperText = styled.span`
	margin-right: 5px;
`

export const SignupLink = styled(Link)`
	text-decoration: none;
	color: #ff8281;
	&:hover {
		opacity: 0.5;
	}
`

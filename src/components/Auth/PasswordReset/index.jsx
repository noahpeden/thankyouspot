import React, { Component } from 'react'
import { compose, graphql } from 'react-apollo'
import gql from 'graphql-tag'
import qs from 'qs'
import { Helmet } from 'react-helmet'

import {
	Wrapper,
	FormWrapper,
	Title,
	HelpText,
	Form,
	TextInput,
	SubmitButton,
	CancelButton,
	ErrorText,
	RequestResetLink,
	VerifyingWrapper,
	Loader
} from '../RequestPasswordReset/styles'

class PasswordReset extends Component {
	state = {
		isVerified: false,
		isVerifying: true,
		isSubmitting: false,
		password: '',
		id: null,
		token: null,
		error: null
	}

	componentDidMount() {
		this.verifyToken()
	}

	render() {
		const { isVerifying, isVerified, isSubmitting, error } = this.state

		if (isVerifying) {
			return (
				<Wrapper>
					<VerifyingWrapper>
						<Loader src={require('../../../assets/images/logo-loop.gif')} />
						<Title>Verifying reset link...</Title>
					</VerifyingWrapper>
				</Wrapper>
			)
		}

		if (!isVerified) {
			return (
				<Wrapper>
					<Helmet>
						<title>Password Reset | Thank You Spot</title>
						<meta
							name="description"
							content="Reset your Thank You Spot password"
						/>
					</Helmet>
					<FormWrapper>
						<Title>Reset link is invalid!</Title>
						<HelpText>
							This reset link seems to be invalid or has expired. Click{' '}
							<RequestResetLink to="/auth/request-password-reset">
								here
							</RequestResetLink>{' '}
							to get another reset link.
						</HelpText>
					</FormWrapper>
				</Wrapper>
			)
		}

		return (
			<Wrapper>
				<Helmet>
					<title>Password Reset | Thank You Spot</title>
					<meta
						name="description"
						content="Reset your Thank You Spot password"
					/>
				</Helmet>
				<FormWrapper>
					<Title>Reset Your Password</Title>
					<HelpText>Enter a new password</HelpText>
					<Form onSubmit={this.onSubmit}>
						<TextInput
							type="password"
							placeholder="Password"
							onChange={this.onChangePassword}
						/>
						{error && <ErrorText>{error}</ErrorText>}
						<div>
							<SubmitButton
								value={isSubmitting ? 'Saving...' : 'Save and Log in'}
								disabled={isSubmitting}
							/>
							<CancelButton to="/">Cancel</CancelButton>
						</div>
					</Form>
				</FormWrapper>
			</Wrapper>
		)
	}

	verifyToken = async () => {
		const params = qs.parse(this.props.location.search, {
			ignoreQueryPrefix: true,
			decoder: function(str) {
				return str
			}
		})

		const { id, token } = params

		try {
			const response = await this.props.sendVerifyTokenMutation({
				variables: { id, token }
			})

			this.setState({
				id,
				token,
				isVerifying: false,
				isVerified: response.data.validatePasswordResetToken.isValid
			})
		} catch (err) {
			this.setState({ isVerifying: false, isVerified: false })
		}
	}

	onChangePassword = e => {
		this.setState({
			password: e.target.value
		})
	}

	onSubmit = async e => {
		e.preventDefault()

		const { id, token, password } = this.state

		this.setState({ isSubmitting: true })

		try {
			const response = await this.props.resetPassword({
				variables: { id, token, password }
			})

			this.setState({ isSubmitting: false })

			const { token: authToken } = response.data.resetPassword
			await this.props.updateAuthInfo({
				variables: { userID: id, token: authToken }
			})

			const { history } = this.props
			history.push(`/${id}`)
		} catch (err) {
			if (
				err.graphQLErrors &&
				err.graphQLErrors[0] &&
				err.graphQLErrors[0].functionError &&
				err.graphQLErrors[0].functionError.userFacingMessage
			) {
				this.setState({
					error: err.graphQLErrors[0].functionError.userFacingMessage,
					isSubmitting: false
				})
			} else {
				this.setState({
					error: 'An unexpeced error happened. Try again later.',
					isSubmitting: false
				})
			}
		}
	}
}

const VERIFY_TOKEN_MUTATION = gql`
	mutation ValidatePasswordResetToken($id: ID!, $token: String!) {
		validatePasswordResetToken(id: $id, token: $token) {
			isValid
		}
	}
`

const RESET_PASSWORD_MUTATION = gql`
	mutation ResetPassword($password: String!, $token: String!, $id: String!) {
		resetPassword(password: $password, token: $token, id: $id) {
			token
		}
	}
`

const UPDATE_AUTH_INFO_MUTATION = gql`
	mutation UpdateAuthInfo($userID: String, $token: String) {
		updateAuthInfo(userID: $userID, token: $token) @client
	}
`

export default compose(
	graphql(VERIFY_TOKEN_MUTATION, {
		props: ({ mutate }) => ({
			sendVerifyTokenMutation: options => {
				return mutate(options)
			}
		})
	}),
	graphql(RESET_PASSWORD_MUTATION, {
		props: ({ mutate }) => ({
			resetPassword: options => {
				return mutate(options)
			}
		})
	}),
	graphql(UPDATE_AUTH_INFO_MUTATION, {
		props: ({ mutate }) => ({
			updateAuthInfo: options => {
				return mutate(options)
			}
		})
	})
)(PasswordReset)

import styled, { injectGlobal } from 'styled-components'
import media from 'styled-media-query'
import { Link } from 'react-router-dom'

injectGlobal`
	.ReactModal__Body--open,
	.ReactModal__Html--open {
		overflow: hidden;
		padding-right: 17px; /* To adjust for scrollbar width */
	}

	.ReactModal__Overlay {
		z-index: 1001;
		${media.lessThan('medium')`
			padding: 50px 20px 20px !important;
		`};
	}
	.ReactModal__Content {
		${media.lessThan('medium')`
			width: 100% !important;
		`};
	}
`

export const DetailsWrapper = styled.div`
	margin-top: 30px;
	margin-bottom: 12px;
`

export const DetailsTo = styled.h4`
	color: #ffffff;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 300;
	text-decoration: none;
	margin: 0 0 8px;
`
export const Person = styled.a`
	color: #ffffff;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 500;
	text-decoration: none;
	margin-bottom: 0;
	a {
		text-decoration: none;
		&:hover {
			color: #ff7676;
		}
	}
`
export const ProfileLink = styled(Link)`
	color: #ffffff;
	font-family: 'Josefin Sans', sans-serif;
	text-decoration: none;
`

export const DateCreated = styled.label`
	color: #ffffff;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 200;
	-webkit-text-decoration: none;
	text-decoration: none;
	opacity: 0.5;
`

export const DetailName = styled.span`
	color: #ffffff;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 500;
	text-decoration: none;
	margin: 0;
	a {
		text-decoration: none;
		&:hover {
			color: #ff7676;
		}
	}
`

export const ThankYouContainer = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
	font-family: 'Josefin Sans', sans-serif;
	background-color: rgba(0, 0, 0, 0.8);
	${media.lessThan('1024px')`
		flex-direction: column;
		width: 100%;
	`};
	& > div {
		${media.lessThan('460px')`
			width: 100% !important;
		`};
	}
`

export const SharingWrapper = styled.div`
	position: absolute;
	right: -60px;
	display: flex;
	flex-direction: column;
	top: calc(50% - 112px);

	background: #fff;
    padding: 20px 12px 16px;
    border-radius: 7px;
    box-shadow: 0 2px 15px rgba(24,78,104,0.15);

	.SocialMediaShareButton {
		cursor: pointer;
		margin-bottom: 16px;
		&:last-child {
			margin-bottom: 0;
		}	
	}
	${media.lessThan('medium')`
		display: none;
	`};
}
`

export const SharingWrapper2 = styled.div`
	display: none;
	${media.lessThan('medium')`
		display: auto;
		display: flex;
		flex-direction: row;
		background: #fff;
		padding: 10px 0 7px;
		border-radius: 7px;
		box-shadow: 0 2px 15px rgba(24,78,104,0.15);
		width: 180px;
		justify-content: center;
		margin: 20px auto;
		align-items: center;
		.SocialMediaShareButton {
			cursor: pointer;
			margin-right: 16px;
			&:last-child {
				margin-right: 0;
			}	
		}
	`};
}
`

export const ThankYouCard = styled.div`
	position: relative;
	display: flex;
	flex-direction: column;
	align-items: center;
	width: 445px;
	height: 650px;
	padding: 40px;
	background-color: #fff;
	box-shadow: 0 2px 14px -5px rgba(24, 79, 105, 0.43);
	${media.lessThan('460px')`
		width: 100%;
		height: auto;
	`};
`

export const Intro = styled.h1`
	color: #184e68;
	font-size: 45px;
	text-align: center;
	font-family: 'Concert One', cursive;
	-webkit-font-smoothing: antialiased;
	letter-spacing: 1px;
	margin: 0;
	word-break: break-word;
	${media.lessThan('medium')`
		font-size: 30px;
	`};
`

export const TextContainer = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	flex: 1;
	overflow: hidden;
	width: 100%;
	${media.lessThan('medium')`
		min-height: 200px;
		max-height: 430px;
	`};
`

export const TextScrollWrapper = styled.div`
	overflow: auto;
	margin: 16px -30px 16px 0;
	padding-right: 30px;
	width: 100%:
`

export const Text = styled.p`
	margin: 0;
	font-weight: 200;
	font-size: 30px;
	text-align: center;
	color: #3a3d3f;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 300;
	word-break: break-word;
	-webkit-font-smoothing: antialiased;
	line-height: 1.2em;
	${media.lessThan('medium')`
		font-size: 20px;
	`};
`

export const AuthorAvatar = styled.img`
	width: 60px;
	height: 60px;
	border-radius: 50%;
	border: 6px solid #ff7676;
	margin-bottom: 10px;
	object-fit: cover;
	${media.lessThan('medium')`
		height: 50px;
		width: 50px;
	`};
`

export const Closing = styled.strong`
	font-family: 'Caveat', cursive;
	font-size: 45px;
	font-weight: 200;
	text-align: center;
	color: #6eaecc;
	word-break: break-word;
	${media.lessThan('medium')`
		font-size: 30px;
	`};
`

export const Photo = styled.div`
	width: 445px;
	height: 650px;
	box-shadow: 0 2px 14px -5px rgba(24,79,105,0.43);
	background-image: url('${props => props.image}');
	background-size: cover;
	background-position: center;
	display: flex;

	${media.lessThan('medium')`
		width: 100%;
	`};
`

export const Video = styled.video`
	width: 445px;
	height: 650px;
	box-shadow: 0 2px 14px -5px rgba(24, 79, 105, 0.43);
	background-color: #333;
	background-size: cover;
	background-position: center;
	display: flex;
	${media.lessThan('medium')`
		width: 100%;
	`};
`

export const ModalCloseButton = styled.a`
	position: absolute;
	right: 0;
	top: 0;
	margin: 1rem;
	cursor: pointer;
	width: 32px;
	height: 32px;
	opacity: 0.8;
	&:hover {
		opacity: 1;
	}
	&:before {
		position: absolute;
		left: 15px;
		content: ' ';
		height: 33px;
		width: 3px;
		background-color: #ffffff;
		transform: rotate(45deg);
	}
	&:after {
		position: absolute;
		left: 15px;
		content: ' ';
		height: 33px;
		width: 3px;
		background-color: #ffffff;
		transform: rotate(-45deg);
	}
`

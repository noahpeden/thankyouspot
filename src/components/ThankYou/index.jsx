import React, { Component } from 'react'
import Modal from 'react-modal'
import { compose, graphql } from 'react-apollo'
import { Helmet } from 'react-helmet'
import gql from 'graphql-tag'
import NotFound from '../Redirect/NotFound'
import {
	ModalCloseButton,
	ThankYouContainer,
	ThankYouCard,
	Intro,
	TextContainer,
	Text,
	AuthorAvatar,
	Closing,
	Video,
	TextScrollWrapper,
	DetailsWrapper,
	DetailsTo,
	DetailName,
	SharingWrapper,
	DateCreated,
	SharingWrapper2,
	Person,
	ProfileLink
} from './styles'

import {
	FacebookShareButton,
	TwitterShareButton,
	PinterestShareButton,
	LinkedinShareButton
} from 'react-share'
import LazyLoadPhoto from '../LazyLoad/LazyLoadPhoto'
import TimeAgo from 'javascript-time-ago'
import en from 'javascript-time-ago/locale/en'

const modalCloseImage = require('../../assets/modal-close.svg')
const fbIcon = require('../../assets/images/facebook-icon.svg')
const twitterIcon = require('../../assets/images/twitter-icon.svg')
const pinterestIcon = require('../../assets/images/pinterest-icon.svg')
const linkedinIcon = require('../../assets/images/linkedin-icon.svg')
// const mailIcon = require('../../assets/images/envelope-icon.svg')

// Initialize TimeAgo
TimeAgo.locale(en)
const timeAgo = new TimeAgo('en-US')

Modal.setAppElement('#root')

class ThankYou extends Component {
	render() {
		const { loading, post = {} } = this.props

		if (!loading && !post) {
			return <NotFound />
		}

		const { author, text, image, customDesign } = post
		const title = `${author ? author.name : 'Anon'} on ThankYouSpot: "${text}"`
		const customDesignUrl =
			customDesign &&
			`https://s3.amazonaws.com/www.thankyouspot.com/custom/${
				customDesign.image
			}`
		const { href: shareUrl, hostname, protocol } = window.location
		const defaultImage = `${protocol}//${hostname}${require('../../assets/images/default-share.jpg')}`
		const imageContent =
			image || (customDesign && customDesignUrl) || defaultImage

		if (loading) {
			// TODO: Show loading component
			return null
		}

		return (
			<React.Fragment>
				<Helmet>
					<title>{title}</title>
					<meta name="description" content={text} />

					<meta itemprop="name" content={title} />
					<meta itemprop="description" content={text} />
					<meta itemprop="image" content={imageContent} />

					<meta name="twitter:card" content="summary_large_image" />
					<meta name="twitter:site" content="@ThankYouSpot" />
					<meta name="twitter:title" content={title} />
					<meta name="twitter:description" content={text} />

					<meta name="twitter:image:src" content={imageContent} />

					<meta property="og:title" content={title} />
					<meta property="og:type" content="article" />
					<meta property="og:url" content={window.location.href} />
					<meta property="og:image" content={imageContent} />
					<meta property="og:image:type" content="image/jpeg" />
					<meta property="og:image:width" content="620" />
					<meta property="og:image:height" content="541" />
					<meta property="og:description" content={text} />
					<meta property="og:site_name" content="ThankYouSpot" />
					<meta
						property="fb:app_id"
						content={process.env.REACT_APP_FB_APP_ID}
					/>
				</Helmet>
				<Modal
					isOpen={true}
					contentLabel="Thank You"
					shouldCloseOnOverlayClick={true}
					onRequestClose={this.onRequestClose}
					style={{
						// TODO: Refactor styling
						overlay: {
							display: 'flex',
							flexDirection: 'column',
							alignItems: 'center',
							justifyContent: 'flex-start',
							padding: 40,
							overflow: 'auto',
							background: 'rgba(0, 0, 0, 0.8)'
						},
						content: {
							position: 'static',
							border: 'none',
							background: 'none',
							fontFamily: 'gotham',
							overflow: 'initial',
							padding: 0
						}
					}}
				>
					<ModalCloseButton onClick={this.onRequestClose} />
					<ThankYouContainer>
						{post.image && <LazyLoadPhoto src={post.image} />}
						{post.video && (
							<Video controls autoplay>
								<source src={post.video} />
							</Video>
						)}
						{post.customDesign && (
							<LazyLoadPhoto
								src={`https://s3.amazonaws.com/www.thankyouspot.com/custom/${
									post.customDesign.image
								}`}
							/>
						)}
						<ThankYouCard>
							<Intro>{post.intro}</Intro>
							<TextContainer>
								<TextScrollWrapper>
									<Text>{post.text}</Text>
								</TextScrollWrapper>
							</TextContainer>
							{post.author ? (
								<AuthorAvatar
									src={
										post.author.picture ||
										require('../../assets/images/default-avatar.svg')
									}
								/>
							) : null}
							<Closing>{post.closing}</Closing>
							<SharingWrapper>
								<FacebookShareButton url={shareUrl}>
									<img src={fbIcon} alt="Facebook Share" />
								</FacebookShareButton>
								<TwitterShareButton
									url={shareUrl}
									title={title}
									via="ThankYouSpot"
								>
									<img src={twitterIcon} alt="Twitter Share" />
								</TwitterShareButton>
								<PinterestShareButton url={shareUrl} media={imageContent}>
									<img src={pinterestIcon} alt="Pinterest Share" />
								</PinterestShareButton>
								<LinkedinShareButton url={shareUrl}>
									<img src={linkedinIcon} alt="LinkedIn Share" />
								</LinkedinShareButton>
							</SharingWrapper>
						</ThankYouCard>
					</ThankYouContainer>
					<SharingWrapper2>
						<FacebookShareButton url={shareUrl}>
							<img src={fbIcon} alt="Facebook Share" />
						</FacebookShareButton>
						<TwitterShareButton url={shareUrl} title={title} via="ThankYouSpot">
							<img src={twitterIcon} alt="Twitter Share" />
						</TwitterShareButton>
						<PinterestShareButton url={shareUrl} media={imageContent}>
							<img src={pinterestIcon} alt="Pinterest Share" />
						</PinterestShareButton>
						<LinkedinShareButton url={shareUrl}>
							<img src={linkedinIcon} alt="LinkedIn Share" />
						</LinkedinShareButton>
					</SharingWrapper2>
					<DetailsWrapper>
						<DetailsTo>
							To:{' '}
							<DetailName>
								<Person>{getRecipient(post)}</Person>
							</DetailName>
						</DetailsTo>
						<DetailsTo>
							From: <DetailName>{getAuthor(post)}</DetailName>
						</DetailsTo>
						<DateCreated>{getCardAge(post)}</DateCreated>
					</DetailsWrapper>
				</Modal>
			</React.Fragment>
		)
	}

	onRequestClose = () => {
		const { location, history, parentPath, match } = this.props

		if (
			location.state &&
			location.state.modal &&
			parentPath !== location.pathname
		) {
			history.push(parentPath, { modal: false, retainScroll: true })
		} else {
			history.push(`/${match.params.userID || 'global-thank-yous'}`, {
				modal: false,
				retainScroll: true
			})
		}
	}
}

function getRecipient(post) {
	const { recipient, name, description } = post

	if (recipient) {
		return (
			<ProfileLink to={{ pathname: `/${recipient.id}` }}>
				{recipient.name}
			</ProfileLink>
		)
	} else if (description && description.trim() !== '') {
		return description
	} else {
		return name
	}
}

function getAuthor(post) {
	const { author, address } = post

	if (author) {
		return (
			<ProfileLink to={{ pathname: `/${author.id}` }}>
				{author.name}
			</ProfileLink>
		)
	} else if (address) {
		return address.name
	} else {
		return 'Anonymous'
	}
}

function getCardAge(post) {
	const dateCreated = new Date(post.createdAt)
	return timeAgo.format(dateCreated)
}

const THANK_YOU_QUERY = gql`
	query ThankYouQuery($id: ID!) {
		Post(id: $id) {
			id
			intro
			text
			closing
			image
			video
			name
			description
			createdAt
			author {
				id
				name
				picture
			}
			recipient {
				id
				name
			}
			customDesign {
				id
				image
			}
		}
	}
`

export default compose(
	graphql(THANK_YOU_QUERY, {
		props: ({ data: { Post, loading } }) => ({
			post: Post,
			loading
		}),
		options: ({ match }) => ({
			variables: { id: match.params.postID }
		})
	})
)(ThankYou)

import styled from 'styled-components'
import media from 'styled-media-query'

export const FooterForm = styled.form`
	display: flex;
	flex-direction: column;
	align-items: flex-end;
`

export const FooterFormInputEmail = styled.input`
	box-sizing: border-box;
	margin-bottom: 20px;
	width: 100%;
	padding: 18px 12px 16px;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 300;
	outline: none;
	border: 0;
	border-bottom: 2px solid transparent;
	transition: all 0.2s ease;
	&:hover,
	&:focus {
		background: #f8fbfe;
		border-bottom: 2px solid #ff7675;
	}

	${media.lessThan('medium')`
		margin-bottom: 10px;
	`};
`
export const FooterFormInputMessage = styled.input`
	box-sizing: border-box;
	margin-bottom: 20px;
	min-width: 100%;
	padding: 18px 12px 16px;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 300;
	outline: none;
	border: 0;
	border-bottom: 2px solid transparent;
	transition: all 0.2s ease;
	&:hover,
	&:focus {
		background: #f8fbfe;
		border-bottom: 2px solid #ff7675;
	}
	${media.lessThan('medium')`
		margin-bottom: 10px;
	`};
`

export const FooterSubmitButton = styled.input.attrs({
	type: 'submit'
})`
	background: #dbeef7;
	text-decoration: none;
	border: 1px solid #dbeef7;
	color: #184e68;
	border-radius: 50px;
	padding: 14px 20px 12px;
	width: 100px;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	font-size: 16px;
	transition: all 0.2s ease;
	cursor: pointer;
	&:hover {
		background: transparent;
		color: #dbeef7;
	}
`

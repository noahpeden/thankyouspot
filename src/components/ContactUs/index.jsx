import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

import {
	FooterForm,
	FooterFormInputEmail,
	FooterFormInputMessage,
	FooterSubmitButton
} from './styles'

class ContactUs extends Component {
	state = {
		contactUsEmail: '',
		contactUsMessage: ''
	}

	render() {
		const { contactUsEmail, contactUsMessage } = this.state

		return (
			<FooterForm onSubmit={this.sendContactUsEmail}>
				<FooterFormInputEmail
					placeholder="Email"
					onChange={this.setContactUsEmail}
					value={contactUsEmail}
				/>
				<FooterFormInputMessage
					placeholder="Message"
					onChange={this.setContactUsMessage}
					value={contactUsMessage}
				/>
				<FooterSubmitButton value="Send" onClick={this.sendContactUsEmail} />
			</FooterForm>
		)
	}

	setContactUsEmail = evt => {
		this.setState({ contactUsEmail: evt.target.value })
	}

	setContactUsMessage = evt => {
		this.setState({ contactUsMessage: evt.target.value })
	}

	sendContactUsEmail = async evt => {
		evt.preventDefault()

		const variables = {
			email: this.state.contactUsEmail,
			message: this.state.contactUsMessage
		}

		try {
			this.resetForm()
			await this.props.contactUs({ variables })
		} catch (err) {
			console.log(err.message)
		}
	}

	resetForm = () => {
		this.setState({ contactUsEmail: '', contactUsMessage: '' })
	}
}

const CONTACT_US_MUTATION = gql`
	mutation SendContactUsEmail($email: String!, $message: String!) {
		contactUs(email: $email, message: $message) {
			success
		}
	}
`

export default graphql(CONTACT_US_MUTATION, {
	props: ({ mutate }) => ({
		contactUs: options => {
			return mutate(options)
		}
	})
})(ContactUs)

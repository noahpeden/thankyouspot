import React, { Component } from 'react'
import { compose, graphql } from 'react-apollo'
import gql from 'graphql-tag'
import { DesignsGrid, DesignCard, DesignWrapper, DesignTitle } from './styles'

class CustomDesigns extends Component {
	render() {
		const { loading, customDesigns } = this.props

		if (loading) return null

		return (
			<DesignsGrid>
				{customDesigns.map(customDesign => (
					<DesignCard>
						<DesignWrapper
							src={`https://s3.amazonaws.com/www.thankyouspot.com/custom/${
								customDesign.image
							}`}
						/>
						<DesignTitle>{customDesign.name}</DesignTitle>
					</DesignCard>
				))}
			</DesignsGrid>
		)
	}
}

const CustomDesignsQuery = gql`
	query CustomDesigns {
		allCustomDesigns(filter: { isPublished: true }) {
			id
			name
			image
		}
	}
`

export default compose(
	graphql(CustomDesignsQuery, {
		props: ({ data: { allCustomDesigns, loading } }) => ({
			customDesigns: allCustomDesigns,
			loading
		})
	})
)(CustomDesigns)

import styled, { css } from 'styled-components'
import media from 'styled-media-query'

export const DesignsGrid = styled.div`
	display: grid;
	grid-gap: 30px;
	grid-template-columns: 232px 232px 232px;
	grid-template-rows: 390px 390px;
	justify-content: center;
	margin-bottom: 70px;
	${media.lessThan('medium')`
		grid-template-columns: 232px;
		padding: 40px 0 0;
    `};
	${props =>
		props.subcategory &&
		css`
			grid-template-rows: 390px;
		`};
`

export const DesignCard = styled.div`
	background: #fefefe;
	width: 100%;
	height: 100%;
`
export const DesignWrapper = styled.img`
	display: block;
	background: #ff7573;
	width: 100%;
	height: 350px;
	background-size: cover;
`
export const DesignTitle = styled.p`
	color: #184e68;
	font-size: 20px;
	font-family: 'Josefin Sans', sans-serif;
`

import gql from 'graphql-tag'
import React from 'react'
import { compose, graphql } from 'react-apollo'
import ThankYouCard from '../ThankYouCard'
import {
	BodyWrapper,
	ButtonContainer,
	SeeAllButton,
	ThankYouList
} from './styles'

const ThankYouCards = ({ allPosts, url }) => {
	return (
		<BodyWrapper>
			<ThankYouList>
				{allPosts &&
					allPosts.map(post => (
						<ThankYouCard key={post.id} post={post} prefixUrl={url} />
					))}
			</ThankYouList>
			<ButtonContainer>
				<SeeAllButton to="/global-thank-yous">See More</SeeAllButton>
			</ButtonContainer>
		</BodyWrapper>
	)
}

const ALL_POSTS_QUERY = gql`
	query allPosts($first: Int!, $archived: Boolean) {
		allPosts(
			first: $first
			orderBy: createdAt_DESC
			filter: { archived: $archived }
		) {
			id
			intro
			text
			closing
			image
			video
			name
			archived
			description
			createdAt
			address {
				name
			}
			author {
				id
				name
				picture
			}
			recipient {
				id
				name
			}
			customDesign {
				id
				image
			}
		}
	}
`

const MAX_NUM_OF_CARDS = 6

export default compose(
	graphql(ALL_POSTS_QUERY, {
		props: ({ data: { allPosts } }) => ({
			allPosts
		}),
		options: {
			variables: {
				first: MAX_NUM_OF_CARDS,
				archived: false
			}
		}
	})
)(ThankYouCards)

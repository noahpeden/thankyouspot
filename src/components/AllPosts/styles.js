import styled from 'styled-components'
import { Link } from 'react-router-dom'
import media from 'styled-media-query'

export const BodyWrapper = styled.div`
	background: linear-gradient(to right, #fde5a7, #ff7676);
	grid-column: 2 / 3;
	grid-row: 2 / 3;
`
export const ThankYouList = styled.div`
	display: grid;
	grid-gap: 30px;
	grid-template-columns: 232px 232px 232px;
	padding: 60px 0 0;
	justify-content: center;
	${media.lessThan('medium')`
		grid-template-columns: 232px;
		padding: 40px 0 0;
	`};
`
export const ButtonContainer = styled.div`
	display: flex;
	justify-content: center;
	padding-bottom: 6%;
`
export const SeeAllButton = styled(Link)`
	background: #184e68;
	text-decoration: none;
	border: 1px solid #184e68;
	color: white;
	border-radius: 50px;
	padding: 14px 30px 12px;
	cursor: pointer;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	font-size: 18px;
	transition: all 0.2s ease;

	&:focus,
	&:hover,
	&:visited,
	&:link,
	&:active {
		text-decoration: none;
	}

	&:hover {
		color: #184e68;
		background-color: transparent;
		border: solid 1px #184e68;
	}
`

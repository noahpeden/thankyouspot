import React from 'react'
import { Helmet } from 'react-helmet'
import Header from '../components/Header'
import Footer from '../components/Footer'
import styled from 'styled-components'
import media from 'styled-media-query'
import { Link } from 'react-router-dom'

const HowItWorksPage = () => {
	return (
		<div>
			<Helmet>
				<title>How it Works | Thank You Spot</title>
				<meta
					name="description"
					content="Learn how Thank You Spot can help you spread global gratitude in style. This is how our website enables you to create and share beautiful thank you's with the world."
				/>
			</Helmet>
			<Header />
			<Wrapper>
				<Hero>
					<Graphic
						src={require('../assets/images/st-how.gif')}
						alt="Feature"
					/>
					<HeroTitle>How It Works</HeroTitle>
				</Hero>
			</Wrapper>
			<Features>
				<Row1>
					<FeatureImage
						src={require('../assets/images/loop1.gif')}
						alt="Phone in hand collage"
					/>
					<FeatureWrapper>
						<FeatureHeader>01.</FeatureHeader>
						<FeatureSubheader>
							Choose a thank you type.
						</FeatureSubheader>
						<FeatureCaption>
							Enter the information of someone you know or pick a
							spot on the map to thank a stranger.
						</FeatureCaption>
					</FeatureWrapper>
				</Row1>
				<Row2>
					<FeatureImage
						src={require('../assets/images/loop2.gif')}
						alt="Letter in envelope collage"
					/>
					<FeatureWrapper>
						<FeatureHeader>02.</FeatureHeader>
						<FeatureSubheader>
							Compose your thank you.
						</FeatureSubheader>
						<FeatureCaption>
							Write a meaningful message and sweeten it with
							features like custom designs, a video thank you, or
							a postcard.
						</FeatureCaption>
					</FeatureWrapper>
				</Row2>
				<Row1>
					<FeatureImage
						src={require('../assets/images/loop3.gif')}
						alt="World collage"
					/>
					<FeatureWrapper>
						<FeatureHeader>03.</FeatureHeader>
						<FeatureSubheader>
							Send and share your thank you.
						</FeatureSubheader>
						<FeatureCaption>
							Share your gratitude with the world and the
							recipient on the Global Thank You’s feed and social
							media.
						</FeatureCaption>
					</FeatureWrapper>
				</Row1>
			</Features>
			<ButtonContainer>
				<SeeMoreButton to="/thank-someone">Start Thanking ></SeeMoreButton>
			</ButtonContainer>
			<Footer />
		</div>
	)
}
const Wrapper = styled.div`
	display: grid;
	grid-template-rows: 320px auto;
	${media.between('medium', 'large')`
		grid-template-rows: 370px auto;
	`};
	${media.lessThan('medium')`
		grid-template-rows: none;
	`};
`
const Graphic = styled.img`
	${media.between('medium', 'large')`
		margin-top: 50px;
	`};
`

const Hero = styled.div`
	grid-column: 1 / 4;
	display: flex;
	align-items: center;
	justify-content: center;
	background-color: #dbeef7;
	flex-direction: column;
`

const HeroTitle = styled.h1`
	color: #184e68;
	font-size: 50px;
	text-shadow: 0 8px 0 #e5cad0;
	text-align: center;
	font-family: 'Paytone One', sans-serif;
	-webkit-font-smoothing: antialiased;
	letter-spacing: 1px;
	${media.lessThan('medium')`
		font-size: 35px;
	`};
`
const Features = styled.div`
	display: grid;
	width: 100%;
	text-align: left;
	padding: 5% 25%;
	background: white;
	grid-row-gap: 100px;
	${media.between('medium', 'large')`
		padding: 5% 8%;
		grid-row-gap: 50px;
	`};
	${media.lessThan('medium')`
		padding: 5% 5%;
		text-align: center;
		grid-row-gap: 20px;

		img {
			max-width: 200px;
		}
		h4 {
			font-size: 20px;
		}
	`};
`
const Row1 = styled.div`
	display: flex;
	flex: 1;
	flex-direction: row;
	justify-content: space-around;

	${media.lessThan('medium')`
		flex-direction: column;
	`};
`
const Row2 = styled.div`
	display: flex;
	flex: 1;
	flex-direction: row-reverse;
	justify-content: space-around;

	${media.lessThan('medium')`
		flex-direction: column;
	`};
`
const FeatureImage = styled.img`
	margin: 0 auto;
	padding: 15px;
	${media.lessThan('medium')`
		max-width: 200px;
	`};
`
const FeatureWrapper = styled.div`
	padding: 15px;
`
const FeatureHeader = styled.h2`
	font-size: 50px;
	color: #184e68;
	margin: 0;
	text-shadow: 0 6px #dbeef7;
	font-family: 'Paytone One', sans-serif;
	-webkit-font-smoothing: antialiased;
	letter-spacing: 1px;
`
const FeatureSubheader = styled.h4`
	font-size: 1.5rem;
	line-height: 1.5;
	color: #ff7676;
	margin: 10px 0;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 600;
	-webkit-font-smoothing: antialiased;
	${media.lessThan('medium')`
		font-size: 20px;
	`};
`
const FeatureCaption = styled.p`
	font-size: 18px;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 300;
	-webkit-font-smoothing: antialiased;
	color: #3a3d3f;
	line-height: 1.5;
`
const ButtonContainer = styled.div`
	display: flex;
	justify-content: center;
`
const SeeMoreButton = styled(Link)`
	width: 100%;
	background: #ff7676;
	text-decoration: none;
	color: white;
	padding: 30px;
	cursor: pointer;
	font-family: 'Paytone One', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	font-size: 28px;
	text-transform: uppercase;
	text-align: center;
	letter-spacing: 2px;
	transition: 0.2s ease;
	&:hover {
		background: #fde5a7;
		color: #ff7676;
		cursor: pointer;
	}
	${media.lessThan('medium')`
		font-size: 24px;
	`};
`
export default HowItWorksPage

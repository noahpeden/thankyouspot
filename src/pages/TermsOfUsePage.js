import React, { Component } from 'react'
import { Helmet } from 'react-helmet'
import Header from '../components/Header'
import Footer from '../components/Footer'
import TermsOfUse from '../components/Static/TermsOfUse'

class TermsOfUsePage extends Component {
	render() {
		return (
			<React.Fragment>
				<Helmet>
					<title>Terms of Use | Thank You Spot</title>
					<meta name="description" content="Thank You Spot Terms Of Use" />
				</Helmet>
				<Header />
				<TermsOfUse />
				<Footer />
			</React.Fragment>
		)
	}
}

export default TermsOfUsePage

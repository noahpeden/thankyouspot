import React, { Component } from 'react'
import { Helmet } from 'react-helmet'

import Header from '../components/Header'
import Footer from '../components/Footer'
import Thanking from '../components/Thanking'

class ThankPage extends Component {
	render() {
		const { match, location } = this.props

		return (
			<React.Fragment>
				<Helmet>
					<title>Thank Someone | Thank You Spot</title>
					<meta
						name="description"
						content="Don't wait—thank someone today! Whether it's a stranger at the coffee shop or your best friend, use Thank You Spot to show your gratitude."
					/>
				</Helmet>
				<Header />
				<Thanking match={match} location={location} />
				<Footer />
			</React.Fragment>
		)
	}
}

export default ThankPage

import React, { Component } from 'react'

import Header from '../components/Header'
import Footer from '../components/Footer'
import PasswordReset from '../components/Auth/PasswordReset'

class PasswordResetPage extends Component {
	render() {
		const { match, location, history } = this.props

		return (
			<React.Fragment>
				<Header />
				<PasswordReset match={match} history={history} location={location} />
				<Footer />
			</React.Fragment>
		)
	}
}

export default PasswordResetPage

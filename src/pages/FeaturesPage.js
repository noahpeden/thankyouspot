import React from 'react'
import { Helmet } from 'react-helmet'
import Header from '../components/Header'
import Footer from '../components/Footer'
import styled from 'styled-components'
import media from 'styled-media-query'
import { Link } from 'react-router-dom'

const FeaturesPage = () => {
	return (
		<div>
			<Helmet>
				<title>Features | Thank You Spot</title>
				<meta
					name="description"
					content="Discover all the ways that Thank You Spot can help you make your thank you's extra special. From custom designs and video thank you's to special add-on's, we have it all!"
				/>
			</Helmet>
			<Header />
			<Wrapper>
				<Hero>
					<Graphic
						src={require('../assets/images/st-feature.gif')}
						alt="Feature"
					/>
					<HeroTitle>Features</HeroTitle>
					<HeroCaption>
						With Thank You Spot you gain a variety of ways to make sure your
						thank you’s are meaningful, memorable, and unforgettable.
					</HeroCaption>
				</Hero>
			</Wrapper>
			<Features1>
				<FeatureWrapper>
					<Icon
						src={require('../assets/images/feature1.gif')}
						alt="Custom Card"
					/>
					<FeatureHeader>CUSTOM DESIGNS</FeatureHeader>
					<FeatureCaption>
						Choose from our gallery of beautiful thank you cards to add
						something extra special to your thank you.
					</FeatureCaption>
				</FeatureWrapper>
				<FeatureWrapper>
					<Icon src={require('../assets/images/feature2.gif')} alt="Camera" />
					<FeatureHeader>ADD A PHOTO</FeatureHeader>
					<FeatureCaption>
						Add your own images to customize your thank you for something extra
						meaningful.
					</FeatureCaption>
				</FeatureWrapper>
				<FeatureWrapper>
					<Icon
						src={require('../assets/images/feature3.gif')}
						alt="Video Thank You's"
					/>
					<FeatureHeader>VIDEO THANK YOU'S</FeatureHeader>
					<FeatureCaption>
						Don’t just type it, say it! Record or upload a video thank you to
						ensure you get your meaning across.
					</FeatureCaption>
				</FeatureWrapper>
				<FeatureWrapper>
					<Icon
						src={require('../assets/images/feature4.gif')}
						alt="Video Camera"
					/>
					<FeatureHeader>MAIL YOUR THANK YOU</FeatureHeader>
					<FeatureCaption>
						Have your thank you printed and mailed as a postcard—snail mail is
						still real and is great for showing gratitude!
					</FeatureCaption>
				</FeatureWrapper>
			</Features1>
			<ButtonContainer>
				<SeeMoreButton to="/thank-someone">Start Thanking ></SeeMoreButton>
			</ButtonContainer>
			<Footer />
		</div>
	)
}
const Wrapper = styled.div`
	display: grid;
	grid-template-rows: 440px auto;
	${media.between('medium', 'large')`
		grid-template-rows: 500px auto;
	`};
	${media.lessThan('medium')`
		grid-template-rows: none;
	`};
`
const Graphic = styled.img`
	${media.between('medium', 'large')`
		margin-top: 50px;
	`};
`

const Hero = styled.div`
	grid-column: 1 / 4;
	grid-row: 1 / 2;
	display: flex;
	align-items: center;
	justify-content: center;
	background-color: #dbeef7;
	flex-direction: column;
	padding: 20px 20%;

	${media.lessThan('medium')`
		padding: 20px;
	`};
`
const HeroTitle = styled.h1`
	color: #184e68;
	font-size: 50px;
	text-shadow: 0 8px 0 #e5cad0;
	text-align: center;
	margin: 0;
	padding-bottom: 10px;
	font-family: 'Paytone One', sans-serif;
	-webkit-font-smoothing: antialiased;
	letter-spacing: 1px;

	${media.lessThan('medium')`
		font-size: 35px;
	`};
`
const HeroCaption = styled.p`
	color: #184e68;
	text-align: center;
	font-size: 20px;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 400;
	-webkit-font-smoothing: antialiased;
	line-height: 1.5;

	${media.lessThan('medium')`
		font-size: 16px;
		font-weight: 300;
	`};
`
const Features1 = styled.div`
	display: grid;
	width: 100%;
	text-align: center;
	padding: 4% 10% 4%;
	background: white;
	grid-template-columns: auto auto auto auto;
	grid-column-gap: 50px;
	${media.between('medium', 'large')`
		grid-template-columns: auto auto;
	`};
	${media.lessThan('medium')`
		grid-template-columns: auto;
		padding: 8% 10% 0;
	`};
`
const FeatureWrapper = styled.div``
const Icon = styled.img`
	padding-bottom: 10px;
	max-height: 120px;
	${media.lessThan('medium')`
		padding: 20px;
		max-height: 100px;
	`};
`
const FeatureHeader = styled.h2`
	font-size: 18px;
	color: #184e68;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 700;
	-webkit-font-smoothing: antialiased;
`
const FeatureCaption = styled.p`
	font-size: 18px;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 300;
	-webkit-font-smoothing: antialiased;
	color: #3a3d3f;
	line-height: 1.5;
	${media.lessThan('medium')`
		padding-bottom: 40px;
			font-size: 16px;
	`};
`
const ButtonContainer = styled.div`
	display: flex;
	justify-content: center;
`
const SeeMoreButton = styled(Link)`
	width: 100%;
	background: #ff7676;
	text-decoration: none;
	color: white;
	padding: 30px;
	cursor: pointer;
	font-family: 'Paytone One', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-size: 28px;
	font-weight: 700;
	text-transform: uppercase;
	text-align: center;
	letter-spacing: 2px;
	transition: 0.2s ease;
	&:hover {
		background: #fde5a7;
		color: #ff7676;
		cursor: pointer;
	}
	${media.lessThan('medium')`
		font-size: 24px;
	`};
`
export default FeaturesPage

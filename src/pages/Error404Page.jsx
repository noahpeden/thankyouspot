import React from 'react'
import styled from 'styled-components'
import { Helmet } from 'react-helmet'
import Header from '../components/Header'
import Footer from '../components/Footer'
import NotFoundImage from '../assets/images/404.gif'
import media from 'styled-media-query'
import { Link } from 'react-router-dom'

const Error404Page = () => {
	return (
		<React.Fragment>
			<Helmet>
				<title>This Page Does Not Exist | Thank You Spot</title>
				<meta
					name="description"
					content="Oh no! We're not sure what happened, but this page does not exist. Please go to the homepage and start over."
				/>
			</Helmet>
			<Header />
			<NotFoundWrapper>
				<Image src={NotFoundImage} alt="" />
				<HeaderText>
					Sorry. We couldn't find the page you were looking for.
				</HeaderText>
				<HomeButton to="/">Home</HomeButton>
			</NotFoundWrapper>
			<Footer />
		</React.Fragment>
	)
}

const NotFoundWrapper = styled.div`
	display: flex;
	flex-direction: column;
	height: auto;
	width: 100%;
	background: #f5f8fa;
	padding: 60px;
	${media.lessThan('medium')`
		padding: 40px 30px;
	`};
`

const HeaderText = styled.h2`
	display: block;
	margin: 0 auto;
	font-size: 42px;
	color: #184e68;
	text-align: center;
	font-family: 'Paytone One', sans-serif;

	${media.lessThan('medium')`
		font-size: 32px;
		margin-top: 20px;
	`};
`

const Image = styled.img`
	display: block;
	margin: auto;
	width: 100%;
	max-width: 900px;
`
const HomeButton = styled(Link)`
	display: block;
	margin: 30px auto 0;
	background: #ff7676;
	border: 2px solid #ff7676;
	color: white;
	border-radius: 50px;
	padding: 17px 35px 15px;
	text-decoration: none;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-size: 20px;
	font-weight: 700;
	align-content: center;
	transition: all 0.2s ease;
	text-align: center;
	&:hover {
		background: transparent;
		color: #ff7676;
		cursor: pointer;
	}
`

export default Error404Page

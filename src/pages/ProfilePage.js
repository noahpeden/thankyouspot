import React, { Component } from 'react'
import { Helmet } from 'react-helmet'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Profile from '../components/Profile'

class ProfilePage extends Component {
	render() {
		const { match } = this.props

		return (
			<React.Fragment>
				<Helmet>
					<title>Profile | Thank You Spot</title>
					<meta
						name="description"
						content="See all of the thank you's that you've sent and recevied on your very own Thank You Spot profile."
					/>
				</Helmet>
				<Header />
				<Profile id={match.params.userID} />
				<Footer />
			</React.Fragment>
		)
	}
}

export default ProfilePage

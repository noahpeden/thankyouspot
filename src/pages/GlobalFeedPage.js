import React, { Component } from 'react'
import { Helmet } from 'react-helmet'
import styled from 'styled-components'
import Header from '../components/Header'
import Footer from '../components/Footer'
import GlobalThankYous from '../components/GlobalThankYous'
import media from 'styled-media-query'
import debounce from 'lodash/debounce'

class GlobalFeedPage extends Component {
	state = {
		filter: ''
	}

	render() {
		const { match } = this.props

		return (
			<React.Fragment>
				<Helmet>
					<title>Global Thank You Feed | Thank You Spot</title>
					<meta
						name="description"
						content="Browse the latest thank you’s posted around the world in the name of gratitude. View, share, and gain inspiration for your own thank you’s!"
					/>
				</Helmet>
				<Header />
				<Wrapper>
					<Hero>
						<Graphic
							src={require('../assets/images/st-global.gif')}
							alt="Feature"
						/>
						<HeroTitle>Global Thank You's</HeroTitle>
						<HeroCaption>
							Browse the latest thank you’s posted around the world in the name
							of gratitude. View, share, and gain inspiration for your own thank
							you’s.
						</HeroCaption>
					</Hero>
					<GlobalThankYous
						url={match.url}
						query={this.state.filter}
						setFilter={this.setFilter}
					/>
				</Wrapper>
				<Footer />
			</React.Fragment>
		)
	}

	setFilter = debounce(evt => {
		this.setState({ filter: evt.target.value })
	}, 500)
}

const Wrapper = styled.div`
	display: grid;
	grid-template-columns: auto 1020px auto;
	grid-template-rows: 440px auto;
	${media.between('medium', 'large')`
		grid-template-columns: auto auto;
		grid-template-rows: 500px auto;
	`};
	${media.lessThan('medium')`
		grid-template-columns: auto auto;
		grid-template-rows: none;
	`};
`
const Graphic = styled.img`
	${media.between('medium', 'large')`
		margin-top: 50px;
	`};
`
const Hero = styled.div`
	grid-column: 1 / 4;
	grid-row: 1 / 2;
	display: flex;
	flex-direction: column;
	padding: 20px 20%;
	align-items: center;
	justify-content: center;
	background-color: #dbeef7;
	${media.between('medium', 'large')`
		padding: 20px;
	`};
	${media.lessThan('medium')`
		padding: 20px;
	`};
`

const HeroTitle = styled.h1`
	color: #184e68;
	font-size: 50px;
	text-shadow: 0 8px 0 #e5cad0;
	margin: 0;
	padding-bottom: 10px;
	font-family: 'Paytone One', sans-serif;
	-webkit-font-smoothing: antialiased;
	letter-spacing: 1px;
	${media.lessThan('medium')`
		font-size: 35px;
		text-align: center;
	`};
`
const HeroCaption = styled.p`
	color: #184e68;
	text-align: center;
	font-size: 20px;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 400;
	-webkit-font-smoothing: antialiased;
	line-height: 1.5;

	${media.lessThan('medium')`
		font-size: 16px;
		font-weight: 300;
	`};
`

export default GlobalFeedPage

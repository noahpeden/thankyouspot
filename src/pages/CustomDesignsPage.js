import React from 'react'
import { Helmet } from 'react-helmet'
import Header from '../components/Header'
import Footer from '../components/Footer'
import styled from 'styled-components'
import media from 'styled-media-query'
import { Link } from 'react-router-dom'
import CustomDesigns from '../components/CustomDesigns'

const CustomDesignsPage = () => {
	return (
		<div>
			<Helmet>
				<title>Custom Designs | Thank You Spot</title>
				<meta
					name="description"
					content="Our team routinely adds unique, beautiful, and custom designs to help make sure your thank you’s are meaningful, memorable, and unforgettable."
				/>
			</Helmet>
			<Header />
			<Wrapper>
				<Hero>
					<Graphic
						src={require('../assets/images/st-custom-designs.gif')}
						alt="Feature"
					/>
					<HeroTitle>Custom Designs</HeroTitle>
					<HeroCaption>
						Our team routinely adds unique, beautiful, and custom designs to
						help make sure your thank you’s are meaningful, memorable, and
						unforgettable.
					</HeroCaption>
				</Hero>
			</Wrapper>
			<CustomDesignsWrapper>
				<DesignCategory>General</DesignCategory>
				<CustomDesigns />
				<StartThankButton to="/thank-someone">Start Thanking</StartThankButton>
			</CustomDesignsWrapper>
			<Footer />
		</div>
	)
}

const Wrapper = styled.div`
	display: grid;
	grid-template-rows: 440px auto;
	${media.between('medium', 'large')`
		grid-template-rows: 500px auto;
	`};
	${media.lessThan('medium')`
		grid-template-rows: none;
	`};
`
const Graphic = styled.img`
	${media.between('medium', 'large')`
		margin-top: 50px;
	`};
`

const Hero = styled.div`
	grid-column: 1 / 4;
	grid-row: 1 / 2;
	display: flex;
	align-items: center;
	justify-content: center;
	background-color: #dbeef7;
	flex-direction: column;
	padding: 20px 20%;

	${media.lessThan('medium')`
		padding: 20px;
	`};
`
const HeroTitle = styled.h1`
	color: #184e68;
	font-size: 50px;
	text-shadow: 0 8px 0 #e5cad0;
	text-align: center;
	margin: 0;
	padding-bottom: 10px;
	font-family: 'Paytone One', sans-serif;
	-webkit-font-smoothing: antialiased;
	letter-spacing: 1px;

	${media.lessThan('medium')`
		font-size: 35px;
	`};
`
const HeroCaption = styled.p`
	color: #184e68;
	text-align: center;
	font-size: 20px;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 400;
	-webkit-font-smoothing: antialiased;
	line-height: 1.5;

	${media.lessThan('medium')`
		font-size: 16px;
		font-weight: 300;
	`};
`

const CustomDesignsWrapper = styled.div`
	display: block;
	width: 100%;
	max-width: 756px;
	margin: auto;
	padding: 60px 0;
`

const DesignCategory = styled.h2`
	color: #184e68;
	font-size: 28px;
	font-weight: bold;
	font-family: 'Josefin Sans', sans-serif;
	${media.lessThan('medium')`
		text-align: center;
	`};
`

export const StartThankButton = styled(Link)`
	display: block;
	width: 240px;
	border: 2px solid #ff7676;
	box-shadow: 0 12px 43px -14px #ff7676;
	background: #ff7676;
	text-decoration: none;
	color: white;
	border-radius: 50px;
	padding: 17px 30px 15px;
	cursor: pointer;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	font-size: 20px;
	text-align: center;
	transition: all 0.2s ease;
	margin: auto;
	&:hover {
		background: transparent;
		color: #ff7676;
		box-shadow: none;
	}
	${media.lessThan('medium')`
		font-size: 18px;
	`};
`

export default CustomDesignsPage

import React, { Component } from 'react'

import Header from '../components/Header'
import Footer from '../components/Footer'
import RequestPasswordReset from '../components/Auth/RequestPasswordReset'

class RequestPasswordResetPage extends Component {
	render() {
		const { match, location } = this.props

		return (
			<React.Fragment>
				<Header />
				<RequestPasswordReset match={match} location={location} />
				<Footer />
			</React.Fragment>
		)
	}
}

export default RequestPasswordResetPage

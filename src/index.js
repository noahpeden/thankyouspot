import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import {
	BrowserRouter as Router,
	Route,
	Switch,
	withRouter
} from 'react-router-dom'
import { ApolloProvider } from 'react-apollo'
import { ApolloClient } from 'apollo-client'
import { ApolloLink } from 'apollo-link'
import { HttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import { withClientState } from 'apollo-link-state'
import { InMemoryCache } from 'apollo-cache-inmemory'
import 'tachyons'
import './index.css'
import './swalstyles'

import ThankPage from './pages/ThankPage'
import ProfilePage from './pages/ProfilePage'
import GlobalFeedPage from './pages/GlobalFeedPage'
import Error404Page from './pages/Error404Page'
import FeaturesPage from './pages/FeaturesPage'
import CustomDesignsPage from './pages/CustomDesignsPage'
import HowItWorksPage from './pages/HowItWorksPage'
import TermsOfUsePage from './pages/TermsOfUsePage'
import RequestPasswordResetPage from './pages/RequestPasswordResetPage'
import PasswordResetPage from './pages/PasswordResetPage'

import ThankYou from './components/ThankYou'
import Login from './components/Auth/Login'
import Signup from './components/Auth/Signup'
import LazyLoadProvider from './components/LazyLoad'
import CookieBanner from './components/CookieBanner'

const { REACT_APP_GRAPHQL_URL } = process.env

// __SIMPLE_API_ENDPOINT__ looks like: 'https://api.graph.cool/simple/v1/__SERVICE_ID__'
const httpLink = new HttpLink({
	uri: REACT_APP_GRAPHQL_URL
})

const authLink = setContext((_, { headers }) => {
	const token = localStorage.getItem('graphcoolToken')

	return {
		headers: {
			...headers,
			authorization: token ? `Bearer ${token}` : ''
		}
	}
})

const cache = new InMemoryCache()

const stateLink = withClientState({
	cache,
	defaults: {
		authInfo: {
			__typename: 'AuthInfo',
			userID: localStorage.getItem('tys-userid'),
			token: localStorage.getItem('graphcoolToken')
		}
	},
	resolvers: {
		Mutation: {
			updateAuthInfo: (_, { userID, token }, { cache }) => {
				if (token) {
					localStorage.setItem('graphcoolToken', token)
				} else {
					localStorage.removeItem('graphcoolToken')
				}

				if (userID) {
					localStorage.setItem('tys-userid', userID)
				} else {
					localStorage.removeItem('tys-userid')
				}

				const data = {
					authInfo: {
						__typename: 'AuthInfo',
						userID,
						token
					}
				}
				cache.writeData({ data })

				return null
			}
		}
	}
})

const client = new ApolloClient({
	cache,
	link: ApolloLink.from([stateLink, authLink, httpLink])
})

const ScrollToTop = withRouter(
	class extends React.Component {
		componentDidUpdate(prevProps) {
			const { location, isModal, retainScroll } = this.props
			if (location !== prevProps.location && !isModal && !retainScroll) {
				window.scrollTo(0, 0)
			}
		}

		render() {
			return this.props.children
		}
	}
)

class AppShell extends React.Component {
	constructor(props) {
		super(props)

		this.previousLocation = props.location
	}

	componentWillUpdate(nextProps) {
		const { location } = this.props

		if (
			nextProps.history.action !== 'POP' &&
			(!location.state || !location.state.modal)
		) {
			this.previousLocation = this.props.location
		}
	}

	render() {
		const { location } = this.props
		const isModal = location.state && location.state.modal
		const retainScroll = location.state && location.state.retainScroll

		return (
			<ScrollToTop isModal={isModal} retainScroll={retainScroll}>
				<Switch location={isModal ? this.previousLocation : location}>
					<Route exact path="/" component={App} />
					<Route exact path="/auth/login" component={App} />
					<Route exact path="/auth/signup" component={App} />
					<Route
						exact
						path="/auth/request-password-reset"
						component={RequestPasswordResetPage}
					/>
					<Route
						exact
						path="/auth/password-reset"
						component={PasswordResetPage}
					/>
					<Route path="/thank-someone" component={ThankPage} />
					<Route path="/features" component={FeaturesPage} />
					<Route path="/custom-designs" component={CustomDesignsPage} />
					<Route path="/how-it-works" component={HowItWorksPage} />
					<Route path="/terms" component={TermsOfUsePage} />
					<Route path="/global-thank-yous" component={GlobalFeedPage} />
					<Route path="/:userID" component={ProfilePage} />
					<Route path="/:userID/sent" exact component={ProfilePage} />
					<Route path="/:userID/edit" exact component={ProfilePage} />
				</Switch>
				<Switch>
					<Route
						path="/auth/login"
						render={routeProps => (
							<Login
								{...routeProps}
								parentPath={this.previousLocation.pathname}
							/>
						)}
					/>
					<Route
						path="/auth/signup"
						render={routeProps => (
							<Signup
								{...routeProps}
								parentPath={this.previousLocation.pathname}
							/>
						)}
					/>
					<Route
						exact
						path="/global-thank-yous/:postID"
						render={routeProps => (
							<ThankYou
								{...routeProps}
								parentPath={this.previousLocation.pathname}
							/>
						)}
					/>
					<Route
						exact
						path="/:userID/thank-yous/:postID"
						render={routeProps => (
							<ThankYou
								{...routeProps}
								parentPath={this.previousLocation.pathname}
							/>
						)}
					/>
				</Switch>
				<CookieBanner />
			</ScrollToTop>
		)
	}
}

const CaptureRouteNotFound = withRouter(({ children, location }) => {
	return location && location.state && location.state.notFoundError ? (
		<Route component={Error404Page} status={404} />
	) : (
		children
	)
})

ReactDOM.render(
	<ApolloProvider client={client}>
		<Router>
			<CaptureRouteNotFound>
				<Route
					component={props => (
						<LazyLoadProvider>
							<AppShell {...props} />
						</LazyLoadProvider>
					)}
				/>
			</CaptureRouteNotFound>
		</Router>
	</ApolloProvider>,
	document.getElementById('root')
)

import React, { Component } from 'react'
import { Helmet } from 'react-helmet'
import Header from './components/Header'
import HowItWorks from './components/HowItWorks'
import Footer from './components/Footer'
import ThankYouCards from './components/AllPosts'
import {
	ButtonTY,
	ButtonPublic,
	HeroSection,
	HeroLeft,
	HeroRight,
	CTAContainer,
	Welcome,
	WelcomeSub,
	SpecialFeature,
	Feature,
	FeatureTitle,
	FeatureTitleHeader,
	Featurette,
	SpecialIcon,
	FeatureSubheader
} from './AppStyles'

class App extends Component {
	render() {
		return (
			<React.Fragment>
				<Helmet>
					<title>Thank Anyone, Anywhere | Thank You Spot</title>
					<meta
						name="description"
						content="Use Thank You Spot to thank anyone, anywhere! Spread global gratitude in three easy steps—choose a thank you type, compose your thank you, send and share."
					/>
				</Helmet>
				<Header />
				<HeroSection>
					<HeroLeft>
						<CTAContainer>
							<Welcome>Welcome to Thank You Spot!</Welcome>
							<WelcomeSub>
								Thank anyone, anywhere, anytime. Pay it forward now.
							</WelcomeSub>
							<ButtonTY to="/thank-someone">Thank Someone</ButtonTY>
							<ButtonPublic to="/global-thank-yous" className="public-ty">
								Global Gratitude
							</ButtonPublic>
						</CTAContainer>
					</HeroLeft>
					<HeroRight>
						<img
							className="hero-image"
							src={require('./assets/images/Graphic.png')}
							alt="Hero"
						/>
					</HeroRight>
				</HeroSection>
				<HowItWorks />
				<section className="public-feed">
					<ThankYouCards />
				</section>
				<SpecialFeature>
					<FeatureTitle>
						<FeatureTitleHeader>Special Features</FeatureTitleHeader>
					</FeatureTitle>
					<Feature>
						<Featurette>
							<SpecialIcon
								src={require('./assets/images/feature1.gif')}
								alt="custom card"
							/>
							<FeatureSubheader>Custom Design</FeatureSubheader>
						</Featurette>

						<Featurette>
							<SpecialIcon
								src={require('./assets/images/feature2.gif')}
								alt="camera"
							/>
							<FeatureSubheader>Add a Photo</FeatureSubheader>
						</Featurette>

						<Featurette>
							<SpecialIcon
								src={require('./assets/images/feature3.gif')}
								alt="video camera"
							/>
							<FeatureSubheader>Create Video</FeatureSubheader>
						</Featurette>
						<Featurette>
							<SpecialIcon
								src={require('./assets/images/feature4.gif')}
								alt="postcard"
							/>
							<FeatureSubheader>Send as Postcard</FeatureSubheader>
						</Featurette>
					</Feature>
				</SpecialFeature>
				{this.props.children}
				<Footer />
			</React.Fragment>
		)
	}
}

export default App

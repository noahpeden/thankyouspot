import { injectGlobal } from 'styled-components'

injectGlobal`
    .swal2-toast {
        padding: 1em !important;
        box-shadow: initial !important;
    }
    .swal2-modal {
        padding: 1.5em 2.5em 2.5em !important;
    }
    .swal2-error {
        border-color: #FF7676 !important;
        .swal2-x-mark-line-left, .swal2-x-mark-line-right {
            background-color: #FF7676 !important;
        }
    }
    .swal2-question {
        border-color: #184E68 !important;
        color: #184E68 !important;
    }
    .swal2-warning {
        border-color: #FDE5A7 !important;
        color: #FDE5A7 !important;
        .swal2-icon {
            border-color: #FDE5A7 !important;
        }
    }
    .swal2-title {
        /* font-size: 2em !important; */
        font-family: 'Paytone One',sans-serif;
    }
    .swal2-content {
        font-family: 'Josefin Sans',sans-serif;
    }
	.swal2-modal button {
        border-radius: 30px !important;
        padding: 17px 30px 15px !important;
        font-size: 18px !important;
        font-family: 'Josefin Sans',sans-serif;
        transition: all 0.2s ease;
        &.swal2-confirm {
            border: 2px solid #ff7676 !important;
            background:#ff7676 !important; 
            &:hover {
                color: #ff7676 !important;
                background: transparent !important;
            }
        }
        &.swal2-cancel {
            border: 2px solid #184E68 !important;
            background:#184E68 !important; 
            &:hover {
                color: #184E68 !important;
                background: transparent !important;
            }
        }
	}
`

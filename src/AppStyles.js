import styled from 'styled-components'
import media from 'styled-media-query'
import { Link } from 'react-router-dom'

export const ButtonTY = styled(Link)`
	background: #ff7676;
	text-decoration: none;
	border: 2px solid #ff7676;
	color: white;
	border-radius: 50px;
	padding: 17px 30px 15px;
	margin-right: 20px;
	cursor: pointer;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	font-size: 18px;
	transition: all 0.2s ease;
	&:hover {
		background: transparent;
		color: #ff7676;
		cursor: pointer;
	}
	${media.lessThan('medium')`
		margin-right: 10px;
		font-size:16px;
		padding: 15px 10px;
	`};
`
export const ButtonPublic = styled(Link)`
	background: #dbeef7;
	text-decoration: none;
	border: 2px solid #dbeef7;
	color: #184e68;
	border-radius: 50px;
	padding: 17px 30px 15px;
	cursor: pointer;
	font-family: 'Josefin Sans', sans-serif;
	-webkit-font-smoothing: antialiased;
	font-weight: 700;
	font-size: 18px;
	transition: all 0.2s ease;
	&:hover {
		color: #dbeef7;
		background: transparent;
		cursor: pointer;
	}
	${media.lessThan('medium')`
		font-size:16px;
		padding: 15px 10px;
	`};
`
export const HeroSection = styled.section`
	background: #184e68;
	display: flex;
	justify-content: space-between;
	${media.lessThan('medium')`
		flex-direction: column;
	`};
`
export const HeroLeft = styled.div`
	width: 50%;
	flex: 1;
	flex-direction: column;
	justify-content: center;
	align-items: center;
	display: flex;
	${media.lessThan('medium')`
		width: auto;
		text-align: center;
	`};
`
export const HeroRight = styled.div`
	padding-top: 20px;
	height: 100%;
	width: 50%;
	display: flex;
	flex: 1;
	flex-direction: column;
	${media.between('medium', 'large')`
			height: 450px;
			justify-content: flex-end;
	`};
	${media.lessThan('medium')`
			width: auto;
			padding-top: 0;
			padding-left: 20px;
	`};
`
export const CTAContainer = styled.div`
	padding: 10%;
	button {
		font-size: 20px;
		padding: 15px;
	}
	${media.between('medium', 'large')`
		min-width: 510px;
	`};
	${media.lessThan('medium')`
		padding: 10% 5px 10px;
	`};
`
export const Welcome = styled.h1`
	font-size: 60px;
	color: #fde5a7;
	text-shadow: -6px 6px #193c4d;
	margin: 0;
	padding-bottom: 30px;
	width: 430px;
	font-family: 'Paytone One', sans-serif;
	-webkit-font-smoothing: antialiased;
	letter-spacing: 1px;
	${media.between('medium', 'large')`
		width: auto;
		font-size: 45px;
	`};
	${media.lessThan('medium')`
		font-size: 45px;
		width: auto;
		padding-bottom: 20px;
	`};
`
export const WelcomeSub = styled.h2`
	font-size: 25px;
	font-weight: lighter;
	color: #ffff;
	line-height: 1.5;
	margin: 0;
	padding-bottom: 60px;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 300;
	-webkit-font-smoothing: antialiased;
	${media.between('medium', 'large')`
		font-size: 22px;
		line-height: 32px;
	`};
	${media.lessThan('medium')`
		font-size: 20px;
		line-height: 32px;
		padding-bottom: 40px;
	`};
`
export const SpecialFeature = styled.section`
	padding: 60px 40px;
	background-color: #dbeef7;
	text-align: center;
	display: flex;
	flex: 1;
	flex-direction: row;
	align-items: center;
	justify-content: center;
	${media.between('medium', 'large')`
		display: grid;
		grid-template-columns: auto;
	`};
	${media.lessThan('medium')`
		display: grid;
		grid-template-columns: auto;
		padding: 40px 40px 10px;
	`};
`
export const FeatureTitle = styled.div`
	width: 230px;
	margin-right: 40px;
	${media.between('medium', 'large')`
			width: auto;
			margin-right: 0;
	`};
	${media.lessThan('medium')`
			width: auto;
			margin-right: 0;
	`};
`
export const FeatureTitleHeader = styled.h2`
	font-size: 2.75rem;
	color: #184e68;
	text-align: left;
	font-family: 'Paytone One', sans-serif;
	-webkit-font-smoothing: antialiased;
	letter-spacing: 1px;
	text-shadow: -6px 6px #e5cad0;
	${media.between('medium', 'large')`
			text-align: center;
			margin: 0 0 50px;
	`};
	${media.lessThan('medium')`
			text-align: center;
			margin: 0 0 50px;
	`};
`
export const Feature = styled.div`
	display: grid;
	grid-template-columns: auto auto auto auto;
	width: auto;
	${media.between('medium', 'large')`
		width: 95vw;
	`};
	${media.lessThan('medium')`
		width: 95vw;
		grid-template-columns: auto auto;
	`};
`
export const Featurette = styled.div`
		margin: 0 auto;
		width: 180px;
	}
	${media.between('medium', 'large')`
			width: auto;
	`};
	${media.lessThan('medium')`
			padding-bottom: 20px;
			width: auto;
	`};
`
export const SpecialIcon = styled.img`
	max-height: 110px;
	${media.between('medium', 'large')`
			max-height: 80px;
	`};
	${media.lessThan('medium')`
			max-height: 80px;
	`};
`
export const FeatureSubheader = styled.h3`
	font-size: 16px;
	text-transform: uppercase;
	color: #184e68;
	font-family: 'Josefin Sans', sans-serif;
	font-weight: 500;
	-webkit-font-smoothing: antialiased;

	${media.lessThan('medium')`
		font-size: 14px;
	`};
`
